package com.spring.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;

@EnableAutoConfiguration
@SpringBootApplication
@EnableAuthorizationServer
@EnableScheduling
public class SpringSecurityOauth2Example1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityOauth2Example1Application.class, args);
	}

}
