/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security;

import com.spring.security.configuration.CallBackConf;
import com.spring.security.configuration.EqConfiguration;
import com.spring.security.configuration.MpnConf;
import com.spring.security.configuration.SknConfiguration;
import com.spring.security.configuration.utility;
import com.spring.security.contoller.UserController;
import com.spring.security.dao.Calbback;
import com.spring.security.dao.Djp;
import com.spring.security.dao.MPNPayment;
import com.spring.security.dao.Transfer;
import com.spring.security.model.WilayahIDSIPD;
import com.spring.security.model.payroll_pajak;
import com.spring.security.model.payroll_pajak_sum;
import com.spring.security.model.tarsansction;
import com.spring.security.model.transaksi_potongan;
import com.spring.security.repository.RepoPotongan;
import com.spring.security.repository.RepoViewGabungan;
import com.spring.security.repository.WilayahSIPDRepository;
import com.spring.security.repository.payroll_pajakRepository;
import com.spring.security.repository.transactionRepository;
import com.spring.security.repository.transaski_potonganRepo;
import com.spring.security.service.KodeBankServiceImpl;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author DELL
 */
@Component
public class SchedulerCekTrx {

    @Autowired
    transactionRepository TR;

    @Autowired
    private KodeBankServiceImpl ki;

    @Autowired
    private SknConfiguration skncf;

    @Autowired
    private EqConfiguration eqConfiguration;

    @Autowired
    private MpnConf MpnConfiguration;

    @Autowired
    private payroll_pajakRepository Payroll_Pjk;

    @Autowired
    private WilayahSIPDRepository wsr;

    @Autowired
    private CallBackConf cbc;

    @Autowired
    private transaski_potonganRepo trp;

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SchedulerCekTrx.class);

    SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

//    @Scheduled(cron = "${cron.NotRealTimeTrx}")
//    public void scheduleTaskUsingCronExpression() {
//        long now = System.currentTimeMillis() / 1000;
//        utility ul = new utility();
//        Date date_log = new Date(System.currentTimeMillis());
//        System.out.println("schedule tasks using cron jobs - " + formatter.format(date_log));
//        LOGGER.info("###Scheduler_Not_RealTime_Transaksi: " + formatter.format(date_log));
//        String npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, nominalPajak, nop, idbilling;
//        WilayahIDSIPD wl;
//        Date date = new Date();
//        Djp in = new Djp();
//        Timestamp ts = new Timestamp(date.getTime());
//        List<tarsansction> tl = TR.listTransaksiPending(ts);
//        JSONObject status = new JSONObject();
//        JSONObject response = new JSONObject();
//        JSONArray pajak2 = new JSONArray();
//        Transfer trf = new Transfer();
//        List<String> nomerSp2d = new ArrayList<String>();;
//        String rsp;
//        int flag = 0;
//        if (tl.size() > 0) {
//            for (int i = 0; i < tl.size(); i++) {
//                tarsansction trn = tl.get(i);
//                UUID trx_id = tl.get(i).getTrx_id();
//                JSONObject InqRequest = new JSONObject(trn.getRequest());
//                response = new JSONObject(trn.getResponse());
//                String amount = String.valueOf(InqRequest.getInt("amount"));
//                String rek_asal = InqRequest.getJSONObject("sender_info").getString("account_number");
//                String rek_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_number");
//                String bank_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_bank");
//                String note = InqRequest.getString("note");
//                String desc = InqRequest.getJSONObject("tx_additional_data").getString("desc_sp2d");
//                String nosp2d = InqRequest.getJSONObject("tx_additional_data").getString("no_sp2d");
//                String KodeWilayah = InqRequest.getJSONObject("tx_additional_data").getString("kode_wilayah");
//                String[] Kdwid = KodeWilayah.split("\\|");
//                String idwilayah = Kdwid[0].trim();
//                String nama_penerima = InqRequest.getJSONObject("recipient_info").getString("account_bank_name");
//                String alamat_penerima = InqRequest.getJSONObject("recipient_info").getJSONObject("additional_data").getString("alamat_wp");
//                flag = 0;
//                String allnarasi = "";
//                int flagtrf = 0;
//                String rek_jamkes = "";
//                String rek_iwp8 = "";
//                String rek_iwp1 = "";
//                String rek_jkk = "";
//                String rek_jkm = "";
//                String rek_tapera = "";
//                String rek_iubulog = "";
//                String rek_iuzakat = "";
//                String nama_daerah = "";
//                System.out.println("hai id wilayah" + idwilayah);
//                utility uli = new utility();
//                wl = wsr.CekNorekPenampungan(Integer.parseInt(idwilayah));
//                if (wl != null) {
//                    rek_tujuan = wl.getNomerRek();
//                    rek_jamkes = wl.getNomerRekJamkes();
//                    rek_iwp8 = wl.getNomerRekIwp8();
//                    rek_iwp1 = wl.getNomerRekIwp1();
//                    rek_jkk = wl.getNomerRekJkk();
//                    rek_jkm = wl.getNomerRekJkm();
//                    rek_tapera = wl.getNomerRekTapera();
//                    rek_iubulog = wl.getNomerRekIuranBulog();
//                    rek_iuzakat = wl.getNomerRekIuranZakat();
//                    nama_daerah = wl.getNamadaerah();
//                    bank_tujuan = "110";
//                    String Nar1 = uli.spaceRightPadding("SIPD pembayaran gaji dan tunjangan lain nya", 35);
//                    String Nar2 = uli.spaceRightPadding(wl.getNamadaerah(), 35);
//                    String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                    allnarasi = Nar1 + Nar2 + Nar3;
//                }
//                rsp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tujuan, rek_asal, amount, allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                if (InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").has("potongan") && wl != null) {
//                    JSONArray potongan = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("potongan");
//                    for (int zk = 0; zk < potongan.length(); zk++) {
//                        if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_wajib_pegawai_1persen") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
//                            String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran wajib pegawai 1persen", 35);
//                            String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
//                            String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                            allnarasi = Nar1 + Nar2 + Nar3;
//                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iwp1, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                            transaksi_potongan Ctrp = new transaksi_potongan();
//                            Ctrp.setJenis_potongan("iuran_wajib_pegawai_1persen");
//                            Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
//                            Ctrp.setNorek_sumber(rek_asal);
//                            Ctrp.setRc(rspp.substring(0, 2));
//                            Ctrp.setNorek_tujuan(rek_iwp1);
//                            Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
//                            if (rspp.trim().length() > 2) {
//                                Ctrp.setRefrence(rspp.substring(2, 14));
//                            }
//                            trp.save(Ctrp);
//                        }
//                        if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_wajib_pegawai_8persen") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
//                            String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran wajib pegawai 8persen", 35);
//                            String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
//                            String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                            allnarasi = Nar1 + Nar2 + Nar3;
//                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iwp8, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                            transaksi_potongan Ctrp = new transaksi_potongan();
//                            Ctrp.setJenis_potongan("iuran_wajib_pegawai_8persen");
//                            Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
//                            Ctrp.setNorek_sumber(rek_asal);
//                            Ctrp.setRc(rspp.substring(0, 2));
//                            Ctrp.setNorek_tujuan(rek_iwp8);
//                            Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
//                            if (rspp.trim().length() > 2) {
//                                Ctrp.setRefrence(rspp.substring(2, 14));
//                            }
//                            trp.save(Ctrp);
//                        }
//                        if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_jaminan_kesehatan") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
//                            String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kesehatan", 35);
//                            String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
//                            String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                            allnarasi = Nar1 + Nar2 + Nar3;
//                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jamkes, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                            transaksi_potongan Ctrp = new transaksi_potongan();
//                            Ctrp.setJenis_potongan("iuran_jaminan_kesehatan");
//                            Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
//                            Ctrp.setNorek_sumber(rek_asal);
//                            Ctrp.setRc(rspp.substring(0, 2));
//                            Ctrp.setNorek_tujuan(rek_jamkes);
//                            Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
//                            if (rspp.trim().length() > 2) {
//                                Ctrp.setRefrence(rspp.substring(2, 14));
//                            }
//                            trp.save(Ctrp);
//                        }
//                        if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_jaminan_kecelakaan_kerja") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
//                            String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kecelakaan kerja", 35);
//                            String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
//                            String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                            allnarasi = Nar1 + Nar2 + Nar3;
//                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jkk, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                            transaksi_potongan Ctrp = new transaksi_potongan();
//                            Ctrp.setJenis_potongan("iuran_jaminan_kecelakaan_kerja");
//                            Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
//                            Ctrp.setNorek_sumber(rek_asal);
//                            Ctrp.setRc(rspp.substring(0, 2));
//                            Ctrp.setNorek_tujuan(rek_jkk);
//                            Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
//                            if (rspp.trim().length() > 2) {
//                                Ctrp.setRefrence(rspp.substring(2, 14));
//                            }
//                            trp.save(Ctrp);
//                        }
//                        if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_jaminan_kematian") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
//                            String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kematian", 35);
//                            String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
//                            String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                            allnarasi = Nar1 + Nar2 + Nar3;
//                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jkm, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                            transaksi_potongan Ctrp = new transaksi_potongan();
//                            Ctrp.setJenis_potongan("iuran_jaminan_kematian");
//                            Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
//                            Ctrp.setNorek_sumber(rek_asal);
//                            Ctrp.setRc(rspp.substring(0, 2));
//                            Ctrp.setNorek_tujuan(rek_jkm);
//                            Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
//                            if (rspp.trim().length() > 2) {
//                                Ctrp.setRefrence(rspp.substring(2, 14));
//                            }
//                            trp.save(Ctrp);
//                        }
//                        if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_simpanan_tapera") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
//                            String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran simpanan tapera", 35);
//                            String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
//                            String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                            allnarasi = Nar1 + Nar2 + Nar3;
//                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tapera, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                            transaksi_potongan Ctrp = new transaksi_potongan();
//                            Ctrp.setJenis_potongan("iuran_simpanan_tapera");
//                            Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
//                            Ctrp.setNorek_sumber(rek_asal);
//                            Ctrp.setRc(rspp.substring(0, 2));
//                            Ctrp.setNorek_tujuan(rek_jkm);
//                            Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
//                            if (rspp.trim().length() > 2) {
//                                Ctrp.setRefrence(rspp.substring(2, 14));
//                            }
//                            trp.save(Ctrp);
//                        }
//                        if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_bulog") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
//                            String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran_bulog", 35);
//                            String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
//                            String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                            allnarasi = Nar1 + Nar2 + Nar3;
//                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iubulog, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                            transaksi_potongan Ctrp = new transaksi_potongan();
//                            Ctrp.setJenis_potongan("iuran_bulog");
//                            Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
//                            Ctrp.setNorek_sumber(rek_asal);
//                            Ctrp.setRc(rspp.substring(0, 2));
//                            Ctrp.setNorek_tujuan(rek_jkm);
//                            Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
//                            if (rspp.trim().length() > 2) {
//                                Ctrp.setRefrence(rspp.substring(2, 14));
//                            }
//                            trp.save(Ctrp);
//                        }
//                        if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_zakat") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
//                            String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran zakat", 35);
//                            String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
//                            String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
//                            allnarasi = Nar1 + Nar2 + Nar3;
//                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iuzakat, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
//                            transaksi_potongan Ctrp = new transaksi_potongan();
//                            Ctrp.setJenis_potongan("iuran_zakat");
//                            Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
//                            Ctrp.setNorek_sumber(rek_asal);
//                            Ctrp.setRc(rspp.substring(0, 2));
//                            Ctrp.setNorek_tujuan(rek_jkm);
//                            Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
//                            if (rspp.trim().length() > 2) {
//                                Ctrp.setRefrence(rspp.substring(2, 14));
//                            }
//                            trp.save(Ctrp);
//                        }
//                    }
//                }
//                //trn.setReal_time(true);
//                if (!nomerSp2d.contains(nosp2d) && !InqRequest.get("tx_type").equals("LS|NONGAJI")) {
//                    nomerSp2d.add(nosp2d);
//                }
//                if (rsp.substring(0, 2).equals("00")) {
//                    System.out.println(rsp.substring(2, 14));//Refnum
//                    trn.setRefnum(rsp.substring(2, 14));
//                    JSONObject isipajak = new JSONObject();
//                    isipajak.put("id_billing", "");
//                    isipajak.put("ntpn", "");
//                    isipajak.put("tgl_ntpn", "");
//                    isipajak.put("ntb", "");
//                    pajak2.put(isipajak);
////                    if (InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").has("pajak")) {
////                        int lt = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").length();
////                        JSONObject pajak;
////                        for (int j = 0; j < lt; j++) {
////                            pajak = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").getJSONObject(j);
////                            if (pajak.getBoolean("include_pajak")) {
////                                npwp = pajak.getString("npwp");
////                                mata_anggaran = pajak.getString("mata_anggaran").trim();
////                                jenis_setoran = pajak.getString("jenis_setoran").trim();
////                                if (pajak.has("masa_pajak")) {
////                                    String[] arrmasapajak = pajak.getString("masa_pajak").trim().split("-");
////                                    masapajak1 = arrmasapajak[0];
////                                    masapajak2 = arrmasapajak[1];
////                                } else {
////                                    masapajak1 = pajak.getString("masa_pajak1").trim();
////                                    masapajak2 = pajak.getString("masa_pajak2").trim();
////                                }
////                                tahunpajak = pajak.getString("tahun_pajak").trim();
////                                nomersk = pajak.getString("no_spm").trim();
////                                if (!UserController.isStringInteger(nomersk)) {
////                                    nomersk = "000000000000000";
////                                }
////                                nominalPajak = pajak.get("nominal_pajak").toString().trim();
////                                nop = pajak.getString("nomor_object_pajak");
////                                if (!UserController.isStringInteger(nop)) {
////                                    nop = "";
////                                }
////                                rsp = in.INpwp(eqConfiguration, npwp, mata_anggaran, jenis_setoran);
////                                //System.out.println("InqDjp" + rsp);
////                                JSONObject isipajak = new JSONObject();
////                                // isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
////                                isipajak.put("id_billing", "");
////                                isipajak.put("ntpn", "");
////                                isipajak.put("tgl_ntpn", "");
////                                isipajak.put("ntb", "");
////                                if (rsp.substring(0, 2).equals("00")) {
////                                    rsp = in.CreateIdbilling(eqConfiguration, nominalPajak, npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, desc, nop);
////                                    if (rsp.substring(0, 2).equals("00")) {
////                                        JSONObject Mpnrsp;
////                                        idbilling = rsp.substring(64, 79);
////                                        isipajak.put("id_billing", idbilling);
////                                        //System.out.println(idbilling);
////                                        MPNPayment mpn = new MPNPayment();
////                                        Mpnrsp = mpn.MpnAll(MpnConfiguration, idbilling, rek_asal);
////                                        if (Mpnrsp.getString("RESPONSECODE").equals("00") || Mpnrsp.getString("RESPONSECODE").equals("69")) {
////                                            isipajak.put("ntpn", Mpnrsp.getJSONObject("MPO").getString("NTPN"));
////                                            isipajak.put("tgl_ntpn", Mpnrsp.getJSONObject("MPO").getString("SETLEMENT_DATE"));
////                                            isipajak.put("ntb", Mpnrsp.getJSONObject("MPO").getString("NTB"));
////                                            pajak2.put(isipajak);
////                                        } else {
////                                            flag = 1;
////                                            pajak2.put(isipajak);
////                                        }
////                                    } else {
////                                        flag = 1;
////                                        pajak2.put(isipajak);
////                                    }
////                                } else {
////                                    flag = 1;
////                                    pajak2.put(isipajak);
////                                }
////                            } else {
////                                JSONObject isipajak = new JSONObject();
////                                // isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
////                                isipajak.put("id_billing", "");
////                                isipajak.put("ntpn", "");
////                                isipajak.put("tgl_ntpn", "");
////                                isipajak.put("ntb", "");
////                                pajak2.put(isipajak);
////                            }
////                        }
////                    }
//                    if (flag == 1) {
//                        trn.setRespone_code("68");
//                        trn.setStatus("8");
//                        status.put("code", "300");
//                        status.put("message", "Pending Transaction");
//                    } else {
//                        trn.setStatus("8");
//                        trn.setRespone_code("00");
//                        status.put("code", "300");
//                        status.put("message", "Pending Transaction");
//                    }
//                } else if (rsp.substring(0, 2).equals("51")) {
//                    JSONObject isipajak = new JSONObject();
//                    // isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
//                    isipajak.put("id_billing", "");
//                    isipajak.put("ntpn", "");
//                    isipajak.put("tgl_ntpn", "");
//                    isipajak.put("ntb", "");
//                    pajak2.put(isipajak);
//                    trn.setRespone_code("51");
//                    trn.setStatus("4");
//                    status.put("code", "203");
//                    status.put("message", "Balance sender is not enough");
//                } else {
//                    JSONObject isipajak = new JSONObject();
//                    trn.setStatus("4");
//                    trn.setRespone_code(rsp.substring(0, 2));
//                    status.put("code", "200");
//                    status.put("message", "Failed to create transaction");
//                    isipajak.put("id_billing", "");
//                    isipajak.put("ntpn", "");
//                    isipajak.put("tgl_ntpn", "");
//                    isipajak.put("ntb", "");
//                    pajak2.put(isipajak);
//                }
//
//                response.put("status", status);
//                JSONObject adtionaldata = new JSONObject();
//                if (!pajak2.isEmpty()) {
//                    //System.out.println(pajak2.length());
//                    if (status.getString("code").equals("100")) {
//                        adtionaldata.put("pajak", pajak2);
//                    } else {
//                        adtionaldata.put("pajak", pajak2);
//                    }
//                } else {
//                    adtionaldata.put("pajak", "");
//                }
//
//                response.put("additional_data", adtionaldata.get("pajak"));
//                date = new Date();
//                ts = new Timestamp(date.getTime());
//                trn.setUpdate_date(ts);
//                trn.setResponse(response.toString());
//                if (!trn.getStatus().equals("8")) {
//                    Calbback cb = new Calbback();
//                    String rsp_callbck = cb.callback_api(response, cbc);
//                    if (rsp_callbck.equals("200")) {
//                        trn.setStatus("1");
//                    } else {
//                        trn.setStatus("6");
//                        // gagal callback
//                    }
//                }
//
//                TR.save(trn);
//                LOGGER.info("###Scheduler_Not_RealTime_Transaksi: " + trn.toString());
//            }
//            List<payroll_pajak> pnp;
//            payroll_pajak_sum prp;
//            List<Object> tes;
//            String ntpn, ntb, tgl_ntpn;
//            //jika non ls skip proses ini
//            for (int k = 0; k < nomerSp2d.size(); k++) {
//                tes = Payroll_Pjk.FindPajak(nomerSp2d.get(k));
//                for (int a = 0; a < tes.size(); a++) {
//                    Object[] row = (Object[]) tes.get(a);
//                    System.out.println(Arrays.toString(row));
//                    prp = new payroll_pajak_sum(Integer.parseInt(row[0].toString()), row[1].toString(), row[2].toString(), row[3].toString(), row[4].toString(), row[5].toString(), row[6].toString(), row[7].toString(), row[8].toString(), row[9].toString(), row[10].toString());
//                    rsp = in.INpwp(eqConfiguration, prp.getNpwp(), prp.getMata_anggaran(), prp.getJenis_setoran());
//                    JSONObject isipajak = new JSONObject();
//                    idbilling = "";
//                    ntpn = "";
//                    tgl_ntpn = "";
//                    ntb = "";
//                    if (rsp.substring(0, 2).equals("00")) {
//                        rsp = in.CreateIdbilling(eqConfiguration, String.valueOf(prp.getNominal()), prp.getNpwp(), prp.getMata_anggaran(), prp.getJenis_setoran(), prp.getMasapajak1(), prp.getMasapajak2(), prp.getTahunpajak(), prp.getNomersk(), prp.getNo_sp2d(), prp.getNop());
//                        if (rsp.substring(0, 2).equals("00")) {
//                            JSONObject Mpnrsp;
//                            idbilling = rsp.substring(64, 79);
//                            isipajak.put("id_billing", idbilling);
//                            try {
//                                //System.out.println(idbilling);
//                                Thread.sleep(1000);
//                            } catch (InterruptedException ex) {
//                                Logger.getLogger(SchedulerCekTrx.class.getName()).log(Level.SEVERE, null, ex);
//                            }
//                            MPNPayment mpn = new MPNPayment();
//                            Mpnrsp = mpn.MpnAll(MpnConfiguration, idbilling, prp.getRekening_asal());
//                            if (Mpnrsp.getString("RESPONSECODE").equals("00") || Mpnrsp.getString("RESPONSECODE").equals("69")) {
//                                ntpn = Mpnrsp.getJSONObject("MPO").getString("NTPN");
//                                tgl_ntpn = Mpnrsp.getJSONObject("MPO").getString("SETLEMENT_DATE");
//                                ntb = Mpnrsp.getJSONObject("MPO").getString("NTB");
//                                pajak2.put(isipajak);
//                            } else {
//                                flag = 1;
//                                // pajak2.put(isipajak);
//                            }
//                        } else {
//                            flag = 1;
//                            //pajak2.put(isipajak);
//                        }
//                    } else {
//                        flag = 1;
//                        //pajak2.put(isipajak);
//                    }
//                    Payroll_Pjk.updatebilling(idbilling, ntpn, ntb, tgl_ntpn, prp.getNo_sp2d(), prp.getNpwp(), prp.getMata_anggaran(), prp.getJenis_setoran(), prp.getMasapajak1(), prp.getMasapajak2(), prp.getTahunpajak(), prp.getNomersk(), prp.getNop(), prp.getRekening_asal());
//                    pnp = Payroll_Pjk.FindWithSP2D(prp.getNo_sp2d(), prp.getNpwp(), prp.getMata_anggaran(), prp.getJenis_setoran(), prp.getMasapajak1(), prp.getMasapajak2(), prp.getTahunpajak(), prp.getNomersk(), prp.getNop(), prp.getRekening_asal());
//                    for (int l = 0; l < pnp.size(); l++) {
//                        tarsansction tnp = TR.findById(pnp.get(l).getTrx_id());
//                        response = new JSONObject(tnp.getResponse());
//                        //JSONArray axz = new JSONArray();
//                        //axz = null;
////                        response.getJSONArray("additional_data").getJSONObject(0).put("id_billing", pnp.get(l).getId_billing());
////                        response.getJSONArray("additional_data").getJSONObject(0).put("ntb", pnp.get(l).getNtb());
////                        response.getJSONArray("additional_data").getJSONObject(0).put("ntpn", pnp.get(l).getNtpn());
////                        response.getJSONArray("additional_data").getJSONObject(0).put("tgl_ntpn", pnp.get(l).getTgl_ntpn());
//                        // axz.put(response.get("additional_data"));
//                        //response.put("additional_data", axz);
//                        isipajak = new JSONObject();
//                        pajak2 = new JSONArray();
//                        isipajak.put("id_billing", pnp.get(l).getId_billing());
//                        isipajak.put("ntpn", pnp.get(l).getNtpn());
//                        isipajak.put("tgl_ntpn", pnp.get(l).getTgl_ntpn());
//                        isipajak.put("ntb", pnp.get(l).getNtb());
//                        pajak2.put(isipajak);
//                        response.remove("additional_data");
//                        response.put("additional_data", pajak2);
//                        if (!pnp.get(l).getId_billing().isEmpty() && !pnp.get(l).getNtb().isEmpty() && tnp.getRespone_code().equals("00")) {
//                            response.getJSONObject("status").put("code", "000");
//                            response.getJSONObject("status").put("message", "Success");
//                        }
//                        Calbback cb = new Calbback();
//                        String rsp_callbck = cb.callback_api(response, cbc);
//                        if (rsp_callbck.equals("200")) {
//                            tnp.setStatus("1");
//                        } else {
//                            tnp.setStatus("6");
//                            // gagal callback
//                        }
//                        tnp.setResponse(response.toString());
//                        TR.save(tnp);
//                    }
//
//                }
//            }
//
//            //System.out.println(tl.get(0).getResponse());
//        } else {
//            LOGGER.info("###Scheduler_Not_RealTime_Transaksi: " + "DATA KOSONG UNTUK DIKESEKUSI");
//            //System.out.println("DATA KOSONG");
//        }
//    }
    @Scheduled(cron = "${cron.NotRealTimeTrx}")
    public void scheduleTaskUsingCronExpression() {
        Date date_log = new Date(System.currentTimeMillis());
        WilayahIDSIPD wl;
        Djp in = new Djp();
        String rsp;
        Transfer trf = new Transfer();
        LOGGER.info("###Scheduler_Not_RealTime_Transaksi: " + formatter.format(date_log));
        if (TR.FindTransaksi().size() > 0) {
            System.out.println("ada");
            for (RepoViewGabungan FindTransaksi : TR.FindTransaksi()) {
                System.out.println(FindTransaksi.gets());
                System.out.println(FindTransaksi.getq());
                if (FindTransaksi.gets().equals("LS|GAJI") && FindTransaksi.getq().equals("2")) {
                    String KodeWilayah = FindTransaksi.getr();
                    String[] Kdwid = KodeWilayah.split("\\|");
                    String idwilayah = Kdwid[0].trim();
                    System.out.println(idwilayah);
                    utility uli = new utility();
                    wl = wsr.CekNorekPenampungan(Integer.parseInt(idwilayah));
                    String rek_jamkes = "";
                    String rek_iwp8 = "";
                    String rek_iwp1 = "";
                    String rek_jkk = "";
                    String rek_jkm = "";
                    String rek_tapera = "";
                    String rek_iubulog = "";
                    String rek_iuzakat = "";
                    String nama_daerah = "";
                    String rek_tujuan = "";
                    String bank_tujuan = "";
                    String allnarasi = "";
                    //String lblTglSuratKeluar = suratKeluarc.TglSurat.ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                    Formatter fmt;
                    Calendar cal = Calendar.getInstance();
                    fmt = new Formatter();
                    fmt.format("%tB", cal);
                    if (wl != null) {
                        rek_tujuan = wl.getNomerRek();
                        rek_jamkes = wl.getNomerRekJamkes();
                        rek_iwp8 = wl.getNomerRekIwp8();
                        rek_iwp1 = wl.getNomerRekIwp1();
                        rek_jkk = wl.getNomerRekJkk();
                        rek_jkm = wl.getNomerRekJkm();
                        rek_tapera = wl.getNomerRekTapera();
                        rek_iubulog = wl.getNomerRekIuranBulog();
                        rek_iuzakat = wl.getNomerRekIuranZakat();
                        nama_daerah = wl.getNamadaerah();
                        bank_tujuan = "110";
                        String Nar1 = uli.spaceRightPadding("Pemb. Gaji SIPD " + fmt, 35);
                        String Nar2 = uli.spaceRightPadding(wl.getNamadaerah(), 35);
                        String Nar3 = "";
                        allnarasi = Nar1 + Nar2 + Nar3;
                    }
                    System.out.println(FindTransaksi.getl() + "==" + FindTransaksi.geto() + "&&" + FindTransaksi.getm() + "==" + FindTransaksi.getn());
                    if ((FindTransaksi.getl().equals(FindTransaksi.geto())) && (FindTransaksi.getm() == FindTransaksi.getn())) {
                        System.out.println("masuk");
                        rsp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tujuan, FindTransaksi.gett(), String.valueOf(FindTransaksi.geta()), allnarasi, "", "", ki, skncf);
                        String refnum = "";
                        if (rsp.trim().length() > 2) {
                            refnum = rsp.substring(2, 14);
                        }
                        String Nar1 = uli.spaceRightPadding("Pemb. Pot SIPD " + fmt, 35);
                        String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                        //iwp1
                        if (FindTransaksi.getb() > 0) {

                            String Nar3 = "IWP1%";
                            allnarasi = Nar1 + Nar2 + Nar3;
                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iwp1, FindTransaksi.gett(), String.valueOf(FindTransaksi.getb()), allnarasi, "", "", ki, skncf);
                            transaksi_potongan Ctrp = new transaksi_potongan();
                            Ctrp.setJenis_potongan("iuran_wajib_pegawai_1persen");
                            Ctrp.setNominal((String) String.valueOf(FindTransaksi.getb()));
                            Ctrp.setNorek_sumber(FindTransaksi.gett());
                            Ctrp.setRc(rspp.substring(0, 2));
                            Ctrp.setNorek_tujuan(rek_iwp1);
                            Ctrp.setTrx_patner_id(FindTransaksi.getk());
                            if (rspp.trim().length() > 2) {
                                Ctrp.setRefrence(rspp.substring(2, 14));
                            }
                            trp.save(Ctrp);
                        }
                        //iwp8
                        if (FindTransaksi.getc() > 0) {
                            //String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran wajib pegawai 8persen"+fmt, 35);
                            // String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                            String Nar3 = "IWP8%";
                            allnarasi = Nar1 + Nar2 + Nar3;
                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iwp8, FindTransaksi.gett(), String.valueOf(FindTransaksi.getc()), allnarasi, "", "", ki, skncf);
                            transaksi_potongan Ctrp = new transaksi_potongan();
                            Ctrp.setJenis_potongan("iuran_wajib_pegawai_8persen");
                            Ctrp.setNominal((String) String.valueOf(FindTransaksi.getc()));
                            Ctrp.setNorek_sumber(FindTransaksi.gett());
                            Ctrp.setRc(rspp.substring(0, 2));
                            Ctrp.setNorek_tujuan(rek_iwp8);
                            Ctrp.setTrx_patner_id(FindTransaksi.getk());
                            if (rspp.trim().length() > 2) {
                                Ctrp.setRefrence(rspp.substring(2, 14));
                            }
                            trp.save(Ctrp);
                        }
                        //JKS
                        if (FindTransaksi.getd() > 0) {
                            //String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kesehatan", 35);
                            //String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                            String Nar3 = "Iuran JKN";
                            allnarasi = Nar1 + Nar2 + Nar3;
                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jamkes, FindTransaksi.gett(), String.valueOf(FindTransaksi.getd()), allnarasi, "", "", ki, skncf);
                            transaksi_potongan Ctrp = new transaksi_potongan();
                            Ctrp.setJenis_potongan("iuran_jaminan_kesehatan");
                            Ctrp.setNominal((String) String.valueOf(FindTransaksi.getd()));
                            Ctrp.setNorek_sumber(FindTransaksi.gett());
                            Ctrp.setRc(rspp.substring(0, 2));
                            Ctrp.setNorek_tujuan(rek_jamkes);
                            Ctrp.setTrx_patner_id(FindTransaksi.getk());
                            if (rspp.trim().length() > 2) {
                                Ctrp.setRefrence(rspp.substring(2, 14));
                            }
                            trp.save(Ctrp);
                        }
                        //JKK
                        if (FindTransaksi.gete() > 0) {
                            //String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kecelakaan kerja", 35);
                            //String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                            String Nar3 = "Iuran JKK";
                            allnarasi = Nar1 + Nar2 + Nar3;
                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jkk, FindTransaksi.gett(), String.valueOf(FindTransaksi.gete()), allnarasi, "", "", ki, skncf);
                            transaksi_potongan Ctrp = new transaksi_potongan();
                            Ctrp.setJenis_potongan("iuran_jaminan_kecelakaan_kerja");
                            Ctrp.setNominal((String) String.valueOf(FindTransaksi.gete()));
                            Ctrp.setNorek_sumber(FindTransaksi.gett());
                            Ctrp.setRc(rspp.substring(0, 2));
                            Ctrp.setNorek_tujuan(rek_jkk);
                            Ctrp.setTrx_patner_id(FindTransaksi.getk());
                            if (rspp.trim().length() > 2) {
                                Ctrp.setRefrence(rspp.substring(2, 14));
                            }
                            trp.save(Ctrp);
                        }
                        //JMK
                        if (FindTransaksi.getf() > 0) {
                            //String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kematian", 35);
                            //String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                            String Nar3 = "Iuran JKM";
                            allnarasi = Nar1 + Nar2 + Nar3;
                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jkm, FindTransaksi.gett(), String.valueOf(FindTransaksi.getf()), allnarasi, "", "", ki, skncf);
                            transaksi_potongan Ctrp = new transaksi_potongan();
                            Ctrp.setJenis_potongan("iuran_jaminan_kematian");
                            Ctrp.setNominal((String) String.valueOf(FindTransaksi.getf()));
                            Ctrp.setNorek_sumber(FindTransaksi.gett());
                            Ctrp.setRc(rspp.substring(0, 2));
                            Ctrp.setNorek_tujuan(rek_jkm);
                            Ctrp.setTrx_patner_id(FindTransaksi.getk());
                            if (rspp.trim().length() > 2) {
                                Ctrp.setRefrence(rspp.substring(2, 14));
                            }
                            trp.save(Ctrp);
                        }
                        //Tapera
                        if (FindTransaksi.getg() > 0) {
                            //String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran simpanan tapera", 35);
                            //String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                            String Nar3 = "Tapera";
                            allnarasi = Nar1 + Nar2 + Nar3;
                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tapera, FindTransaksi.gett(), String.valueOf(FindTransaksi.getg()), allnarasi, "", "", ki, skncf);
                            transaksi_potongan Ctrp = new transaksi_potongan();
                            Ctrp.setJenis_potongan("iuran_simpanan_tapera");
                            Ctrp.setNominal((String) String.valueOf(FindTransaksi.getg()));
                            Ctrp.setNorek_sumber(FindTransaksi.gett());
                            Ctrp.setRc(rspp.substring(0, 2));
                            Ctrp.setNorek_tujuan(rek_tapera);
                            Ctrp.setTrx_patner_id(FindTransaksi.getk());
                            if (rspp.trim().length() > 2) {
                                Ctrp.setRefrence(rspp.substring(2, 14));
                            }
                            trp.save(Ctrp);
                        }
                        //zakat
                        if (FindTransaksi.geti() > 0) {
                            //String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran zakat", 35);
                            //String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                            String Nar3 = "Zakat";
                            allnarasi = Nar1 + Nar2 + Nar3;
                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iuzakat, FindTransaksi.gett(), String.valueOf(FindTransaksi.geti()), allnarasi, "", "", ki, skncf);
                            transaksi_potongan Ctrp = new transaksi_potongan();
                            Ctrp.setJenis_potongan("iuran_zakat");
                            Ctrp.setNominal((String) String.valueOf(FindTransaksi.geti()));
                            Ctrp.setNorek_sumber(FindTransaksi.gett());
                            Ctrp.setRc(rspp.substring(0, 2));
                            Ctrp.setNorek_tujuan(rek_iuzakat);
                            Ctrp.setTrx_patner_id(FindTransaksi.getk());
                            if (rspp.trim().length() > 2) {
                                Ctrp.setRefrence(rspp.substring(2, 14));
                            }
                            trp.save(Ctrp);
                        }
                        //bulog
                        if (FindTransaksi.getj() > 0) {
                            //String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran bulog", 35);
                            //String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                            String Nar3 = "Bulog";
                            allnarasi = Nar1 + Nar2 + Nar3;
                            String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iubulog, FindTransaksi.gett(), String.valueOf(FindTransaksi.getj()), allnarasi, "", "", ki, skncf);
                            transaksi_potongan Ctrp = new transaksi_potongan();
                            Ctrp.setJenis_potongan("iuran_bulog");
                            Ctrp.setNominal((String) String.valueOf(FindTransaksi.getj()));
                            Ctrp.setNorek_sumber(FindTransaksi.gett());
                            Ctrp.setRc(rspp.substring(0, 2));
                            Ctrp.setNorek_tujuan(rek_iubulog);
                            Ctrp.setTrx_patner_id(FindTransaksi.getk());
                            if (rspp.trim().length() > 2) {
                                Ctrp.setRefrence(rspp.substring(2, 14));
                            }
                            trp.save(Ctrp);
                        }
                        //Pembayaran PPN 
                        String rsppp = in.INpwp(eqConfiguration, FindTransaksi.getu(), FindTransaksi.getv(), FindTransaksi.getw());
                        JSONObject isipajak = new JSONObject();
                        String idbilling = "";
                        String ntpn = "";
                        String tgl_ntpn = "";
                        String ntb = "";
                        String[] arrmasapajak = FindTransaksi.getx().trim().split("-");
                        String masapajak1 = arrmasapajak[0];
                        String masapajak2 = arrmasapajak[1];
                        String tahun = arrmasapajak[2];
                        if (rsppp.substring(0, 2).equals("00")) {
                            rsppp = in.CreateIdbilling(eqConfiguration, String.valueOf(FindTransaksi.geth()), FindTransaksi.getu(), FindTransaksi.getv(), FindTransaksi.getw(), masapajak1, masapajak2, tahun, FindTransaksi.gety(), FindTransaksi.getk(), FindTransaksi.getz());
                            if (rsppp.substring(0, 2).equals("00")) {
                                JSONObject Mpnrsp;
                                idbilling = rsppp.substring(64, 79);
                                isipajak.put("id_billing", idbilling);
                                Payroll_Pjk.updateWithSp2d(idbilling, "", "", "", FindTransaksi.getk());
                                //updateWithSp2d(String idbilling, String ntpn, String ntb, String tgl_ntpn, String nosp2d);
                                try {
                                    //System.out.println(idbilling);
                                    Thread.sleep(1000);
                                } catch (InterruptedException ex) {
                                    Logger.getLogger(SchedulerCekTrx.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                MPNPayment mpn = new MPNPayment();
                                Mpnrsp = mpn.MpnAll(MpnConfiguration, idbilling, FindTransaksi.gett());
                                if (Mpnrsp.getString("RC").equals("0000") || Mpnrsp.getString("RC").equals("0069")) {
                                    ntpn = Mpnrsp.getJSONObject("MPO").getString("NTPN");
                                    tgl_ntpn = Mpnrsp.getJSONObject("MPO").getString("SETLEMENT_DATE");
                                    ntb = Mpnrsp.getJSONObject("MPO").getString("NTB");
                                    Payroll_Pjk.updateWithSp2d(idbilling, ntpn, ntb, tgl_ntpn, FindTransaksi.getk());
                                } else {
                                    // pajak2.put(isipajak);
                                }
                            } else {
                                //pajak2.put(isipajak);
                            }
                        } else {

                            //pajak2.put(isipajak);
                        }
                        // savean
                        TR.updateWithSp2d(refnum, rsp.substring(0, 2), "6", FindTransaksi.getk());
                    }
                }
            }
        }
    }

    @Scheduled(cron = "${cron.CekCalbackGaji}")
    public void scheduleTaskCalbackGaji() {
        Date date_log = new Date(System.currentTimeMillis());
        WilayahIDSIPD wl;
        Djp in = new Djp();
        String rsp;
        Transfer trf = new Transfer();
//        List<String> potongan = new ArrayList<String>();
        Calbback cb = new Calbback();
        LOGGER.info("###Scheduler_CalbackGaji: " + formatter.format(date_log));
        if (TR.FindTransaksi().size() > 0) {
            for (RepoViewGabungan FindTransaksi : TR.FindTransaksi()) {
                List<String> potongan = new ArrayList<String>();

                if (FindTransaksi.gets().equals("LS|GAJI") && FindTransaksi.getq().equals("6")) {
                    String countTransaksi = TR.FindTransaksiCount(FindTransaksi.getk());
                    System.out.println(countTransaksi);
                    System.out.println(FindTransaksi.getm());
                    int count = Integer.parseInt(countTransaksi);
                    if (FindTransaksi.getm() == count) {
                        //cek potongan ada atau tidak
                        System.out.println("masuk sisni");
                        System.out.println(TR.FindPotongan(FindTransaksi.getk()).size());
                        if (TR.FindPotongan(FindTransaksi.getk()).size() > 0) {
                            for (RepoPotongan rp : TR.FindPotongan(FindTransaksi.getk())) {
                                if (rp.getb().equals("1")) {
                                    potongan.add("iuran_wajib_pegawai_1persen");
                                }
                                if (rp.getc().equals("1")) {
                                    potongan.add("iuran_wajib_pegawai_8persen");
                                }
                                if (rp.getd().equals("1")) {
                                    potongan.add("iuran_jaminan_kesehatan");
                                }
                                if (rp.gete().equals("1")) {
                                    potongan.add("iuran_jaminan_kecelakaan_kerja");
                                }
                                if (rp.getf().equals("1")) {
                                    potongan.add("iuran_jaminan_kematian");
                                }
                                if (rp.getg().equals("1")) {
                                    potongan.add("iuran_simpanan_tapera");
                                }
                                if (rp.geth().equals("1")) {
                                    potongan.add("iuran_zakat");
                                }
                                if (rp.geth().equals("1")) {
                                    potongan.add("iuran_bulog");
                                }
                            }
                        }
                        //query hasil postingn potongan
                        //jika ada potongan
                        if (potongan.size() > 0) {
                            if (trp.TransaksiPotonganCek(potongan, FindTransaksi.getk()).size() > 0) {
                                List<transaksi_potongan> ltp = trp.TransaksiPotonganCek(potongan, FindTransaksi.getk());
                                if (potongan.size() == ltp.size()) {
                                    //cek pajak
                                    if (FindTransaksi.geth() > 0) {
                                        List<payroll_pajak> lsp = Payroll_Pjk.FindCountNosp2d(FindTransaksi.getk());
                                        if (FindTransaksi.getm() == lsp.size()) {
                                            //persiapan pengiriman call back
                                            String idbilling = lsp.get(0).getId_billing();
                                            String ntb = lsp.get(0).getNtb();
                                            String ntpn = lsp.get(0).getNtpn();
                                            String tgl_ntpn = lsp.get(0).getTgl_ntpn();
                                            JSONObject status = new JSONObject();
                                            JSONObject addtional_data = new JSONObject();
                                            JSONArray addtional_data_array = new JSONArray();
                                            JSONObject call_bak = new JSONObject();
                                            List<tarsansction> lits = TR.listTransaksiCallback("6", FindTransaksi.getk());
                                            for (tarsansction lit : lits) {
                                                SimpleDateFormat formatter5 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
                                                Date date = new Date(System.currentTimeMillis());
                                                //response.put("created", );
                                                call_bak.put("created", formatter5.format(date));
                                                call_bak.put("tx_partner_id", lit.getTx_partner_id());
                                                call_bak.put("tx_id", lit.getTrx_id());
                                                status.put("code", "000");
                                                status.put("message", "Success");
                                                call_bak.put("status", status);
                                                addtional_data.put("ntpn", ntpn);
                                                addtional_data.put("id_billing", idbilling);
                                                addtional_data.put("ntb", ntb);
                                                addtional_data.put("tgl_ntpn", tgl_ntpn);
                                                addtional_data_array.put(0, addtional_data);
                                                call_bak.put("additional_data", addtional_data_array);
                                                call_bak.put("bpd_code", "110");
                                                call_bak.put("tx_type", lit.getType_trx());
                                                //Calbback cb = new Calbback();
                                                String rsp_callbck = cb.callback_api(call_bak, cbc);
                                                if (rsp_callbck.equals("200")) {
                                                    lit.setStatus("1");
                                                    lit.setResponse(call_bak.toString());
                                                } else {
                                                    lit.setStatus("6");
                                                    // gagal callback
                                                }
                                                TR.save(lit);
                                            }
                                        }//jika pajak belum tercreate
                                        else {
                                        }
                                        //jika ppn 0
                                    } else {
                                        JSONObject status = new JSONObject();
                                        JSONObject addtional_data = new JSONObject();
                                        JSONArray addtional_data_array = new JSONArray();
                                        JSONObject call_bak = new JSONObject();
                                        List<tarsansction> lits = TR.listTransaksiCallback("6", FindTransaksi.getk());
                                        for (tarsansction lit : lits) {
                                            SimpleDateFormat formatter5 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
                                            Date date = new Date(System.currentTimeMillis());
                                            //response.put("created", );
                                            call_bak.put("created", formatter5.format(date));
                                            call_bak.put("tx_partner_id", lit.getTx_partner_id());
                                            call_bak.put("tx_id", lit.getTrx_id());
                                            status.put("code", "000");
                                            status.put("message", "Success");
                                            call_bak.put("status", status);
                                            addtional_data_array.put(0, addtional_data);
                                            call_bak.put("additional_data", addtional_data_array);
                                            call_bak.put("bpd_code", "110");
                                            call_bak.put("tx_type", lit.getType_trx());
                                            //Calbback cb = new Calbback();
                                            String rsp_callbck = cb.callback_api(call_bak, cbc);
                                            if (rsp_callbck.equals("200")) {
                                                lit.setStatus("1");
                                                lit.setResponse(call_bak.toString());
                                            } else {
                                                lit.setStatus("6");
                                                // gagal callback
                                            }
                                            TR.save(lit);
                                        }
                                    }
                                    //jika potongan tidak sama reposting potongan yang belum ada
                                }

                            }
                            //jika tidak ada potongan
                        } else {
                            //cek pajak
                            List<payroll_pajak> lsp = Payroll_Pjk.FindCountNosp2d(FindTransaksi.getk());
                            if (FindTransaksi.getm() == lsp.size()) {
                                //persiapan pengiriman call back
                                String idbilling = lsp.get(0).getId_billing();
                                String ntb = lsp.get(0).getNtb();
                                String ntpn = lsp.get(0).getNtpn();
                                String tgl_ntpn = lsp.get(0).getTgl_ntpn();

                                List<tarsansction> lits = TR.listTransaksiCallback("6", FindTransaksi.getk());
                                for (tarsansction lit : lits) {
                                    JSONObject status = new JSONObject();
                                    JSONObject addtional_data = new JSONObject();
                                    JSONArray addtional_data_array = new JSONArray();
                                    JSONObject call_bak = new JSONObject();
                                    SimpleDateFormat formatter5 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
                                    Date date = new Date(System.currentTimeMillis());
                                    //response.put("created", );
                                    call_bak.put("created", formatter5.format(date));
                                    call_bak.put("tx_partner_id", lit.getTx_partner_id());
                                    call_bak.put("tx_id", lit.getTrx_id());
                                    status.put("code", "000");
                                    status.put("message", "Success");
                                    call_bak.put("status", status);
                                    addtional_data.put("ntpn", ntpn);
                                    addtional_data.put("id_billing", idbilling);
                                    addtional_data.put("ntb", ntb);
                                    addtional_data.put("tgl_ntpn", tgl_ntpn);
                                    addtional_data_array.put(0, addtional_data);
                                    System.out.println(addtional_data_array.length());
                                    call_bak.put("additional_data", addtional_data_array);
                                    String rsp_callbck = cb.callback_api(call_bak, cbc);
                                    System.out.println("masukkk dulu sini ih");
                                    call_bak.put("bpd_code", "110");
                                    call_bak.put("tx_type", lit.getType_trx());
                                    if (rsp_callbck.equals("200")) {
                                        lit.setStatus("1");
                                        lit.setResponse(call_bak.toString());
                                    } else {
                                        lit.setStatus("6");
                                        // gagal callback
                                    }
                                    TR.save(lit);
                                }
                            }

                        }
                    }
                }
            }
        }
    }

    @Scheduled(cron = "${cron.NotRealTimeTrxNonGaji}")
    public void scheduleTransaksiNonGaji() {
        long now = System.currentTimeMillis() / 1000;
        utility ul = new utility();
        Date date_log = new Date(System.currentTimeMillis());
        System.out.println("schedule tasks using cron jobs - " + formatter.format(date_log));
        LOGGER.info("###Scheduler_Not_RealTime_Transaksi_Non_Gaji: " + formatter.format(date_log));
        String npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, nominalPajak, nop, idbilling;
        WilayahIDSIPD wl;
        Date date = new Date();
        Djp in = new Djp();
        Timestamp ts = new Timestamp(date.getTime());
        List<tarsansction> tl = TR.listTransaksiPendingNonGaji(ts);
        JSONObject status = new JSONObject();
        JSONObject response = new JSONObject();
        JSONArray pajak2 = new JSONArray();
        Transfer trf = new Transfer();
        List<String> nomerSp2d = new ArrayList<String>();;
        String rsp;
        int flag = 0;
        if (tl.size() > 0) {
            for (int i = 0; i < tl.size(); i++) {
                tarsansction trn = tl.get(i);
                UUID trx_id = tl.get(i).getTrx_id();
                JSONObject InqRequest = new JSONObject(trn.getRequest());
                response = new JSONObject(trn.getResponse());
                String amount = String.valueOf(InqRequest.getInt("amount"));
                String rek_asal = InqRequest.getJSONObject("sender_info").getString("account_number");
                String rek_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_number");
                String bank_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_bank");
                String note = InqRequest.getString("note");
                String desc = InqRequest.getJSONObject("tx_additional_data").getString("desc_sp2d");
                String nosp2d = InqRequest.getJSONObject("tx_additional_data").getString("no_sp2d");
                String KodeWilayah = InqRequest.getJSONObject("tx_additional_data").getString("kode_wilayah");
                String[] Kdwid = KodeWilayah.split("\\|");
                String idwilayah = Kdwid[0].trim();
                String nama_penerima = InqRequest.getJSONObject("recipient_info").getString("account_bank_name");
                String alamat_penerima = InqRequest.getJSONObject("recipient_info").getJSONObject("additional_data").getString("alamat_wp");
                int jumlah_pajak = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").length();
                flag = 0;
                String allnarasi = "";
                int flagtrf = 0;
                String rek_jamkes = "";
                String rek_iwp8 = "";
                String rek_iwp1 = "";
                String rek_jkk = "";
                String rek_jkm = "";
                String rek_tapera = "";
                String rek_iubulog = "";
                String rek_iuzakat = "";
                String nama_daerah = "";
                JSONObject isipajak = new JSONObject();
                System.out.println("hai id wilayah" + idwilayah);

                if (jumlah_pajak > 0) {
                    JSONArray pjk_n = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak");
                    MPNPayment mpn2 = new MPNPayment();
                    JSONObject RCMPN;
                    for (int z = 0; z < jumlah_pajak; z++) {
                        if (pjk_n.getJSONObject(z).getString("id_billing").trim().length() != 15) {
                            flag = 0;
                            break;
                        } else {
                            RCMPN = mpn2.MpnInquery(MpnConfiguration, pjk_n.getJSONObject(z).getString("id_billing").trim(), rek_asal);
                            if (RCMPN.getString("RESPONSECODE").equals("00") && (Integer.parseInt(RCMPN.getJSONObject("MPO").getString("BILL_AMOUNT")) == pjk_n.getJSONObject(z).getInt("nominal_pajak"))) {
                                flag = 1;
                            } else {
                                flag = 0;
                                break;
                            }
                        }

                    }
                    if (flag == 1) {
                        for (int z = 0; z < jumlah_pajak; z++) {
                            RCMPN = mpn2.MpnAll(MpnConfiguration, pjk_n.getJSONObject(z).getString("id_billing").trim(), rek_asal);
                            if (RCMPN.getString("RESPONSECODE").equals("00")) {
                                flag = 1;
                                isipajak = new JSONObject();
                                isipajak.put("id_billing", pjk_n.getJSONObject(z).getString("id_billing"));
                                isipajak.put("ntpn", RCMPN.getJSONObject("MPO").getString("NTPN"));
                                isipajak.put("tgl_ntpn", RCMPN.getJSONObject("MPO").getString("SETLEMENT_DATE"));
                                isipajak.put("ntb", RCMPN.getJSONObject("MPO").getString("NTB"));
                                pajak2.put(isipajak);
                            } else {
                                flag = 0;
                                break;
                            }
                        }
                    }
                } else {
                    flag = 1;
                }
                if (flag == 1) {
                    utility uli = new utility();
                    wl = wsr.CekNorekPenampungan(Integer.parseInt(idwilayah));
                    if (wl != null) {
                        //rek_tujuan = wl.getNomerRek();
                        rek_jamkes = wl.getNomerRekJamkes();
                        rek_iwp8 = wl.getNomerRekIwp8();
                        rek_iwp1 = wl.getNomerRekIwp1();
                        rek_jkk = wl.getNomerRekJkk();
                        rek_jkm = wl.getNomerRekJkm();
                        rek_tapera = wl.getNomerRekTapera();
                        rek_iubulog = wl.getNomerRekIuranBulog();
                        rek_iuzakat = wl.getNomerRekIuranZakat();
                        nama_daerah = wl.getNamadaerah();
                        //bank_tujuan = "110";
                        String Nar1 = uli.spaceRightPadding("SIPD pembayaran Non gaji ", 35);
                        String Nar2 = uli.spaceRightPadding(wl.getNamadaerah(), 35);
                        String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                        allnarasi = Nar1 + Nar2 + Nar3;
                    }
                    rsp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tujuan, rek_asal, amount, allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                    if (InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").has("potongan") && wl != null) {
                        JSONArray potongan = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("potongan");
                        for (int zk = 0; zk < potongan.length(); zk++) {
                            if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_wajib_pegawai_1persen") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
                                String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran wajib pegawai 1persen", 35);
                                String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                                String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                                allnarasi = Nar1 + Nar2 + Nar3;
                                String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iwp1, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                                transaksi_potongan Ctrp = new transaksi_potongan();
                                Ctrp.setJenis_potongan("iuran_wajib_pegawai_1persen");
                                Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
                                Ctrp.setNorek_sumber(rek_asal);
                                Ctrp.setRc(rspp.substring(0, 2));
                                Ctrp.setNorek_tujuan(rek_iwp1);
                                Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
                                if (rspp.trim().length() > 2) {
                                    Ctrp.setRefrence(rspp.substring(2, 14));
                                }
                                trp.save(Ctrp);
                            }
                            if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_wajib_pegawai_8persen") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
                                String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran wajib pegawai 8persen", 35);
                                String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                                String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                                allnarasi = Nar1 + Nar2 + Nar3;
                                String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iwp8, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                                transaksi_potongan Ctrp = new transaksi_potongan();
                                Ctrp.setJenis_potongan("iuran_wajib_pegawai_8persen");
                                Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
                                Ctrp.setNorek_sumber(rek_asal);
                                Ctrp.setRc(rspp.substring(0, 2));
                                Ctrp.setNorek_tujuan(rek_iwp8);
                                Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
                                if (rspp.trim().length() > 2) {
                                    Ctrp.setRefrence(rspp.substring(2, 14));
                                }
                                trp.save(Ctrp);
                            }
                            if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_jaminan_kesehatan") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
                                String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kesehatan", 35);
                                String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                                String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                                allnarasi = Nar1 + Nar2 + Nar3;
                                String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jamkes, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                                transaksi_potongan Ctrp = new transaksi_potongan();
                                Ctrp.setJenis_potongan("iuran_jaminan_kesehatan");
                                Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
                                Ctrp.setNorek_sumber(rek_asal);
                                Ctrp.setRc(rspp.substring(0, 2));
                                Ctrp.setNorek_tujuan(rek_jamkes);
                                Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
                                if (rspp.trim().length() > 2) {
                                    Ctrp.setRefrence(rspp.substring(2, 14));
                                }
                                trp.save(Ctrp);
                            }
                            if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_jaminan_kecelakaan_kerja") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
                                String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kecelakaan kerja", 35);
                                String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                                String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                                allnarasi = Nar1 + Nar2 + Nar3;
                                String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jkk, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                                transaksi_potongan Ctrp = new transaksi_potongan();
                                Ctrp.setJenis_potongan("iuran_jaminan_kecelakaan_kerja");
                                Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
                                Ctrp.setNorek_sumber(rek_asal);
                                Ctrp.setRc(rspp.substring(0, 2));
                                Ctrp.setNorek_tujuan(rek_jkk);
                                Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
                                if (rspp.trim().length() > 2) {
                                    Ctrp.setRefrence(rspp.substring(2, 14));
                                }
                                trp.save(Ctrp);
                            }
                            if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_jaminan_kematian") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
                                String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran jaminan kematian", 35);
                                String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                                String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                                allnarasi = Nar1 + Nar2 + Nar3;
                                String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_jkm, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                                transaksi_potongan Ctrp = new transaksi_potongan();
                                Ctrp.setJenis_potongan("iuran_jaminan_kematian");
                                Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
                                Ctrp.setNorek_sumber(rek_asal);
                                Ctrp.setRc(rspp.substring(0, 2));
                                Ctrp.setNorek_tujuan(rek_jkm);
                                Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
                                if (rspp.trim().length() > 2) {
                                    Ctrp.setRefrence(rspp.substring(2, 14));
                                }
                                trp.save(Ctrp);
                            }
                            if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_simpanan_tapera") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
                                String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran simpanan tapera", 35);
                                String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                                String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                                allnarasi = Nar1 + Nar2 + Nar3;
                                String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tapera, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                                transaksi_potongan Ctrp = new transaksi_potongan();
                                Ctrp.setJenis_potongan("iuran_simpanan_tapera");
                                Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
                                Ctrp.setNorek_sumber(rek_asal);
                                Ctrp.setRc(rspp.substring(0, 2));
                                Ctrp.setNorek_tujuan(rek_jkm);
                                Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
                                if (rspp.trim().length() > 2) {
                                    Ctrp.setRefrence(rspp.substring(2, 14));
                                }
                                trp.save(Ctrp);
                            }
                            if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_bulog") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
                                String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran_bulog", 35);
                                String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                                String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                                allnarasi = Nar1 + Nar2 + Nar3;
                                String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iubulog, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                                transaksi_potongan Ctrp = new transaksi_potongan();
                                Ctrp.setJenis_potongan("iuran_bulog");
                                Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
                                Ctrp.setNorek_sumber(rek_asal);
                                Ctrp.setRc(rspp.substring(0, 2));
                                Ctrp.setNorek_tujuan(rek_jkm);
                                Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
                                if (rspp.trim().length() > 2) {
                                    Ctrp.setRefrence(rspp.substring(2, 14));
                                }
                                trp.save(Ctrp);
                            }
                            if (potongan.getJSONObject(zk).getString("nama_potongan").equals("iuran_zakat") && potongan.getJSONObject(zk).getInt("nominal") > 0) {
                                String Nar1 = uli.spaceRightPadding("SIPD pembayaran iuran zakat", 35);
                                String Nar2 = uli.spaceRightPadding(nama_daerah, 35);
                                String Nar3 = uli.spaceRightPadding("AN " + uli.spaceRightPadding(nama_penerima, 12) + " NOREK " + InqRequest.getJSONObject("recipient_info").getString("account_number"), 35);
                                allnarasi = Nar1 + Nar2 + Nar3;
                                String rspp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_iuzakat, rek_asal, potongan.getJSONObject(zk).get("nominal").toString(), allnarasi, nama_penerima, alamat_penerima, ki, skncf);
                                transaksi_potongan Ctrp = new transaksi_potongan();
                                Ctrp.setJenis_potongan("iuran_zakat");
                                Ctrp.setNominal((String) potongan.getJSONObject(zk).get("nominal").toString());
                                Ctrp.setNorek_sumber(rek_asal);
                                Ctrp.setRc(rspp.substring(0, 2));
                                Ctrp.setNorek_tujuan(rek_jkm);
                                Ctrp.setTrx_patner_id(tl.get(i).getTx_partner_id());
                                if (rspp.trim().length() > 2) {
                                    Ctrp.setRefrence(rspp.substring(2, 14));
                                }
                                trp.save(Ctrp);
                            }
                        }
                    }
                    if (rsp.substring(0, 2).equals("00")) {
                        if (flag == 1) {
                            trn.setRefnum(rsp.substring(2, 14));
                            trn.setRespone_code("00");
                            trn.setStatus("1");
                            status.put("code", "000");
                            status.put("message", "Succes");

                        } else {
                            trn.setStatus("8");
                            trn.setRespone_code("00");
                            status.put("code", "300");
                            status.put("message", "Pending Transaction");
                        }
                    } else if (rsp.substring(0, 2).equals("51")) {

                        // isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
                        isipajak.put("id_billing", "");
                        isipajak.put("ntpn", "");
                        isipajak.put("tgl_ntpn", "");
                        isipajak.put("ntb", "");
                        pajak2.put(isipajak);
                        trn.setRespone_code("51");
                        trn.setStatus("4");
                        status.put("code", "203");
                        status.put("message", "Balance sender is not enough");
                    } else {
                        //JSONObject isipajak = new JSONObject();
                        trn.setStatus("4");
                        trn.setRespone_code(rsp.substring(0, 2));
                        status.put("code", "200");
                        status.put("message", "Failed to create transaction");
                        isipajak.put("id_billing", "");
                        isipajak.put("ntpn", "");
                        isipajak.put("tgl_ntpn", "");
                        isipajak.put("ntb", "");
                        pajak2.put(isipajak);
                    }
                } else {
                    trn.setStatus("4");
                    trn.setRespone_code("14");
                    status.put("code", "200");
                    status.put("message", "Failed to create transaction");
                    isipajak.put("id_billing", "");
                    isipajak.put("ntpn", "");
                    isipajak.put("tgl_ntpn", "");
                    isipajak.put("ntb", "");
                    pajak2.put(isipajak);
                }
                response.put("status", status);
                JSONObject adtionaldata = new JSONObject();
                if (!pajak2.isEmpty()) {
                    //System.out.println(pajak2.length());
                    if (status.getString("code").equals("100")) {
                        adtionaldata.put("pajak", pajak2);
                    } else {
                        adtionaldata.put("pajak", pajak2);
                    }
                } else {
                    adtionaldata.put("pajak", pajak2);
                }

                response.put("additional_data", adtionaldata.get("pajak"));
                date = new Date();
                ts = new Timestamp(date.getTime());
                trn.setUpdate_date(ts);
                trn.setResponse(response.toString());
                if (!trn.getStatus().equals("8")) {
                    Calbback cb = new Calbback();
                    String rsp_callbck = cb.callback_api(response, cbc);
                    if (rsp_callbck.equals("200")) {
                        trn.setStatus("1");
                    } else {
                        trn.setStatus("6");
                        // gagal callback
                    }
                }

                TR.save(trn);
                LOGGER.info("###Scheduler_Not_RealTime_Transaksi_NonGaji: " + trn.toString());
            }
        } else {
            LOGGER.info("###Scheduler_Not_RealTime_Transaksi_NonGaji: " + "DATA KOSONG UNTUK DIKESEKUSI");
            //System.out.println("DATA KOSONG");
        }
    }

//    @Scheduled(cron = "${cron.CallBack}")
//    public void scheduleTaskForCallbackFaild() {
//        Date date = new Date(System.currentTimeMillis());
//        LOGGER.info("###Scheduler_Call_Back_Response: " + formatter.format(date));
//        JSONObject response;
//        List<tarsansction> lt = TR.listTransaksiOnstatus("6");
//        if (lt.size() > 0) {
//            for (int i = 0; i < lt.size(); i++) {
//                tarsansction trn = lt.get(i);
//                response = new JSONObject(lt.get(i).getResponse());
//                Calbback cb = new Calbback();
//                String rsp_callbck = cb.callback_api(response, cbc);
//                if (rsp_callbck.equals("200")) {
//                    trn.setStatus("1");
//                } else {
//                    trn.setStatus("6");
//                    // gagal callback
//                }
//                TR.save(trn);
//                LOGGER.info("###Scheduler_Call_Back_Response: " + trn.toString());
//            }
//        } else {
//            LOGGER.info("###Scheduler_Call_Back_Response: " + "DATA KOSONG UNTUK DIKESEKUSI");
//        }
//    }
//    @Scheduled(cron = "${cron.PembayaranPajak}")
//    public void PembayaranPajak() {
//        Date date = new Date(System.currentTimeMillis());
//        LOGGER.info("###Scheduler_Call_Back_Pembayaran_Pajak " + formatter.format(date));
//        JSONObject response;
//        List<tarsansction> lt = TR.listTransaksiOnstatus("8");
//        if (lt.size() > 0) {
//            for (int i = 0; i < lt.size(); i++) {
//                tarsansction trn = lt.get(i);
//                response = new JSONObject(lt.get(i).getResponse());
//                if (response.getJSONObject("additional_data").has("pajak")) {
//                    response.put("additional_data", response.getJSONObject("additional_data").get("pajak"));
//                }
//                Calbback cb = new Calbback();
//                String rsp_callbck = cb.callback_api(response, cbc);
//                if (rsp_callbck.equals("200")) {
//                    trn.setStatus("1");
//                } else {
//                    trn.setStatus("6");
//                    // gagal callback
//                }
//                TR.save(trn);
//                LOGGER.info("###Scheduler_Call_Back_Response: " + trn.toString());
//            }
//        } else {
//            LOGGER.info("###Scheduler_Call_Back_Response: " + "DATA KOSONG UNTUK DIKESEKUSI");
//        }
//    }
    @Scheduled(cron = "${cron.Pembayaranpajak}")
    public void PembayaranPajak() {
        Date date = new Date(System.currentTimeMillis());
        LOGGER.info("###Scheduler_Pembayaran_pajak: " + formatter.format(date));
        JSONObject response;
        Djp in = new Djp();
        int flag = 0;
        List<tarsansction> lt = TR.listTransaksiOnstatus("8");
        if (lt.size() > 0) {
            List<String> nomerSp2d = new ArrayList<String>();
            String rsp, idbilling;
            JSONArray pajak2 = new JSONArray();
            for (int j = 0; j < lt.size(); j++) {
                JSONObject InqRequest = new JSONObject(lt.get(j).getRequest());
                String nosp2d = InqRequest.getJSONObject("tx_additional_data").getString("no_sp2d");
                if (!nomerSp2d.contains(nosp2d)) {
                    nomerSp2d.add(nosp2d);
                }
            }
            List<payroll_pajak> pnp;
            payroll_pajak_sum prp;
            List<Object> tes;
            String ntpn, ntb, tgl_ntpn;
            for (int k = 0; k < nomerSp2d.size(); k++) {
                tes = Payroll_Pjk.FindPajak(nomerSp2d.get(k));
                for (int a = 0; a < tes.size(); a++) {
                    Object[] row = (Object[]) tes.get(a);
                    System.out.println(Arrays.toString(row));
                    prp = new payroll_pajak_sum(Integer.parseInt(row[0].toString()), row[1].toString(), row[2].toString(), row[3].toString(), row[4].toString(), row[5].toString(), row[6].toString(), row[7].toString(), row[8].toString(), row[9].toString(), row[10].toString());
                    rsp = in.INpwp(eqConfiguration, prp.getNpwp(), prp.getMata_anggaran(), prp.getJenis_setoran());
                    JSONObject isipajak = new JSONObject();
                    idbilling = "";
                    ntpn = "";
                    tgl_ntpn = "";
                    ntb = "";
                    if (rsp.substring(0, 2).equals("00")) {
                        rsp = in.CreateIdbilling(eqConfiguration, String.valueOf(prp.getNominal()), prp.getNpwp(), prp.getMata_anggaran(), prp.getJenis_setoran(), prp.getMasapajak1(), prp.getMasapajak2(), prp.getTahunpajak(), prp.getNomersk(), prp.getNo_sp2d(), prp.getNop());

                        if (rsp.substring(0, 2).equals("00")) {
                            JSONObject Mpnrsp;
                            idbilling = rsp.substring(64, 79);
                            isipajak.put("id_billing", idbilling);
                            try {
                                //System.out.println(idbilling);
                                Thread.sleep(2000);
                            } catch (InterruptedException ex) {
                                Logger.getLogger(SchedulerCekTrx.class
                                        .getName()).log(Level.SEVERE, null, ex);
                            }
                            MPNPayment mpn = new MPNPayment();
                            Mpnrsp = mpn.MpnAll(MpnConfiguration, idbilling, prp.getRekening_asal());
                            if (Mpnrsp.getString("RC").equals("0000") || Mpnrsp.getString("RC").equals("0069")) {
                                ntpn = Mpnrsp.getJSONObject("MPO").getString("NTPN");
                                tgl_ntpn = Mpnrsp.getJSONObject("MPO").getString("SETLEMENT_DATE");
                                ntb = Mpnrsp.getJSONObject("MPO").getString("NTB");
                                pajak2.put(isipajak);
                            } else {
                                flag = 1;
                                // pajak2.put(isipajak);
                            }
                        } else {
                            flag = 1;
                            //pajak2.put(isipajak);
                        }
                    } else {
                        flag = 1;
                        //pajak2.put(isipajak);
                    }
                    Payroll_Pjk.updatebilling(idbilling, ntpn, ntb, tgl_ntpn, prp.getNo_sp2d(), prp.getNpwp(), prp.getMata_anggaran(), prp.getJenis_setoran(), prp.getMasapajak1(), prp.getMasapajak2(), prp.getTahunpajak(), prp.getNomersk(), prp.getNop(), prp.getRekening_asal());
                    pnp = Payroll_Pjk.FindWithSP2D(prp.getNo_sp2d(), prp.getNpwp(), prp.getMata_anggaran(), prp.getJenis_setoran(), prp.getMasapajak1(), prp.getMasapajak2(), prp.getTahunpajak(), prp.getNomersk(), prp.getNop(), prp.getRekening_asal());
                    for (int l = 0; l < pnp.size(); l++) {
                        tarsansction tnp = TR.findById(pnp.get(l).getTrx_id());
                        response = new JSONObject(tnp.getResponse());
                        isipajak = new JSONObject();
                        isipajak.put("id_billing", pnp.get(l).getId_billing());
                        isipajak.put("ntpn", pnp.get(l).getNtpn());
                        isipajak.put("tgl_ntpn", pnp.get(l).getTgl_ntpn());
                        isipajak.put("ntb", pnp.get(l).getNtb());
                        pajak2.put(isipajak);
                        response.put("additional_data", pajak2);
                        if (!pnp.get(l).getId_billing().isEmpty() && !pnp.get(l).getNtb().isEmpty() && tnp.getRespone_code().equals("00")) {
                            response.getJSONObject("status").put("code", "000");
                            response.getJSONObject("status").put("message", "Success");
                        } else {
                            response.getJSONObject("status").put("code", "300");
                            response.getJSONObject("status").put("message", "Pending Transaction");
                        }
                        Calbback cb = new Calbback();
                        String rsp_callbck = cb.callback_api(response, cbc);
                        if (rsp_callbck.equals("200")) {
                            tnp.setStatus("1");
                        } else {
                            tnp.setStatus("6");
                            // gagal callback
                        }
                        tnp.setResponse(response.toString());
                        TR.save(tnp);
                    }

                }
            }

        } else {
            LOGGER.info("###Scheduler_Pembayaran_pajak: " + "DATA KOSONG UNTUK DIKESEKUSI");
        }
    }

}
