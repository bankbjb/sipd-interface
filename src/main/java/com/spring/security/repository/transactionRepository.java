/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.repository;

import com.spring.security.model.tarsansction;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.spring.security.model.view_transaksi;
import org.springframework.data.jpa.repository.Modifying;

/**
 *
 * @author DELL
 */
@Repository
@Transactional
public interface transactionRepository extends JpaRepository<tarsansction, UUID> {

    public tarsansction findById(UUID fromString);

    @Query(value = "SELECT transaksi.* FROM transaksi WHERE transaksi.status <> '5' AND transaksi.created_date BETWEEN ?1 AND ?2 ORDER BY ?#{#pageable}", nativeQuery = true)
    List<tarsansction> listTransaksi(Timestamp date1, Timestamp date2, Pageable pageable);

    @Query(value = "SELECT transaksi.* FROM transaksi WHERE transaksi.tx_partner_id = ?1", nativeQuery = true)
    tarsansction TransaksiCek(String id_patner);

    @Query(value = "SELECT transaksi.* FROM transaksi WHERE transaksi.tx_partner_id = ?1 AND trx_id = ?2", nativeQuery = true)
    tarsansction TransaksiGet(String id_patner, UUID id);

    @Query(value = "SELECT transaksi.* FROM transaksi WHERE transaksi.real_time = false and exe_time <= ?1 AND transaksi.status = '2' AND type_trx <> 'LS|NONGAJI'", nativeQuery = true)
    List<tarsansction> listTransaksiPending(Timestamp date);

    @Query(value = "SELECT transaksi.* FROM transaksi WHERE transaksi.real_time = false and exe_time <= ?1 AND transaksi.status = '2' AND type_trx= 'LS|NONGAJI'", nativeQuery = true)
    List<tarsansction> listTransaksiPendingNonGaji(Timestamp date);

    @Query(value = "SELECT transaksi.* FROM transaksi WHERE transaksi.status = ?1", nativeQuery = true)
    List<tarsansction> listTransaksiOnstatus(String Status);
    
    @Query(value = "SELECT transaksi.* FROM transaksi WHERE transaksi.status = ?1 AND no_sp2d = ?2", nativeQuery = true)
    List<tarsansction> listTransaksiCallback(String Status,String no_sp2d);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE transaksi SET refnum = ?1, respone_code = ?2, status = ?3  WHERE no_sp2d = ?4 ", nativeQuery = true)
    void updateWithSp2d(String refnum, String respone_code, String status, String nosp2d);

    @Query(nativeQuery = true, value = "SELECT transaksi_not_real_time_group.* FROM transaksi_not_real_time_group")
    List<RepoViewGabungan> FindTransaksi();

    @Query(nativeQuery = true, value = "SELECT pajak_status.* FROM pajak_status where no_sp2d = ?1")
    List<RepoPotongan> FindPotongan(String Nosp2d);
    
    @Query(nativeQuery = true, value = "SELECT COUNT(no_sp2d) FROM transaksi WHERE no_sp2d = ?1 AND respone_code = '00'")
    String FindTransaksiCount(String Nosp2d);
    
    
////    @Query("SELECT "+"new com.spring.security.model.view_transaksi(GAJIH,IWP1,IWP8,JKS,JKK,JMK,TAPERA,ZAKAT,BULOG,total_data_Messaging,PPN,Nominal_SP2D,Norek,NO_SP2D,Kode_Wilayah,type_trx,exe_time)"+" FROM payroll_pajak WHERE status = 5 AND no_sp2d = ?1 GROUP BY no_sp2d,  npwp,  mata_anggaran,  jenis_setoran,  masapajak1,  masapajak2,  tahunpajak,  nomersk,  nop,rekening_asal")
//    List<view_transaksi> FindTransaksi();
}
