/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.repository;

import com.spring.security.model.WilayahIDSIPD;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author DELL
 */
public interface WilayahSIPDRepository extends JpaRepository<WilayahIDSIPD, Integer>{
    // public WilayahIDSIPD findById(Integer fromString);
     
     
    @Query(value="SELECT rek_penampungan.* FROM rek_penampungan WHERE rek_penampungan.id = ?1",nativeQuery = true)
    WilayahIDSIPD CekNorekPenampungan(Integer id_patner);
}
