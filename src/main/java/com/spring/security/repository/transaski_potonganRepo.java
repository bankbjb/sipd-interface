/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.repository;

import com.spring.security.model.tarsansction;
import com.spring.security.model.transaksi_potongan;
import java.util.List;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DELL
 */
@Repository
public interface transaski_potonganRepo extends JpaRepository<transaksi_potongan,UUID> {
    
    @Query(value = "SELECT transaksi_potongan.* FROM public.transaksi_potongan WHERE jenis_potongan IN ?1 AND trx_patner_id = ?2", nativeQuery = true)
    List<transaksi_potongan> TransaksiPotonganCek(List<String> potongan,String NoP2d);
    
}
