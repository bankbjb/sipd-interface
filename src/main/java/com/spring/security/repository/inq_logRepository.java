/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.repository;

import com.spring.security.model.inq_log;
import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

/**
 *
 * @author DELL
 */
public interface inq_logRepository extends JpaRepository<inq_log, UUID> {
    
    
    @Query(value="SELECT inq_log.* FROM inq_log WHERE inq_log.inquiry_partner_id = ?1",nativeQuery = true)
    inq_log GetPatnerId(String id_patner);
}
