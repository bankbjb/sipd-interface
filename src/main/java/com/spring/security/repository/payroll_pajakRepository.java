/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.repository;

import com.spring.security.model.payroll_pajak;
import com.spring.security.model.payroll_pajak_sum;
import com.spring.security.model.payroll_pajak_sum1;
import java.util.List;
import java.util.UUID;
import javax.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DELL
 */
@Transactional
@Repository
public interface payroll_pajakRepository extends JpaRepository<payroll_pajak, UUID> {

    @Query(value = "SELECT SUM(nominal) AS nominal,no_sp2d,  npwp,  mata_anggaran,  jenis_setoran,  masapajak1,  masapajak2,  tahunpajak,  nomersk,  nop,rekening_asal FROM payroll_pajak WHERE no_sp2d = ?1 GROUP BY no_sp2d,  npwp,  mata_anggaran,  jenis_setoran,  masapajak1,  masapajak2,  tahunpajak,  nomersk,  nop,rekening_asal", nativeQuery = true)
    List<Object> FindPajak(String NoSp2d);

    @Query("SELECT "+"new com.spring.security.model.payroll_pajak_sum1(SUM(nominal),no_sp2d,  npwp,  mata_anggaran,  jenis_setoran,  masapajak1,  masapajak2,  tahunpajak,  nomersk,  nop,rekening_asal)"+" FROM payroll_pajak WHERE no_sp2d = ?1 GROUP BY no_sp2d,  npwp,  mata_anggaran,  jenis_setoran,  masapajak1,  masapajak2,  tahunpajak,  nomersk,  nop,rekening_asal")
    List<payroll_pajak_sum1> FindPajak2(String NoSp2d);

    @Query(value = "Select * from payroll_pajak WHERE no_sp2d = ?1 AND npwp =?2 AND mata_anggaran = ?3 AND jenis_setoran = ?4 AND masapajak1 =?5 AND masapajak2 =?6 AND tahunpajak= ?7 AND nomersk =?8 AND nop =?9 AND rekening_asal = ?10", nativeQuery = true)
    List<payroll_pajak> FindWithSP2D(String nosp2d, String npwp, String mataanggran, String jenis_setoran, String masapajak1, String masapajak2, String tahunpajak, String nomersk, String nop, String rekening_asal);

    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE payroll_pajak SET id_billing = ?1, ntpn = ?2, ntb = ?3 , tgl_ntpn = ?4 WHERE no_sp2d = ?5 AND npwp =?6 AND mata_anggaran = ?7 AND jenis_setoran = ?8 AND masapajak1 =?9 AND masapajak2 =?10 AND tahunpajak= ?11 AND nomersk =?12 AND nop =?13 AND rekening_asal = ?14", nativeQuery = true)
    void updatebilling(String idbilling, String ntpn, String ntb, String tgl_ntpn, String nosp2d, String npwp, String mataanggran, String jenis_setoran, String masapajak1, String masapajak2, String tahunpajak, String nomersk, String nop, String rekening_asal);
    
    @Modifying(clearAutomatically = true)
    @Query(value = "UPDATE payroll_pajak SET id_billing = ?1, ntpn = ?2, ntb = ?3 , tgl_ntpn = ?4 WHERE no_sp2d = ?5 ", nativeQuery = true)
    void updateWithSp2d(String idbilling, String ntpn, String ntb, String tgl_ntpn, String nosp2d);
    
    @Query(value = "SELECT payroll_pajak.* FROM public.payroll_pajak WHERE no_sp2d = ?1 AND id_billing IS NOT NULL AND ntpn IS NOT NULL AND ntb IS NOT NULL AND tgl_ntpn IS NOT NULL", nativeQuery = true)
    List<payroll_pajak> FindCountNosp2d(String no_sp2d);
}
