/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.model;

import java.io.Serializable;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DELL
 */
@Entity
@Table(name = "inq_log")
public class inq_log implements Serializable {

    @Column(name = "kode_inquery")
    @Id
    UUID kode_inquery;
    @Column(name = "inquiry_partner_id")
    String inquiry_partner_id;
    @Column(name = "request")
    String request;
    @Column(name = "response")
    String response;

    public UUID getKode_inquery() {
        return kode_inquery;
    }

    public void setKode_inquery(UUID kode_inquery) {
        this.kode_inquery = kode_inquery;
    }

    public String getInquiry_partner_id() {
        return inquiry_partner_id;
    }

    public void setInquiry_partner_id(String inquiry_partner_id) {
        this.inquiry_partner_id = inquiry_partner_id;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

}
