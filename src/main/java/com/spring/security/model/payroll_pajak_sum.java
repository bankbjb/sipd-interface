/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.model;

import java.math.BigInteger;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author DELL
 */
public class payroll_pajak_sum {
    
    
    int nominal;
    String no_sp2d;
    String npwp;
    String mata_anggaran;
    String jenis_setoran;
    String masapajak1;
    String masapajak2;
    String tahunpajak;
    String nomersk;
    String nop;
    String rekening_asal;

    public payroll_pajak_sum(int nominal, String no_sp2d, String npwp, String mata_anggaran, String jenis_setoran, String masapajak1, String masapajak2, String tahunpajak, String nomersk, String nop, String rekening_asal) {
        this.nominal = nominal;
        this.no_sp2d = no_sp2d;
        this.npwp = npwp;
        this.mata_anggaran = mata_anggaran;
        this.jenis_setoran = jenis_setoran;
        this.masapajak1 = masapajak1;
        this.masapajak2 = masapajak2;
        this.tahunpajak = tahunpajak;
        this.nomersk = nomersk;
        this.nop = nop;
        this.rekening_asal = rekening_asal;
    }
  
    
    

    public String getRekening_asal() {
        return rekening_asal;
    }

    public void setRekening_asal(String rekening_asal) {
        this.rekening_asal = rekening_asal;
    }

    public String getMata_anggaran() {
        return mata_anggaran;
    }

    public void setMata_anggaran(String mata_anggaran) {
        this.mata_anggaran = mata_anggaran;
    }

    public String getJenis_setoran() {
        return jenis_setoran;
    }

    public void setJenis_setoran(String jenis_setoran) {
        this.jenis_setoran = jenis_setoran;
    }

    public String getMasapajak1() {
        return masapajak1;
    }

    public void setMasapajak1(String masapajak1) {
        this.masapajak1 = masapajak1;
    }

    public String getMasapajak2() {
        return masapajak2;
    }

    public void setMasapajak2(String masapajak2) {
        this.masapajak2 = masapajak2;
    }

    public String getTahunpajak() {
        return tahunpajak;
    }

    public void setTahunpajak(String tahunpajak) {
        this.tahunpajak = tahunpajak;
    }

    public String getNomersk() {
        return nomersk;
    }

    public void setNomersk(String nomersk) {
        this.nomersk = nomersk;
    }

    public String getNop() {
        return nop;
    }

    public void setNop(String nop) {
        this.nop = nop;
    }

    public int getNominal() {
        return nominal;
    }

    public void setNominal(int nominal) {
        this.nominal = nominal;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getNo_sp2d() {
        return no_sp2d;
    }

    public void setNo_sp2d(String no_sp2d) {
        this.no_sp2d = no_sp2d;
    }

}
