/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.spring.security.model;

import java.sql.Timestamp;

/**
 *
 * @author DELL
 */

public class view_transaksi  {

    Long gajih;
    Long iwp1;
    Long iwp8;
    Long jks;
    Long jkk;
    Long jmk;
    Long tapera;
    Long ppn;
    Long zakat;
    Long bulog;
    String no_sp2d;
    Long nominal_sp2d;
    int total_data_msg;
    int total_data_messaging;
    Long total_gaji;
    Timestamp exe_time;
    String status;
    String kode_wilayah;
    String type_trx;

    public view_transaksi(Long gajih, Long iwp1, Long iwp8, Long jks, Long jkk, Long jmk, Long tapera, Long ppn, Long zakat, Long bulog, String no_sp2d, Long nominal_sp2d, int total_data_msg, int total_data_messaging, Long total_gaji, Timestamp exe_time, String status, String kode_wilayah, String type_trx) {
        this.gajih = gajih;
        this.iwp1 = iwp1;
        this.iwp8 = iwp8;
        this.jks = jks;
        this.jkk = jkk;
        this.jmk = jmk;
        this.tapera = tapera;
        this.ppn = ppn;
        this.zakat = zakat;
        this.bulog = bulog;
        this.no_sp2d = no_sp2d;
        this.nominal_sp2d = nominal_sp2d;
        this.total_data_msg = total_data_msg;
        this.total_data_messaging = total_data_messaging;
        this.total_gaji = total_gaji;
        this.exe_time = exe_time;
        this.status = status;
        this.kode_wilayah = kode_wilayah;
        this.type_trx = type_trx;
    }

    public Long getGajih() {
        return gajih;
    }

    public Long getIwp1() {
        return iwp1;
    }

    public Long getIwp8() {
        return iwp8;
    }

    public Long getJks() {
        return jks;
    }

    public Long getJkk() {
        return jkk;
    }

    public Long getJmk() {
        return jmk;
    }

    public Long getTapera() {
        return tapera;
    }

    public Long getPpn() {
        return ppn;
    }

    public Long getZakat() {
        return zakat;
    }

    public Long getBulog() {
        return bulog;
    }

    public String getNo_sp2d() {
        return no_sp2d;
    }

    public Long getNominal_sp2d() {
        return nominal_sp2d;
    }

    public int getTotal_data_msg() {
        return total_data_msg;
    }

    public int getTotal_data_messaging() {
        return total_data_messaging;
    }

    public Long getTotal_gaji() {
        return total_gaji;
    }

    public Timestamp getExe_time() {
        return exe_time;
    }

    public String getStatus() {
        return status;
    }

    public String getKode_wilayah() {
        return kode_wilayah;
    }

    public String getType_trx() {
        return type_trx;
    }

   

}
