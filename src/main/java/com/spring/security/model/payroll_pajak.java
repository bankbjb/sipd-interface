/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.model;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DELL
 */
@Entity
@Table(name = "payroll_pajak")
public class payroll_pajak {

    @Column(name = "no_sp2d")
    String no_sp2d;
    @Id
    @Column(name = "trx_id")
    UUID trx_id;
    @Column(name = "id_billing")
    String id_billing;
    @Column(name = "ntpn")
    String ntpn;
    @Column(name = "ntb")
    String ntb;
    @Column(name = "nominal")
    Integer nominal;
    @Column(name = "npwp")
    String npwp;

    @Column(name = "mata_anggaran")
    String mata_anggaran;
    @Column(name = "jenis_setoran")
    String jenis_setoran;
    @Column(name = "masapajak1")
    String masapajak1;
    @Column(name = "masapajak2")
    String masapajak2;
    @Column(name = "tahunpajak")
    String tahunpajak;
    @Column(name = "nomersk")
    String nomersk;
    @Column(name = "nop")
    String nop;
    @Column(name = "rekening_asal")
    String rekening_asal;
    @Column(name = "tgl_ntpn")
    String tgl_ntpn;

    public String getTgl_ntpn() {
        return tgl_ntpn;
    }

    public void setTgl_ntpn(String tgl_ntpn) {
        this.tgl_ntpn = tgl_ntpn;
    }

    public String getRekening_asal() {
        return rekening_asal;
    }

    public void setRekening_asal(String rekening_asal) {
        this.rekening_asal = rekening_asal;
    }

    public String getMata_anggaran() {
        return mata_anggaran;
    }

    public void setMata_anggaran(String mata_anggaran) {
        this.mata_anggaran = mata_anggaran;
    }

    public String getJenis_setoran() {
        return jenis_setoran;
    }

    public void setJenis_setoran(String jenis_setoran) {
        this.jenis_setoran = jenis_setoran;
    }

    public String getMasapajak1() {
        return masapajak1;
    }

    public void setMasapajak1(String masapajak1) {
        this.masapajak1 = masapajak1;
    }

    public String getMasapajak2() {
        return masapajak2;
    }

    public void setMasapajak2(String masapajak2) {
        this.masapajak2 = masapajak2;
    }

    public String getTahunpajak() {
        return tahunpajak;
    }

    public void setTahunpajak(String tahunpajak) {
        this.tahunpajak = tahunpajak;
    }

    public String getNomersk() {
        return nomersk;
    }

    public void setNomersk(String nomersk) {
        this.nomersk = nomersk;
    }

    public String getNop() {
        return nop;
    }

    public void setNop(String nop) {
        this.nop = nop;
    }

    public Integer getNominal() {
        return nominal;
    }

    public void setNominal(Integer nominal) {
        this.nominal = nominal;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getNo_sp2d() {
        return no_sp2d;
    }

    public void setNo_sp2d(String no_sp2d) {
        this.no_sp2d = no_sp2d;
    }

    public UUID getTrx_id() {
        return trx_id;
    }

    public void setTrx_id(UUID trx_id) {
        this.trx_id = trx_id;
    }

    public String getId_billing() {
        return id_billing;
    }

    public void setId_billing(String id_billing) {
        this.id_billing = id_billing;
    }

    public String getNtpn() {
        return ntpn;
    }

    public void setNtpn(String ntpn) {
        this.ntpn = ntpn;
    }

    public String getNtb() {
        return ntb;
    }

    public void setNtb(String ntb) {
        this.ntb = ntb;
    }

}
