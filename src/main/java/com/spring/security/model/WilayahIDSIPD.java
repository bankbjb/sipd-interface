/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DELL
 */
@Entity
@Table(name = "rek_penampungan")
public class WilayahIDSIPD implements Serializable {

    @Id
    @Column(name = "id")
    Integer id;

    @Column(name = "nomerrek_penampungan_gajih")
    String NomerRek;

    @Column(name = "namarek")
    String NamaRek;

    @Column(name = "namadaerah")
    String namadaerah;

    @Column(name = "nomerrek_jamkes")
    String NomerRekJamkes;

    @Column(name = "nomerrek_iwp8")
    String NomerRekIwp8;

    @Column(name = "nomerrek_iwp1")
    String NomerRekIwp1;

    @Column(name = "nomerrek_jkk")
    String NomerRekJkk;

    @Column(name = "nomerrek_jkm")
    String NomerRekJkm;

    @Column(name = "nomerrek_tapera")
    String NomerRekTapera;

    @Column(name = "nomerrek_iubulog")
    String NomerRekIuranBulog;

    @Column(name = "nomerrek_iuzakat")
    String NomerRekIuranZakat;

    public String getNomerRekIuranBulog() {
        return NomerRekIuranBulog;
    }

    public void setNomerRekIuranBulog(String NomerRekIuranBulog) {
        this.NomerRekIuranBulog = NomerRekIuranBulog;
    }

    public String getNomerRekIuranZakat() {
        return NomerRekIuranZakat;
    }

    public void setNomerRekIuranZakat(String NomerRekIuranZakat) {
        this.NomerRekIuranZakat = NomerRekIuranZakat;
    }

    public String getNomerRekTapera() {
        return NomerRekTapera;
    }

    public void setNomerRekTapera(String NomerRekTapera) {
        this.NomerRekTapera = NomerRekTapera;
    }

    public String getNomerRekJamkes() {
        return NomerRekJamkes;
    }

    public void setNomerRekJamkes(String NomerRekJamkes) {
        this.NomerRekJamkes = NomerRekJamkes;
    }

    public String getNomerRekIwp8() {
        return NomerRekIwp8;
    }

    public void setNomerRekIwp8(String NomerRekIwp8) {
        this.NomerRekIwp8 = NomerRekIwp8;
    }

    public String getNomerRekIwp1() {
        return NomerRekIwp1;
    }

    public void setNomerRekIwp1(String NomerRekIwp1) {
        this.NomerRekIwp1 = NomerRekIwp1;
    }

    public String getNomerRekJkk() {
        return NomerRekJkk;
    }

    public void setNomerRekJkk(String NomerRekJkk) {
        this.NomerRekJkk = NomerRekJkk;
    }

    public String getNomerRekJkm() {
        return NomerRekJkm;
    }

    public void setNomerRekJkm(String NomerRekJkm) {
        this.NomerRekJkm = NomerRekJkm;
    }

    public String getNamadaerah() {
        return namadaerah;
    }

    public void setNamadaerah(String namadaerah) {
        this.namadaerah = namadaerah;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNomerRek() {
        return NomerRek;
    }

    public void setNomerRek(String NomerRek) {
        this.NomerRek = NomerRek;
    }

    public String getNamaRek() {
        return NamaRek;
    }

    public void setNamaRek(String NamaRek) {
        this.NamaRek = NamaRek;
    }

}
