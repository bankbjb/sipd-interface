/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DELL
 */
@Entity
@Table(name = "transaksi")
public class tarsansction implements Serializable {

    @Column(name = "tx_partner_id")
    String tx_partner_id;

    @Id
    @Column(name = "trx_id")
    UUID id;

    @Column(name = "created_date")
    Timestamp created_date;

    @Column(name = "update_date")
    Timestamp update_date;

    @Column(name = "status")
    String status;

    @Column(name = "real_time")
    boolean real_time;

    @Column(name = "exe_time")
    Timestamp exe_time;

    @Column(name = "request")
    String request;

    @Column(name = "response")
    String response;

    @Column(name = "respone_code")
    String respone_code;

    @Column(name = "refnum")
    String refnum;

    @Column(name = "no_sp2d")
    String no_sp2d;

    @Column(name = "type_trx")
    String type_trx;

    public String getNo_sp2d() {
        return no_sp2d;
    }

    public void setNo_sp2d(String no_sp2d) {
        this.no_sp2d = no_sp2d;
    }

    public String getType_trx() {
        return type_trx;
    }

    public void setType_trx(String type_trx) {
        this.type_trx = type_trx;
    }

    public String getTx_partner_id() {
        return tx_partner_id;
    }

    public void setTx_partner_id(String tx_partner_id) {
        this.tx_partner_id = tx_partner_id;
    }

    public UUID getTrx_id() {
        return id;
    }

    public void setTrx_id(UUID trx_id) {
        this.id = trx_id;
    }

    public Timestamp getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Timestamp created_date) {
        this.created_date = created_date;
    }

    public Timestamp getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Timestamp update_date) {
        this.update_date = update_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isReal_time() {
        return real_time;
    }

    public void setReal_time(boolean real_time) {
        this.real_time = real_time;
    }

    public Timestamp getExe_time() {
        return exe_time;
    }

    public void setExe_time(Timestamp exe_time) {
        this.exe_time = exe_time;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getRespone_code() {
        return respone_code;
    }

    public void setRespone_code(String respone_code) {
        this.respone_code = respone_code;
    }

    public String getRefnum() {
        return refnum;
    }

    public void setRefnum(String refnum) {
        this.refnum = refnum;
    }

}
