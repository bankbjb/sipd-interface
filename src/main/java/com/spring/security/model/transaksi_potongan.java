/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.model;

import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DELL
 */
@Entity
@Table(name = "transaksi_potongan")
public class transaksi_potongan {
    @Id
    @GeneratedValue
    UUID id_transaksi;
    String norek_sumber;
    String norek_tujuan;
    String refrence;
    String jenis_potongan;
    String rc;
    String trx_patner_id;
    String nominal;


    public String getNorek_sumber() {
        return norek_sumber;
    }

    public void setNorek_sumber(String norek_sumber) {
        this.norek_sumber = norek_sumber;
    }

    public String getNorek_tujuan() {
        return norek_tujuan;
    }

    public void setNorek_tujuan(String norek_tujuan) {
        this.norek_tujuan = norek_tujuan;
    }

    public String getRefrence() {
        return refrence;
    }

    public void setRefrence(String refrence) {
        this.refrence = refrence;
    }

    public String getJenis_potongan() {
        return jenis_potongan;
    }

    public void setJenis_potongan(String jenis_potongan) {
        this.jenis_potongan = jenis_potongan;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }

    public String getTrx_patner_id() {
        return trx_patner_id;
    }

    public void setTrx_patner_id(String trx_patner_id) {
        this.trx_patner_id = trx_patner_id;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }
    
    
    
}
