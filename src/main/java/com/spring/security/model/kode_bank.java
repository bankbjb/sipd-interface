/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author DELL
 */
@Entity
@Table(name = "kode_bank")
public class kode_bank implements Serializable {

    @GeneratedValue
    @Column(name = "id")
    @Id
    int id;
    @Column(name = "nama_bank")
    String nama_bank;
    @Column(name = "kode_bank_online")
    String kode_bank_online;
    @Column(name = "kode_bank_rtgs")
    String kode_bank_rtgs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama_bank() {
        return nama_bank;
    }

    public void setNama_bank(String nama_bank) {
        this.nama_bank = nama_bank;
    }

    public String getKode_bank_online() {
        return kode_bank_online;
    }

    public void setKode_bank_online(String kode_bank_online) {
        this.kode_bank_online = kode_bank_online;
    }

    public String getKode_bank_rtgs() {
        return kode_bank_rtgs;
    }

    public void setKode_bank_rtgs(String kode_bank_rtgs) {
        this.kode_bank_rtgs = kode_bank_rtgs;
    }

}
