/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;
import com.ibm.as400.access.QSYSObjectPathName;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author DELL
 */
public class Eqhandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(Eqhandler.class);
    
    public String eqHandler(String alamat_ip,String user_eq,String password_eq,String nama_program, AS400Text input,AS400Text output, String input_paramter,int panjang) throws AS400SecurityException, ErrorCompletingRequestException, IOException, InterruptedException, PropertyVetoException, ObjectDoesNotExistException {
        String OUTPUT = "";
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        AS400 as400System = new AS400(alamat_ip, user_eq, password_eq);
        ProgramCall program = new ProgramCall(as400System);
        CommandCall command = new CommandCall(as400System);

        // Initialize the name of the program to run.
        if (command.run("addlible SWITCHLIB") != true) {
            System.out.println("command failed");
        }
//        if (command.run("addlible IF400N") != true) {
        if (command.run("addlible IF400N") != true) {
            System.out.println("command failed");
        }
        //System.out.println("ajsdklas");
        AS400Message[] messagelist = command.getMessageList();
        for (int i = 0; i < messagelist.length; ++i) {
            // Show each message.
            System.out.println(messagelist[i].getText());
        }
        ProgramParameter[] parameterList = new ProgramParameter[2];
        QSYSObjectPathName programName = new QSYSObjectPathName("SWITCHLIB", nama_program, "PGM");
        parameterList[0] = new ProgramParameter(input.toBytes(input_paramter), panjang);
        parameterList[1] = new ProgramParameter(999);
        program.setProgram(programName.getPath(), parameterList);
        LOGGER.info("###["+formatter.format(date)+"]EQ_THREAD "+nama_program+" REQUEST "+ input_paramter);
        if (program.run() != true) {
            // Report failure.
            //SystemLog.getSingleton().log(this, LogType.ERROR, "Program failed!");
            //System.out.println("Program failed!");
            // Show the messages.
            AS400Message[] messageList = program.getMessageList();
            for (int i = 0; i < messageList.length; ++i) {
                // Show each message.
                //System.out.println();
                //SystemLog.getSingleton().log(this, LogType.TRACE, messageList[i].getText());
                // Load additional message information.
                messageList[i].load();
                //Show help text.
                // System.out.println(messageList[i].getHelp());
                //SystemLog.getSingleton().log(this, LogType.TRACE, messageList[i].getHelp());
            }
        } // Else no error, get output data.
        else {
            //OUTPUT1 = new AS400Text(500);
            OUTPUT = (String) output.toObject(parameterList[1].getOutputData());
            LOGGER.info("###["+formatter.format(date)+"]EQ_THREAD "+nama_program+" RESPONSE "+ OUTPUT);
            //SystemLog.getSingleton().log(this, LogType.STREAM, OUTPUT);

        }
        as400System.disconnectAllServices();

        return OUTPUT;
    }
}
