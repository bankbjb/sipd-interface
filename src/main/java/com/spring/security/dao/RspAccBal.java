package com.spring.security.dao;

import com.fasterxml.jackson.annotation.JsonProperty;

// AB Response
public class RspAccBal {
	
	private String NOREK,RC,ST,SALDO,CURENCY;
    private String NAMA_LENGKAP;
    private String NAMA_PENDEK;
    
    public RspcodeMsg rcmsg;
    
    public RspAccBal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RspcodeMsg getRcmsg() {
		return rcmsg;
	}

	public void setRcmsg(RspcodeMsg rcmsg) {
		this.rcmsg = rcmsg;
	}

	@JsonProperty("nama_lengkap")
    public String getNAMA_LENGKAP() {
        return NAMA_LENGKAP;
    }

    public void setNAMA_LENGKAP(String NAMA_LENGKAP) {
        this.NAMA_LENGKAP = NAMA_LENGKAP;
    }
    
    @JsonProperty("nama_pendek")
    public String getNAMA_PENDEK() {
        return NAMA_PENDEK;
    }

    public void setNAMA_PENDEK(String NAMA_PENDEK) {
        this.NAMA_PENDEK = NAMA_PENDEK;
    }

    public String getSALDO() {
        return SALDO;
    }

    public void setSALDO(String SALDO) {
        this.SALDO = SALDO;
    }

    public String getCURENCY() {
        return CURENCY;
    }

    public void setCURENCY(String CURENCY) {
        this.CURENCY = CURENCY;
    }

    public String getNOREK() {
        return NOREK;
    }

    public void setNOREK(String NOREK) {
        this.NOREK = NOREK;
    }

    public String getRC() {
        return RC;
    }

    public void setRC(String RC) {
        this.RC = RC;
    }

    public String getST() {
        return ST;
    }

    public void setST(String ST) {
        this.ST = ST;
    }

	@Override
	public String toString() {
		return "RspAccBal [NOREK=" + NOREK + ", RC=" + RC + ", ST=" + ST + ", SALDO=" + SALDO + ", CURENCY=" + CURENCY
				+ ", NAMA_LENGKAP=" + NAMA_LENGKAP + ", NAMA_PENDEK=" + NAMA_PENDEK + ", rcmsg=" + rcmsg + "]";
	}
}
