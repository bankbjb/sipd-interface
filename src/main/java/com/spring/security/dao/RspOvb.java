package com.spring.security.dao;

// ITA Response
public class RspOvb {
	
	private String Referensi ,rekCredit ,rekDebit ,Narasi1 ,Narasi2 ,Narasi3 ,Narasi4 ,CdDbt ,CdCrd ;
	private String nominal,st,rc ;
	private RspcodeMsg rcmsg;
	
	public RspOvb() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getRc() {
	    return rc;
	}
	
	public void setRc(String rc) {
	    this.rc = rc;
	}
	
	public RspcodeMsg getRcmsg() {
	    return rcmsg;
	}
	
	public void setRcmsg(RspcodeMsg rcmsg) {
	    this.rcmsg = rcmsg;
	}
	
	public String getReferensi() {
	    return Referensi;
	}
	
	public void setReferensi(String Referensi) {
	    this.Referensi = Referensi;
	}
	
	public String getRekCredit() {
	    return rekCredit;
	}
	
	public void setRekCredit(String rekCredit) {
	    this.rekCredit = rekCredit;
	}
	
	public String getRekDebit() {
	    return rekDebit;
	}
	
	public void setRekDebit(String rekDebit) {
	    this.rekDebit = rekDebit;
	}
	
	public String getNarasi1() {
	    return Narasi1;
	}
	
	public void setNarasi1(String Narasi1) {
	    this.Narasi1 = Narasi1;
	}
	
	public String getNarasi2() {
	    return Narasi2;
	}
	
	public void setNarasi2(String Narasi2) {
	    this.Narasi2 = Narasi2;
	}
	
	public String getNarasi3() {
	    return Narasi3;
	}
	
	public void setNarasi3(String Narasi3) {
	    this.Narasi3 = Narasi3;
	}
	
	public String getNarasi4() {
	    return Narasi4;
	}
	
	public void setNarasi4(String Narasi4) {
	    this.Narasi4 = Narasi4;
	}
	
	public String getCdDbt() {
	    return CdDbt;
	}
	
	public void setCdDbt(String CdDbt) {
	    this.CdDbt = CdDbt;
	}
	
	public String getCdCrd() {
	    return CdCrd;
	}
	
	public void setCdCrd(String CdCrd) {
	    this.CdCrd = CdCrd;
	}
	
	public String getNominal() {
	    return nominal;
	}
	
	public void setNominal(String nominal) {
	    this.nominal = nominal;
	}
	
	public String getSt() {
	    return st;
	}
	
	public void setSt(String st) {
	    this.st = st;
	}

	@Override
	public String toString() {
		return "RspOvb [Referensi=" + Referensi + ", rekCredit=" + rekCredit + ", rekDebit=" + rekDebit
				+ ", Narasi1=" + Narasi1 + ", Narasi2=" + Narasi2 + ", Narasi3=" + Narasi3 + ", Narasi4=" + Narasi4
				+ ", CdDbt=" + CdDbt + ", CdCrd=" + CdCrd + ", nominal=" + nominal + ", st=" + st + ", rc=" + rc
				+ ", rcmsg=" + rcmsg + "]";
	}
	
}
