/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import com.spring.security.configuration.MpnConf;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author DELL
 */
public class MPNPayment {
    public JSONObject MpnInquery(MpnConf ConFMpn,String Billid,String rekSumber){
        JSONObject MPN = new JSONObject();
        JSONObject MPI = new JSONObject();
        JSONObject MPNRsp = new JSONObject();
        MPN.put("PAN", rekSumber.trim());
        MPN.put("AQC","000");
        MPN.put("CAL", "SIPD");
        MPN.put("REMOTE-IP", "10.6.226.214");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        MPN.put("DT", formatter.format(date));
        MPN.put("CNM", "622011201001001362");
        MPN.put("MT", "2100");
        MPN.put("CID",ConFMpn.getCid());
        MPN.put("SID", "SIPD");
        SimpleDateFormat formatter2 = new SimpleDateFormat("ddHHmmsssss");
        date = new Date(System.currentTimeMillis());
        MPN.put("ST", formatter2.format(date));
        MPI.put("CR", "IDR");
        MPI.put("AMOUNT", "0");
        MPI.put("BILLID", Billid);
        MPI.put("USER", "SIPD");
        MPN.put("MPI", MPI);
        MPN.put("TID", "0001");
        MPN.put("CTY", "201");
        MPN.put("BC", "110");
        MPN.put("CC", "6020");
        MPN.put("FWC", "110");
        MPN.put("PIN","000001");
        MPN.put("DTR", "1234567890123456789012345678901234567");
        MPN.put("MC","03021");
        MPN.put("TNM", "02A002");
        MPN.put("PC", "301100");
        
        MpnCon mpnc = new MpnCon();
        MPNRsp = mpnc.ConnectServerSocket("Inquery_MPN", ConFMpn.getIp(),ConFMpn.getPort(), MPN, ConFMpn.getTimeout());
        return MPNRsp;
    }
    
    public JSONObject MpnPayment(MpnConf ConFMpn,JSONObject MpnRsp,String rekSumber){
        JSONObject MPN = new JSONObject();
        JSONObject MPI = new JSONObject();
        JSONObject MPNRspP = new JSONObject();
        MPN.put("PAN", rekSumber.trim());
        MPN.put("AQC","000");
        MPN.put("CAL", "SIPD");
        MPN.put("REMOTE-IP", "10.6.226.214");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        MPN.put("DT", formatter.format(date));
        MPN.put("CNM", "622011201001001362");
        MPN.put("MT", "2200");
        MPN.put("CID",ConFMpn.getCid());
        MPN.put("SID", "SIPD");
        MPN.put("GWSTAN", MpnRsp.getJSONObject("MPO").getString("STAN"));
        SimpleDateFormat formatter2 = new SimpleDateFormat("ddHHmmsssss");
        date = new Date(System.currentTimeMillis());
        MPN.put("ST", formatter2.format(date));
        MPN.put("MPI", MpnRsp.getJSONObject("MPO"));
        MPN.put("TID", "0001");
        MPN.put("CTY", "201");
        MPN.put("BC", "110");
        MPN.put("CC", "6020");
        MPN.put("FWC", "110");
        MPN.put("PIN","000001");
        MPN.put("DTR", "1234567890123456789012345678901234567");
        MPN.put("MC","03021");
        MPN.put("TNM", "02A002");
        MPN.put("MPR", MpnRsp.getString("MPR"));
        MPN.put("SPPU", "SIPD");
        MPN.put("RF",MpnRsp.getString("RF"));
        MPN.put("PC", "500100");
        
        MpnCon mpnc = new MpnCon();
        MPNRspP = mpnc.ConnectServerSocket("Payment_MPN", ConFMpn.getIp(),ConFMpn.getPort(), MPN, ConFMpn.getTimeout());
        return MPNRspP;
    }
    
    public JSONObject MpnAll(MpnConf ConFMpn,String Billid,String rekSumber){
        JSONObject MPNRsp;
        JSONObject MPNRspP;
        MPNRsp = this.MpnInquery(ConFMpn, Billid, rekSumber);
        if(MPNRsp.getString("RESPONSECODE").equals("00")){
            //MPNRsp = null;
            MPNRspP = this.MpnPayment(ConFMpn, MPNRsp, rekSumber);
            return MPNRspP;
        }else{
            return MPNRsp;
        }
    }
}
