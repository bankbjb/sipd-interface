package com.spring.security.dao;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.spring.security.configuration.GapuraConfiguration;

public class ReqRspOvb {
	
    private final ObjectMapper mapper;
    GSSConnection gssConn = new GSSConnection();
    byte EndMsg = 0x00;

   public ReqRspOvb() {
       this.mapper = new ObjectMapper();
       mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
   }
    
   private JSONObject ITAValidation_inq(ReqOvb ita, GapuraConfiguration gapuraConf) throws JsonProcessingException, IOException{
      
       JSONObject ReqITAValidation,RspITAValidation,MPI;
       ReqITAValidation = new JSONObject();
       MPI = new JSONObject();
       MPI.put("ZLDRF1", ita.getReferensi().length()>15 ? ita.getReferensi().substring(15) : ita.getReferensi());
       MPI.put("ZLEAN1", ita.getRekCredit().length()>20 ? ita.getRekCredit().substring(20) : ita.getRekCredit());
       MPI.put("ZLEAN", ita.getRekDebit().length()>20 ? ita.getRekDebit().substring(15) : ita.getRekDebit());
       MPI.put("ZLDRF2", ita.getReferensi().length()>15 ? ita.getReferensi().substring(15) : ita.getReferensi());
       MPI.put("ZLNR8", ita.getNarasi4().length()>35 ? ita.getNarasi4().substring(35) : ita.getNarasi4());
       MPI.put("ZLNR7", ita.getNarasi3().length()>35 ? ita.getNarasi3().substring(35) : ita.getNarasi3());
       MPI.put("ZLNR6", ita.getNarasi2().length()>35 ? ita.getNarasi2().substring(35) : ita.getNarasi2());
       MPI.put("ZLTCD1", ita.getCdDbt().length()>3 ? ita.getCdDbt().substring(3) : ita.getCdDbt());
       MPI.put("ZLTCD2", ita.getCdCrd().length()>3 ? ita.getCdCrd().substring(3) : ita.getCdCrd());
       MPI.put("ZLREF", ita.getReferensi().length()>15 ? ita.getReferensi().substring(15) : ita.getReferensi());
       MPI.put("ZLAMZ1", ita.getNominal().length()>15 ? ita.getReferensi().substring(15) : ita.getNominal());
       MPI.put("ZLNR3", ita.getNarasi3().length()>35 ? ita.getNarasi3().substring(35) : ita.getNarasi3());
       MPI.put("ZLNR2", ita.getNarasi2().length()>35 ? ita.getNarasi2().substring(35) : ita.getNarasi2());
       MPI.put("ZLNR5", ita.getNarasi1().length()>35 ? ita.getNarasi1().substring(35) : ita.getNarasi1());
       MPI.put("ZLNR4", ita.getNarasi4().length()>35 ? ita.getNarasi4().substring(35) : ita.getNarasi4());
       MPI.put("ZLNR1", ita.getNarasi1().length()>35 ? ita.getNarasi1().substring(35) : ita.getNarasi1());
       ReqITAValidation.put("BRANCHCD", "0000");
       ReqITAValidation.put("PCC", "1");
       ReqITAValidation.put("FC", "ITA");
       SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
       Date date = new Date(System.currentTimeMillis());
       ReqITAValidation.put("DT", formatter.format(date));
       ReqITAValidation.put("AUDITUID", "");
       ReqITAValidation.put("MT", "9200");
       ReqITAValidation.put("CID", gapuraConf.getCid());
       ReqITAValidation.put("SID", "singleUserIDWebTeller");
       ReqITAValidation.put("MPI", MPI);
       ReqITAValidation.put("ST", ita.getSt());
       ReqITAValidation.put("CC", "0001");
       ReqITAValidation.put("SPPW",gapuraConf.getSppw());
       ReqITAValidation.put("MC","90023");
       ReqITAValidation.put("SPPU",gapuraConf.getSppu());
       ReqITAValidation.put("PC","001002");
       RspITAValidation = gssConn.ConnectServerSocket("GAPURA-ITA-VALIDATION", gapuraConf.getIp() ,gapuraConf.getPort(), ReqITAValidation, gapuraConf.getTimeout(), EndMsg, EndMsg);
       return RspITAValidation;

   }
   
   private JSONObject ITACreate(ReqOvb ita, GapuraConfiguration gapuraConf) throws JsonProcessingException, IOException{

       JSONObject ReqITACreation,RspITACreation , MPI;
       ReqITACreation = new JSONObject();
       MPI = new JSONObject();
       MPI.put("ZLDRF1", ita.getReferensi().length()>15 ? ita.getReferensi().substring(15) : ita.getReferensi());
       MPI.put("ZLEAN1", ita.getRekCredit().length()>20 ? ita.getRekCredit().substring(20) : ita.getRekCredit());
       MPI.put("ZLEAN", ita.getRekDebit().length()>20 ? ita.getRekDebit().substring(15) : ita.getRekDebit());
       MPI.put("ZLDRF2", ita.getReferensi().length()>15 ? ita.getReferensi().substring(15) : ita.getReferensi());
       MPI.put("ZLNR8", ita.getNarasi4().length()>35 ? ita.getNarasi4().substring(35) : ita.getNarasi4());
       MPI.put("ZLNR7", ita.getNarasi3().length()>35 ? ita.getNarasi3().substring(35) : ita.getNarasi3());
       MPI.put("ZLNR6", ita.getNarasi2().length()>35 ? ita.getNarasi2().substring(35) : ita.getNarasi2());
       MPI.put("ZLTCD1", ita.getCdDbt().length()>3 ? ita.getCdDbt().substring(3) : ita.getCdDbt());
       MPI.put("ZLTCD2", ita.getCdCrd().length()>3 ? ita.getCdCrd().substring(3) : ita.getCdCrd());
       MPI.put("ZLREF", ita.getReferensi().length()>15 ? ita.getReferensi().substring(15) : ita.getReferensi());
       MPI.put("ZLAMZ1", ita.getNominal().length()>15 ? ita.getReferensi().substring(15) : ita.getNominal());
       MPI.put("ZLNR3", ita.getNarasi3().length()>35 ? ita.getNarasi3().substring(35) : ita.getNarasi3());
       MPI.put("ZLNR2", ita.getNarasi2().length()>35 ? ita.getNarasi2().substring(35) : ita.getNarasi2());
       MPI.put("ZLNR5", ita.getNarasi1().length()>35 ? ita.getNarasi1().substring(35) : ita.getNarasi1());
       MPI.put("ZLNR4", ita.getNarasi4().length()>35 ? ita.getNarasi4().substring(35) : ita.getNarasi4());
       MPI.put("ZLNR1", ita.getNarasi1().length()>35 ? ita.getNarasi1().substring(35) : ita.getNarasi1());
       ReqITACreation.put("BRANCHCD", "");
       ReqITACreation.put("PCC", "2");
       ReqITACreation.put("FC", "ITA");
       SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
       Date date = new Date(System.currentTimeMillis());
       ReqITACreation.put("DT", formatter.format(date));
       ReqITACreation.put("AUDITUID", "");
       ReqITACreation.put("MT", "9200");
       ReqITACreation.put("CID", gapuraConf.getCid());
       ReqITACreation.put("SID", "singleUserIDWebTeller");
       ReqITACreation.put("MPI", MPI);
       ReqITACreation.put("ST", ita.getSt());
       ReqITACreation.put("CC", "0001");
       ReqITACreation.put("SPPW", gapuraConf.getSppw());
       ReqITACreation.put("MC","90023");
       ReqITACreation.put("SPPU", gapuraConf.getSppu());
       ReqITACreation.put("PC","001002");
       RspITACreation = gssConn.ConnectServerSocket("GAPURA-ITA-CREATION", gapuraConf.getIp() ,gapuraConf.getPort(), ReqITACreation, gapuraConf.getTimeout(), EndMsg, EndMsg);
       return RspITACreation;

   }
   
   public RspOvb ItaRsp(ReqOvb ita, GapuraConfiguration gapuraConf) throws IOException{
       RspOvb ItaRsp1 = new RspOvb();
       JSONObject ITAValidationRsp, ITACreationRsp;
       BeanUtils.copyProperties(ita, ItaRsp1);
       JSONObject RCMSG = new JSONObject("{\n"
               + "        \"0\": {\n"
               + "            \"IS_OVERRIDE\": \"1\",\n"
               + "            \"AUTHORITY\": \"  \",\n"
               + "            \"MESSAGE_ID\": \"KSM2010\",\n"
               + "            \"FIELD_IN_ERROR\": \"KSM2010\",\n"
               + "            \"SEVERITY_STATUS\": \" \",\n"
               + "            \"MESSAGE\": \"KSM2010   does not exist on the database\",\n"
               + "            \"DESCRIPTION\": \"KSM2010   does not exist on the database\",\n"
               + "            \"SEVERITY\": \"20\"\n"
               + "        }\n"
               + "    }");
       if(!ita.isValid()){
           mapper.enable(SerializationFeature.INDENT_OUTPUT);
           mapper.setPropertyNamingStrategy(new MyPropertyNamingStrategy());
           ItaRsp1.setRcmsg(mapper.readValue(RCMSG.getJSONObject("0").toString(), new TypeReference<RspcodeMsg>() {}));
           ItaRsp1.setRc("14");
           return ItaRsp1;
       }
       ITAValidationRsp = ITAValidation_inq(ita, gapuraConf);
       mapper.enable(SerializationFeature.INDENT_OUTPUT);
       mapper.setPropertyNamingStrategy(new MyPropertyNamingStrategy());
       ItaRsp1.setRcmsg(mapper.readValue(ITAValidationRsp.getJSONObject("RCMSG").getJSONObject("0").toString(), new TypeReference<RspcodeMsg>() {}));
       ItaRsp1.setRc(ITAValidationRsp.getString("RC"));
       ItaRsp1.setSt(ITAValidationRsp.getString("ST"));
       
       if(ITAValidationRsp.getString("RC").equals("0000") || (ITAValidationRsp.getString("RC").equals("0099") && ITAValidationRsp.getJSONObject("RCMSG").getJSONObject("0").getString("SEVERITY").equals("10") && (ITAValidationRsp.getJSONObject("RCMSG").getJSONObject("0").getString("AUTHORITY").equals("L") || ITAValidationRsp.getJSONObject("RCMSG").getJSONObject("0").getString("AUTHORITY").equals("50")))){
          ITACreationRsp = ITACreate(ita, gapuraConf);
          mapper.enable(SerializationFeature.INDENT_OUTPUT);
           mapper.setPropertyNamingStrategy(new MyPropertyNamingStrategy());
           ItaRsp1.setRcmsg(mapper.readValue(ITACreationRsp.getJSONObject("RCMSG").getJSONObject("0").toString(), new TypeReference<RspcodeMsg>() {}));
           ItaRsp1.setRc(ITACreationRsp.getString("RC"));
           ItaRsp1.setSt(ITACreationRsp.getString("ST"));
       }
       
       return ItaRsp1;
   }
}
