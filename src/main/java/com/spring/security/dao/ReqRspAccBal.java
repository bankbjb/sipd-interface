package com.spring.security.dao;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.spring.security.configuration.GapuraConfiguration;

public class ReqRspAccBal {
	//private static final Logger LOGGER = LoggerFactory.getLogger(ReqRspAccBal.class);
	
	private final ObjectMapper mapper;
    
    public ReqRspAccBal() {
        this.mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    
    public RspAccBal ABCreate_inq(ReqAccBal ab, GapuraConfiguration gapuraConf) throws JsonProcessingException, IOException{
        RspAccBal rspAccBal = new RspAccBal();
        JSONObject RCMSG = new JSONObject("{\n" +
        		"        \"0\": {\n" +
        		"            \"IS_OVERRIDE\": \"1\",\n" +
        		"            \"AUTHORITY\": \"  \",\n" +
        		"            \"MESSAGE_ID\": \"KSM2010\",\n" +
        		"            \"FIELD_IN_ERROR\": \"KSM2010\",\n" +
        		"            \"SEVERITY_STATUS\": \" \",\n" +
        		"            \"MESSAGE\": \"KSM2010   does not exist on the database\",\n" +
        		"            \"DESCRIPTION\": \"KSM2010   does not exist on the database\",\n" +
        		"            \"SEVERITY\": \"20\"\n" +
        		"        }\n" +
        		"    }");
        if(!ab.isValid()){
        	rspAccBal.setNOREK(ab.getNOREK());
        	rspAccBal.setST(ab.getST());
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            mapper.setPropertyNamingStrategy(new MyPropertyNamingStrategy());
            rspAccBal.setRcmsg(mapper.readValue(RCMSG.getJSONObject("0").toString(), new TypeReference<RspcodeMsg>() {}));
            rspAccBal.setRC("14");
            return rspAccBal;
        }
        GSSConnection gssConn = new GSSConnection();
        byte endMsg = 0x00;
        JSONObject ReqAB,RspAB , MPI;
        ReqAB = new JSONObject();
        MPI = new JSONObject();
        MPI.put("ZLEAN",ab.getNOREK());
        ReqAB.put("BRANCHCD", "0000");
        ReqAB.put("PCC", "5");
        ReqAB.put("FC", "AB");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        ReqAB.put("DT", formatter.format(date));
        ReqAB.put("AUDITUID", "");
        ReqAB.put("MT", "9200");
        ReqAB.put("CID", gapuraConf.getCid());
        ReqAB.put("SID", "singleUserIDWebTeller");
        ReqAB.put("MPI", MPI);
        ReqAB.put("ST", ab.getST());
        ReqAB.put("CC", "0001");
        ReqAB.put("SPPW",gapuraConf.getSppw());
        ReqAB.put("MC","90023");
        ReqAB.put("SPPU",gapuraConf.getSppu());
        ReqAB.put("PC","001001");
        RspAB = gssConn.ConnectServerSocket("GSS-AB-INQUERY",gapuraConf.getIp() ,gapuraConf.getPort(), ReqAB, gapuraConf.getTimeout(), endMsg, endMsg);
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setPropertyNamingStrategy(new MyPropertyNamingStrategy());
        if("0000".equals(RspAB.getString("RC"))){
        	rspAccBal.setNOREK(RspAB.getJSONObject("MPO").getJSONObject("0").getString("ZLEAN"));
        	rspAccBal.setNAMA_LENGKAP(RspAB.getJSONObject("MPO").getJSONObject("0").getString("ZLCUN"));
        	rspAccBal.setNAMA_PENDEK(RspAB.getJSONObject("MPO").getJSONObject("0").getString("ZLSHN"));
        	rspAccBal.setSALDO(RspAB.getJSONObject("MPO").getJSONObject("0").getString("ZLBAL"));
        	rspAccBal.setCURENCY(RspAB.getJSONObject("MPO").getJSONObject("0").getString("ZLCCY"));
        }
        rspAccBal.setNOREK(RspAB.getJSONObject("MPI").getString("ZLEAN"));
        rspAccBal.setST(RspAB.getString("ST"));
        rspAccBal.setRcmsg(mapper.readValue(RspAB.getJSONObject("RCMSG").getJSONObject("0").toString(), new TypeReference<RspcodeMsg>() {}));
        rspAccBal.setRC(RspAB.getString("RC"));
        
        return rspAccBal;
    }
}
