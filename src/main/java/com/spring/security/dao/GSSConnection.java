package com.spring.security.dao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GSSConnection {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GSSConnection.class);
    
    public JSONObject ConnectServerSocket(String namaService,String ipaddres, int Port, JSONObject ReqMessage,int timeout, byte EndMReq, byte EndMRsp) throws IOException{
        Socket GatewaySocket = null;
        ByteArrayOutputStream tRequestByteStream;
        String tResponseStream = "";
        byte tMessageByte = -1;
        StringBuilder sb = new StringBuilder();
        JSONObject RspMessage = null;
        
        try {
        	SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            LOGGER.trace("###["+formatter.format(date)+"]REQ_INQ_IP_"+namaService+":"+ipaddres+":"+Port+": " + ReqMessage.toString());
            LOGGER.info("###["+formatter.format(date)+"]REQ_INQ_IP_"+namaService+":"+ipaddres+":"+Port+": " + ReqMessage.toString());
            InetSocketAddress gatewaySocketAddress = new InetSocketAddress(ipaddres, Port);
            GatewaySocket = new Socket();
            tRequestByteStream = new ByteArrayOutputStream();
            GatewaySocket.setSoTimeout(timeout);
            GatewaySocket.connect(gatewaySocketAddress, timeout);
            tRequestByteStream.write(ReqMessage.toString().getBytes());
            tRequestByteStream.write(EndMReq);
            GatewaySocket.getOutputStream().write(tRequestByteStream.toByteArray());
            
            do {
                tMessageByte = (byte)GatewaySocket.getInputStream().read();
                if ( tMessageByte == EndMRsp){
                     
                    break;
                 }
                 sb.append((char)tMessageByte);
                 
            } while (GatewaySocket.getInputStream().available() > 0);
            tResponseStream = sb.toString();
            //System.out.println(tResponseStream);
            RspMessage = new JSONObject(tResponseStream);
            Date date1 = new Date(System.currentTimeMillis());
            LOGGER.trace("###["+formatter.format(date1)+"]RES_INQ_IP_"+namaService+":"+ipaddres+":"+Port+": "+tResponseStream);
            LOGGER.info("###["+formatter.format(date1)+"]RES_INQ_IP_"+namaService+":"+ipaddres+":"+Port+": "+tResponseStream);
            GatewaySocket.close();
        } catch (IOException ex) {
            LOGGER.error(ex.toString());
            GatewaySocket.close();
        }
        return RspMessage;
    }
}
