package com.spring.security.dao;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RspcodeMsg {
	
	private String IS_OVERRIDE,AUTHORITY,MESSAGE_ID,FIELD_IN_ERROR,SEVERITY_STATUS,MESSAGE,DESCRIPTION,SEVERITY;
    
    @JsonProperty("IS_OVERRIDE")
    
    public String getIS_OVERRIDE() {
        return IS_OVERRIDE;
    }

    public void setIS_OVERRIDE(String IS_OVERRIDE) {
        this.IS_OVERRIDE = IS_OVERRIDE;
    }

    public String getAUTHORITY() {
        return AUTHORITY;
    }

    public void setAUTHORITY(String AUTHORITY) {
        this.AUTHORITY = AUTHORITY;
    }
    @JsonProperty("MESSAGE_ID")
    public String getMESSAGE_ID() {
        return MESSAGE_ID;
    }

    public void setMESSAGE_ID(String MESSAGE_ID) {
        this.MESSAGE_ID = MESSAGE_ID;
    }
    
    @JsonProperty("FIELD_IN_ERROR")
    public String getFIELD_IN_ERROR() {
        return FIELD_IN_ERROR;
    }

    public void setFIELD_IN_ERROR(String FIELD_IN_ERROR) {
        this.FIELD_IN_ERROR = FIELD_IN_ERROR;
    }
    @JsonProperty("SEVERITY_STATUS")
    public String getSEVERITY_STATUS() {
        return SEVERITY_STATUS;
    }

    public void setSEVERITY_STATUS(String SEVERITY_STATUS) {
        this.SEVERITY_STATUS = SEVERITY_STATUS;
    }

    public String getMESSAGE() {
        return MESSAGE;
    }

    public void setMESSAGE(String MESSAGE) {
        this.MESSAGE = MESSAGE;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getSEVERITY() {
        return SEVERITY;
    }

    public void setSEVERITY(String SEVERITY) {
        this.SEVERITY = SEVERITY;
    }

	public RspcodeMsg() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
