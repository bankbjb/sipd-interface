/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 *
 * @author DELL
 */
    
public class MpnCon {
    private static final Logger LOGGER = LoggerFactory.getLogger(MpnCon.class);
    private static final byte cEndMessageByte = -0x01;
    
    public JSONObject ConnectServerSocket(String namaService, String ipaddres, int Port, JSONObject ReqMessage, int timeout) {
        Socket GatewaySocket;
        ByteArrayOutputStream tRequestByteStream;
        String tResponseStream = "";
        byte tMessageByte = -1;
        StringBuilder sb = new StringBuilder();
        JSONObject RspMessage = null;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date) + "]REQ_INQ_IP_" + namaService + ":" + ipaddres + ":" + Port + ": " + ReqMessage.toString());
            GatewaySocket = new Socket(ipaddres, Port);
            tRequestByteStream = new ByteArrayOutputStream();
            tRequestByteStream.write(ReqMessage.toString().getBytes());
            tRequestByteStream.write(cEndMessageByte);
            GatewaySocket.getOutputStream().write(tRequestByteStream.toByteArray());

            GatewaySocket.setSoTimeout(timeout);
            while ((tMessageByte = (byte) GatewaySocket.getInputStream().read()) != cEndMessageByte) {
                sb.append((char) tMessageByte);
                tResponseStream = sb.toString();
            }
            System.out.println(tResponseStream);
            RspMessage = new JSONObject(tResponseStream);
            Date date1 = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date1) + "]RES_INQ_IP_" + namaService + ":" + ipaddres + ":" + Port + ": " + tResponseStream);
        } catch (IOException ex) {
            LOGGER.error(ex.toString());
        }
        return RspMessage;
    }
}
