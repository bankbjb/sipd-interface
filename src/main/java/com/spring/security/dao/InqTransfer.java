/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.spring.security.configuration.EqConfiguration;
import com.spring.security.configuration.utility;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author DELL
 */
public class InqTransfer {

    public String inqTrannsfer(EqConfiguration confEq, String KdbankTujuan, String NorekTujuan, String Norek_Asal) {
        Eqhandler eq = new Eqhandler();
        String output = "";
        AS400Text asinput;
        AS400Text asoutput = new AS400Text(999);
        utility ul = new utility();
        String Bit36 = ul.spaceRightPadding("JABBANK JABAR     BANK BJB", 33);
        String Bit41 = ul.spaceRightPadding("027473", 8);
        String Bit42 = ul.spaceRightPadding("027473", 15);
        String Bit43 = ul.spaceRightPadding("SIPD", 40);
        String Bit48 = ul.spaceRightPadding("SIPD", 30);
        String Bit49 = ul.spaceRightPadding("360", 3);
        String Bit59 = ul.spaceRightPadding("OTA", 3);
        String Bit63 = ul.spaceRightPadding("01", 2);
        String Bit100 = ul.spaceRightPadding("110", 3);
        String Bit103 = ul.spaceRightPadding(NorekTujuan, 13);
        String Bit107 = ul.spaceRightPadding("7473", 4);
        String Bit127 = ul.spaceRightPadding("1101103", 7);
        String input = "";
        if (KdbankTujuan.equals("110")) {
            asinput = new AS400Text(228);
            //asinput.
            input = "1622011201001002071000000000000000110747300110622011201001002071=9912?" + Bit36 + Bit41 + Bit42 + Bit43 + Bit48 + Bit49 + Bit63 + Bit100 + Bit103 + Bit107 + Bit127;
        } else {
            //System.out.println("anjing masuk");
            asinput = new AS400Text(238);
            Bit100 = KdbankTujuan;
            Bit36 = ul.spaceRightPadding("JABBANK JABAR", 33);
            Bit103 = ul.spaceRightPadding(NorekTujuan, 20);
            Bit127 = ul.spaceRightPadding(KdbankTujuan+"1103", 7);
            input = "2622011201001002071000000010000000110747300110622011201001002071=9912?" + Bit36 + Bit41 + Bit42 + Bit43 + Bit48 + Bit49 + Bit59 + Bit63 + Bit100 + Bit103 + Bit107 + Bit127;
        }
        try {
            output = eq.eqHandler(confEq.getIp(), confEq.getUserName(), confEq.getPassword(), "SIPDR", asinput, asoutput, input, input.length());
        } catch (AS400SecurityException | ErrorCompletingRequestException | IOException | InterruptedException | PropertyVetoException | ObjectDoesNotExistException ex) {
            Logger.getLogger(InqTransfer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }
    
    public String GetAlamat(EqConfiguration confEq, String KdbankTujuan, String NorekTujuan, String Norek_Asal) {
        Eqhandler eq = new Eqhandler();
        String output = "";
        AS400Text asinput;
        AS400Text asoutput = new AS400Text(999);
        utility ul = new utility();
        String Bit36 = ul.spaceRightPadding("JABBANK JABAR     BANK BJB", 33);
        String Bit41 = ul.spaceRightPadding("027473", 8);
        String Bit42 = ul.spaceRightPadding("027473", 15);
        String Bit43 = ul.spaceRightPadding("SIPD", 40);
        String Bit48 = ul.spaceRightPadding("SIPD", 30);
        String Bit49 = ul.spaceRightPadding("360", 3);
        String Bit59 = ul.spaceRightPadding("OTA", 3);
        String Bit63 = ul.spaceRightPadding("01", 2);
        String Bit100 = ul.spaceRightPadding("110", 3);
        String Bit103 = ul.spaceRightPadding(NorekTujuan, 13);
        String Bit107 = ul.spaceRightPadding("7473", 4);
        String Bit127 = ul.spaceRightPadding("1101103", 7);
        String input = "";
        if (KdbankTujuan.equals("110")) {
            asinput = new AS400Text(228);
            //asinput.
            input = "5622011201001002071000000000000000110747300110622011201001002071=9912?" + Bit36 + Bit41 + Bit42 + Bit43 + Bit48 + Bit49 + Bit63 + Bit100 + Bit103 + Bit107 + Bit127;
        } else {
            //System.out.println("anjing masuk");
            asinput = new AS400Text(238);
            Bit100 = KdbankTujuan;
            Bit36 = ul.spaceRightPadding("JABBANK JABAR", 33);
            Bit103 = ul.spaceRightPadding(NorekTujuan, 20);
            Bit127 = ul.spaceRightPadding(KdbankTujuan+"1103", 7);
            input = "2622011201001002071000000010000000110747300110622011201001002071=9912?" + Bit36 + Bit41 + Bit42 + Bit43 + Bit48 + Bit49 + Bit59 + Bit63 + Bit100 + Bit103 + Bit107 + Bit127;
        }
        try {
            output = eq.eqHandler(confEq.getIp(), confEq.getUserName(), confEq.getPassword(), "SIPDR", asinput, asoutput, input, input.length());
        } catch (AS400SecurityException | ErrorCompletingRequestException | IOException | InterruptedException | PropertyVetoException | ObjectDoesNotExistException ex) {
            Logger.getLogger(InqTransfer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }
}
