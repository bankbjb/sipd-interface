/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.spring.security.configuration.EqConfiguration;
import com.spring.security.configuration.utility;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author DELL
 */
public class Djp {

    public String INpwp(EqConfiguration confEq, String npwp, String mata_anggaran, String jenis_pajak) {
        Eqhandler eq = new Eqhandler();
        String output = "";
        AS400Text asinput;
        AS400Text asoutput = new AS400Text(999);
        utility ul = new utility();
        String Bit18 = ul.spaceRightPadding("7012", 4);
        String Bit32 = ul.spaceRightPadding("0110", 4);
        String Bit48 = ul.spaceRightPadding(npwp+mata_anggaran+jenis_pajak, 24);
        String Bit49 = ul.spaceRightPadding("360", 3);
        String Bit63 = ul.spaceRightPadding("110028", 6);
        String input = "";
        asinput = new AS400Text(42);
        //asinput.
        input = "4"+Bit18+Bit32+Bit48+Bit49+Bit63;
        try {
            output = eq.eqHandler(confEq.getIp(), confEq.getUserName(), confEq.getPassword(), "SIPDR", asinput, asoutput, input, input.length());
        } catch (AS400SecurityException | ErrorCompletingRequestException | IOException | InterruptedException | PropertyVetoException | ObjectDoesNotExistException ex) {
            Logger.getLogger(InqTransfer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }
    
    public String CreateIdbilling(EqConfiguration confEq,String nominal,String npwp, String mata_anggaran, String jenis_pajak,String masapajak1,String masapajak2,String tahunpajak,String nomersk,String desc,String nop) {
        Eqhandler eq = new Eqhandler();
        String output = "";
        AS400Text asinput;
        AS400Text asoutput = new AS400Text(999);
        utility ul = new utility();
        String Bit4 = ul.zeroLeftPadding(nominal, 12);
        String Bit18 = ul.spaceRightPadding("7012", 4);
        String Bit32 = ul.spaceRightPadding("0110", 4);
        String Bit47 = ul.spaceRightPadding(nop, 18);
        String Bit48 = ul.spaceRightPadding(npwp+mata_anggaran+jenis_pajak+masapajak1+masapajak2+tahunpajak+nomersk+npwp, 62);
        String Bit49 = ul.spaceRightPadding("360", 3);
        String Bit62 = ul.spaceRightPadding(desc, 75);
        String Bit63 = ul.spaceRightPadding("110028", 6);
        String input = "";
        System.out.println("nominal"+nominal);
        input = "4"+Bit4+Bit18+Bit32+Bit47+Bit48+Bit49+Bit62+Bit63;
        System.out.println("input : "+input);
        asinput = new AS400Text(input.length());
        //asinput.
       // input = "4"+Bit4+Bit18+Bit32+Bit47+Bit48+Bit49+Bit62+Bit63;
        try {
            output = eq.eqHandler(confEq.getIp(), confEq.getUserName(), confEq.getPassword(), "SIPD1R", asinput, asoutput, input, input.length());
        } catch (AS400SecurityException | ErrorCompletingRequestException | IOException | InterruptedException | PropertyVetoException | ObjectDoesNotExistException ex) {
            Logger.getLogger(InqTransfer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return output;
    }

}
