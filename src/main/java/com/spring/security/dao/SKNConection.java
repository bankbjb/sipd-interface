/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import com.spring.security.configuration.EncryptSKN;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author DELL
 */
public class SKNConection {
    private static final Logger LOGGER = LoggerFactory.getLogger(MpnCon.class);
    private static final byte cEndMessageByte = -0x01;
    
    public JSONObject ConnectServerSocket(String namaService, String ipaddres, int Port, JSONObject ReqMessage, int timeout, String key_enrypt) {
        Socket GatewaySocket;
        ByteArrayOutputStream tRequestByteStream;
        String tResponseStream = "";
        String request,response;
        byte tMessageByte = -1;
        StringBuilder sb = new StringBuilder();
        JSONObject RspMessage = null;

        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            Date date = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date) + "]REQ_INQ_IP_" + namaService + ":" + ipaddres + ":" + Port + ": " + ReqMessage.toString());
            GatewaySocket = new Socket(ipaddres, Port);
            tRequestByteStream = new ByteArrayOutputStream();
            EncryptSKN eskn = new EncryptSKN();
            request = eskn.encrypt(ReqMessage.toString(), key_enrypt);
            LOGGER.info("###[" + formatter.format(date) + "]REQ_IP_ENCRYPT" + namaService + ":" + ipaddres + ":" + Port + ": " + request);
            tRequestByteStream.write(request.getBytes());
            tRequestByteStream.write(cEndMessageByte);
            GatewaySocket.getOutputStream().write(tRequestByteStream.toByteArray());

            GatewaySocket.setSoTimeout(timeout);
            while ((tMessageByte = (byte) GatewaySocket.getInputStream().read()) != cEndMessageByte) {
                sb.append((char) tMessageByte);
                tResponseStream = sb.toString();
            }
            if(tResponseStream.isEmpty()){
                response ="{\"RC\":\"0091\"}";
            }else{
                LOGGER.info("###[" + formatter.format(date) + "]RES_IP_ENCRYPT" + namaService + ":" + ipaddres + ":" + Port + ": " + tResponseStream);
                if(tResponseStream.substring(0,1).equals("{")){
                    response = tResponseStream;
                }else{
                response = eskn.decrypt(tResponseStream, key_enrypt);
                }
            }
            //System.out.println(tResponseStream);
            RspMessage = new JSONObject(response);
            Date date1 = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date1) + "]RES_IP_" + namaService + ":" + ipaddres + ":" + Port + ": " + response);
        } catch (IOException ex) {
            RspMessage.put("RC", "0091");
            LOGGER.error(ex.toString());
            
        }
        return RspMessage;
    }
}
