/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import com.spring.security.configuration.CallBackConf;
import org.json.JSONObject;

/**
 *
 * @author DELL
 */
public class Calbback {

    public String callback_api(JSONObject Request, CallBackConf conf) {
        JSONObject response;
        String rsp;
        HTTPCon cb = new HTTPCon();
        String token = this.token(conf);
        //System.out.println(token);
        response = cb.sendRestAPI(conf.getURL(), Request, "POST", "ib/callback", conf.getClient_id(), conf.getSecretId(),token);
        rsp = response.getString("HttpResponseCode");
        return rsp;
    }

    private String token(CallBackConf conf) {
        JSONObject Request = new JSONObject();
        Request.put("bpd_code", "110");
        //String response;
        HTTPCon cb = new HTTPCon();
        JSONObject rsp = cb.sendRestAPI(conf.getURL(), Request, "POST", "ib/get-token", conf.getClient_id(), conf.getSecretId(), "");
        // = new JSONObject(response);
        String token="";
        if(rsp.has("data")){
            token = rsp.getJSONObject("data").getString("token");
        }
        return token;
    }
}
