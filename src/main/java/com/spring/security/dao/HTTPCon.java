/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author DELL
 */
public class HTTPCon {

    private static final Logger LOGGER = LoggerFactory.getLogger(HTTPCon.class);

    public JSONObject sendRestAPI(String Domain, JSONObject pRequestMessage, String Method, String Url, String client_id, String secretId, String token) {

        JSONObject JsonMessage = pRequestMessage;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String response = null;

        JSONObject JsonResponse = null;
        HttpURLConnection conn = null;
        String hasil = client_id.concat(":").concat(secretId);
        //System.out.println(hasil);
        //devel
        //SSLContext sc=  sslContext("D:\\certifucate\\keystore.jks","30Juni1954*");
        //server
        //SSLContext sc=  sslContext("/opt/api_kharisma/cer/keystore.jks","30Juni1954*");

        try {
            String alamat = Domain.concat(Url);
            URL url = new URL(alamat);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(30000);
            conn.setRequestProperty("Authorization: Bearer ", Base64.getEncoder().encodeToString(hasil.getBytes(StandardCharsets.UTF_8)));
            if (!token.isEmpty()) {
                conn.setRequestProperty("token", token);
                System.out.println("Token masuk : "+token);
            }
            //conn.
            if (Method.equals("POST")) {

                //conn.setSSLSocketFactory(SSLSocketFactory(trusted));
                conn.setDoOutput(true);
                conn.setRequestMethod(Method);
                conn.setRequestProperty("Content-Type", "application/json");

                OutputStream os = conn.getOutputStream();
                os.write(JsonMessage.toString().getBytes());
                os.flush();
                Date date = new Date(System.currentTimeMillis());
                LOGGER.info("###[" + formatter.format(date) + "]REQ_INQ_IP_" + alamat + ": " + JsonMessage.toString());
            } else {

                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                //conn.setRequestProperty("body", JsonMessage.toString());
                Date date = new Date(System.currentTimeMillis());
                LOGGER.info("###[" + formatter.format(date) + "]REQ_INQ_IP_" + alamat + ": ");
            }
            //SystemLog.getSingleton().log(this, LogType.STREAM, new StringBuilder().append("REQUEST MESSAGE SWITCHER to : ").append(alamat).append(JsonMessage).toString());
            response = String.valueOf(conn.getResponseCode());
//            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
//                //SystemLog.getSingleton().log(this, LogType.ERROR, "Failed : HTTP error code : " + conn.getResponseCode());
//                Date date = new Date(System.currentTimeMillis());
//                LOGGER.info("###[" + formatter.format(date) + "]REQ_INQ_IP_" + alamat + ": HTTP error code : " + conn.getResponseCode());
//                
//            }
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            StringBuilder output = new StringBuilder();
            String output2;
            while ((output2 = br.readLine()) != null) {
                //System.out.println(output2);
                output.append(output2);
            }
            if (output.charAt(0) == '{') {
                JsonResponse = new JSONObject(output.toString());
                JsonResponse.put("HttpResponseCode", response);
            } else {
                JsonResponse = new JSONObject();
                JsonResponse.put("HttpResponseCode", response);
            }

            conn.disconnect();
            Date date = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date) + "]RSP_INQ_IP_" + alamat + ": " + JsonResponse.toString());
            //SystemLog.getSingleton().log(this, LogType.STREAM, new StringBuilder().append("RESPONSE MESSAGE SWITCHER from : ").append(alamat).append(JsonResponse).toString());
            //System.out.println();

        } catch (MalformedURLException ex) {
            // Logger.getLogger();
            Date date = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date) + "]ERROR :" + ex.toString());
            //SystemLog.getSingleton().log(this, LogType.ERROR, ex.toString());
            conn.disconnect();
            /*     */
        } catch (IOException ex) {
            Date date = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date) + "]ERROR :" + ex.toString());
            JsonResponse = new JSONObject();
            JsonResponse.put("HttpResponseCode", response);
            conn.disconnect();
        }

        return JsonResponse;
    }
    
    public JSONObject sendRestAPI2(String Domain, JSONObject pRequestMessage, String Method, String Url, String client_id, String secretId) {

        JSONObject JsonMessage = pRequestMessage;
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String response = null;

        JSONObject JsonResponse = null;
        HttpURLConnection conn = null;
        String hasil = client_id.concat(":").concat(secretId);
        //System.out.println(hasil);
        //devel
        //SSLContext sc=  sslContext("D:\\certifucate\\keystore.jks","30Juni1954*");
        //server
        //SSLContext sc=  sslContext("/opt/api_kharisma/cer/keystore.jks","30Juni1954*");

        try {
            String alamat = Domain.concat(Url);
            URL url = new URL(alamat);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(30000);
            LOGGER.info("###RSP_INQ_IP_" + alamat + ": " + "Authorization" +"Basic "+Base64.getEncoder().encodeToString(hasil.getBytes()));
            conn.setRequestProperty("Authorization", "Basic "+Base64.getEncoder().encodeToString(hasil.getBytes(StandardCharsets.UTF_8)));

            if (Method.equals("POST")) {

                //conn.setSSLSocketFactory(SSLSocketFactory(trusted));
                conn.setDoOutput(true);
                conn.setRequestMethod(Method);
                conn.setRequestProperty("Content-Type", "application/json");

                OutputStream os = conn.getOutputStream();
                os.write(JsonMessage.toString().getBytes());
                os.flush();
                Date date = new Date(System.currentTimeMillis());
                LOGGER.info("###[" + formatter.format(date) + "]REQ_INQ_IP_" + alamat + ": " + JsonMessage.toString());
            } else {

                conn.setRequestMethod("GET");
                conn.setRequestProperty("Content-Type", "application/json");
                //conn.setRequestProperty("body", JsonMessage.toString());
                Date date = new Date(System.currentTimeMillis());
                LOGGER.info("###[" + formatter.format(date) + "]REQ_INQ_IP_" + alamat + ": ");
            }
            //SystemLog.getSingleton().log(this, LogType.STREAM, new StringBuilder().append("REQUEST MESSAGE SWITCHER to : ").append(alamat).append(JsonMessage).toString());
            //response = String.valueOf(conn.getResponseCode());
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                //SystemLog.getSingleton().log(this, LogType.ERROR, "Failed : HTTP error code : " + conn.getResponseCode());
                Date date = new Date(System.currentTimeMillis());
                LOGGER.info("###[" + formatter.format(date) + "]REQ_INQ_IP_" + alamat + ": HTTP error code : " + conn.getResponseCode());

            }
            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            StringBuilder output = new StringBuilder();
            String output2;
            while ((output2 = br.readLine()) != null) {
                //System.out.println(output2);
                output.append(output2);
            }

            JsonResponse = new JSONObject(output.toString());
            JsonResponse.put("HttpResponseCode", String.valueOf(conn.getResponseCode()));
            conn.disconnect();
            Date date = new Date(System.currentTimeMillis());
            //LOGGER.info("###[" + formatter.format(date) + "]RSP_INQ_IP_" + alamat + ": " + output.toString());
            LOGGER.info("###[" + formatter.format(date) + "]RSP_INQ_IP_" + alamat + ": " + JsonResponse.toString());
            //SystemLog.getSingleton().log(this, LogType.STREAM, new StringBuilder().append("RESPONSE MESSAGE SWITCHER from : ").append(alamat).append(JsonResponse).toString());
            //System.out.println();

        } catch (MalformedURLException ex) {
            // Logger.getLogger();
            Date date = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date) + "]ERROR :" + ex.toString());
            //SystemLog.getSingleton().log(this, LogType.ERROR, ex.toString());
            conn.disconnect();
            /*     */
        } catch (IOException ex) {
            Date date = new Date(System.currentTimeMillis());
            LOGGER.info("###[" + formatter.format(date) + "]ERROR :" + ex.toString());
            response = "068";
            conn.disconnect();
        }

        return JsonResponse;
    }
}
