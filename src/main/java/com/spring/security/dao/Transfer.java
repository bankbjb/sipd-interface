/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.dao;

import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.ObjectDoesNotExistException;
import com.spring.security.configuration.EqConfiguration;
import com.spring.security.configuration.SknConfiguration;
import com.spring.security.configuration.utility;
import com.spring.security.model.kode_bank;
import com.spring.security.service.KodeBankServiceImpl;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.json.JSONObject;

/**
 *
 * @author DELL
 */
public class Transfer {

    public String TrannsferAll(EqConfiguration confEq, String KdbankTujuan, String NorekTujuan, String Norek_Asal, String Nominal, String desc, String nama_penerima, String alamat_penerima, KodeBankServiceImpl kb, SknConfiguration skconf) {
        Eqhandler eq = new Eqhandler();
        String output = "";
        AS400Text asinput = null;
        AS400Text asoutput = new AS400Text(999);
        utility ul = new utility();
        String Bit2 = ul.spaceRightPadding("622011201001002071", 18);
        String Bit4 = ul.zeroLeftPadding(Nominal, 12);
        String Bit18 = ul.spaceRightPadding("6010", 4);
        String Bit22 = ul.spaceRightPadding("002", 3);
        String Bit28 = "C" + ul.zeroLeftPadding("25000", 8);
        String Bit32 = ul.spaceRightPadding("0001107473", 10);
        String Bit33 = ul.spaceRightPadding("00110", 5);
        String Bit35 = ul.spaceRightPadding("622011201001002071=9912?", 24);
        String Bit36 = ul.spaceRightPadding("JABBANK JABAR     BANK BJB", 33);
        String Bit41 = ul.spaceRightPadding("7473", 8);
        String Bit42 = ul.spaceRightPadding("7473", 15);
        String Bit43 = ul.spaceRightPadding("SIPD", 40);
        String Bit48 = ul.spaceRightPadding("SIPD", 30);
        String Bit49 = ul.spaceRightPadding("360", 3);
        double total = Double.valueOf(Nominal) - 25000;
        String Bit57 = ul.zeroLeftPadding(Nominal, 12) + ul.zeroLeftPadding("25000", 12) + ul.zeroLeftPadding(String.valueOf((int) total), 12);
        String Bit59 = ul.spaceRightPadding("OTA", 3);
        String Bit60 = ul.spaceRightPadding("JAB", 3);
        // = new KodeBankServiceImpl();
        kode_bank kbd = kb.findByKodebankOnline(KdbankTujuan);
        String Bit61 = ul.spaceRightPadding(kbd.getKode_bank_rtgs(), 8) + ul.spaceRightPadding(desc, 32) + ul.spaceRightPadding(nama_penerima, 35) + ul.spaceRightPadding(alamat_penerima, 35);
        String Bit63 = ul.spaceRightPadding("01", 2);
        String Bit100 = ul.spaceRightPadding(KdbankTujuan, 3);
        String Bit102 = ul.spaceRightPadding(Norek_Asal, 13);
        String Bit103 = ul.spaceRightPadding(NorekTujuan, 13);
        String Bit105 = ul.spaceRightPadding(desc, 105);
        String Bit107 = ul.spaceRightPadding("7473", 4);

        String Bit125 = ul.spaceRightPadding("110", 3);
        String Bit127 = ul.spaceRightPadding(KdbankTujuan + "110", 6);
        String input = "";
        if (KdbankTujuan.equals("110")) {
            input = "1" + Bit2 + Bit4 + Bit22 + Bit32 + Bit33 + Bit35 + Bit41 + Bit42 + Bit43 + Bit49 + Bit100 + Bit102 + Bit103 + Bit105 + Bit125;
            //asinput = new AS400Text(input.length());
            Date date = new Date(System.currentTimeMillis());
            SimpleDateFormat formatter2 = new SimpleDateFormat("HssS");
            //String ref = 
            JSONObject ReqIta = new JSONObject();
            ReqIta.put("SOURCEACC", Norek_Asal);
            ReqIta.put("DESTACC", NorekTujuan);
            ReqIta.put("TRXREF", "SPD-7473-"+formatter2.format(date));
            ReqIta.put("DRTRXCODE", "019");
            ReqIta.put("DRNARATIVE", "SPD-7473-"+formatter2.format(date));
            ReqIta.put("DRLINE1", desc.substring(0, 35));
            ReqIta.put("DRLINE2", desc.substring(35, 70));
            ReqIta.put("DRLINE3", desc.substring(70,desc.length()));
            ReqIta.put("DRLINE4", "");
            ReqIta.put("CRTRXCODE", "519");
            ReqIta.put("CRNARATIVE", "SPD-7473-"+formatter2.format(date));
            ReqIta.put("CRLINE1", desc.substring(0, 35));
            ReqIta.put("CRLINE2", desc.substring(35, 70));
            ReqIta.put("CRLINE3", desc.substring(70,desc.length()));
            ReqIta.put("CRLINE4", "");
            ReqIta.put("AMOUNT", Nominal);
//        ReqAb.put("STAN", TrxId.substring(TrxId.length() - 11));
            HTTPCon Hc = new HTTPCon();
            ReqIta = Hc.sendRestAPI2(confEq.getUrl(), ReqIta, "POST", "ita", confEq.getClient_id(), confEq.getClient_secreet());
            output = ReqIta.getString("code")+"000000"+formatter2.format(date);

        } else if (Double.parseDouble(Nominal) < 50000000) {
            Bit36 = ul.spaceRightPadding("JABBANK JABAR", 33);
            Bit41 = ul.spaceRightPadding("027473", 8);
            Bit42 = ul.spaceRightPadding("027473", 15);
            Bit103 = ul.spaceRightPadding(NorekTujuan, 20);
            Bit28 = "C" + ul.zeroLeftPadding("6500", 8);
            System.out.println("option 2");
            input = "2" + Bit2 + Bit4 + Bit22 + Bit28 + Bit32 + Bit33 + Bit35 + Bit41 + Bit42 + Bit43 + Bit49 + Bit59 + Bit60 + Bit63 + Bit100 + Bit102 + Bit103 + ul.spaceRightPadding(Bit105, 35) + Bit127;
            asinput = new AS400Text(input.length());
        } else if (Double.parseDouble(Nominal) > 100000000) {
            Bit22 = ul.spaceRightPadding("021", 3);
            Bit41 = ul.spaceRightPadding("027473", 8);
            Bit42 = ul.spaceRightPadding("027473", 15);
            Bit59 = ul.spaceRightPadding("PAY", 3);
            Bit60 = ul.spaceRightPadding("RTO", 3);
            Bit100 = ul.spaceRightPadding("110", 3);
            Bit103 = ul.spaceRightPadding(NorekTujuan, 20);
            System.out.println("option 5");
            input = "5" + Bit2 + Bit4 + Bit18 + Bit22 + Bit28 + Bit32 + Bit35 + Bit41 + Bit42 + Bit43 + Bit49 + Bit57 + Bit59 + Bit60 + Bit61 + Bit100 + Bit102 + Bit103;
            asinput = new AS400Text(input.length());

        } else if (Double.parseDouble(Nominal) >= 50000000 && Double.parseDouble(Nominal) <= 100000000) {
            InqTransfer It = new InqTransfer();
            String alamat_krm = "Bandung";
            output = It.inqTrannsfer(confEq, "110", Norek_Asal, Norek_Asal);
            if (output.substring(0, 2).equals("00")) {
                output = It.GetAlamat(confEq, "110", Norek_Asal, Norek_Asal);
                if (output.substring(0, 2).equals("00")) {
                    alamat_krm = output.substring(2, 36).trim().concat(" ").concat(output.substring(37, 72).trim()).concat(" ").concat(output.substring(72, 107).trim()).concat(" ").concat(output.substring(107, 142).trim()).concat(" ").concat(output.substring(142, 177).trim());
                }
                output = this.SKNTransfer(skconf, Nominal, Norek_Asal, NorekTujuan, output.substring(2, 35).trim(), NorekTujuan, desc, alamat_krm, kbd.getKode_bank_rtgs());
            }

        } else {
            asinput = new AS400Text(999);
        }
        if (asinput != null) {
            try {
                output = eq.eqHandler(confEq.getIp(), confEq.getUserName(), confEq.getPassword(), "SIPD1R", asinput, asoutput, input, input.length());
            } catch (AS400SecurityException | ErrorCompletingRequestException | IOException | InterruptedException | PropertyVetoException | ObjectDoesNotExistException ex) {
                Logger.getLogger(Transfer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return output;
    }

    public String SKNTransfer(SknConfiguration skconf, String Nominal, String Norek_Asal, String NorekTujuan, String nama_asal, String nama_tujuan, String desc, String alamat_wajib_pajak, String kbd) {
        JSONObject SKN = new JSONObject();
        JSONObject MPI = new JSONObject();
        JSONObject SKNRsp;
        String response;

        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date(System.currentTimeMillis());
        SKN.put("DT", formatter.format(date));
        SKN.put("MT", "9200");
        SKN.put("CID", skconf.getCid());
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyMMddHHmmsss");
        date = new Date(System.currentTimeMillis());
        SKN.put("ST", formatter2.format(date));

        //MPI.put("CR", "IDR");
        MPI.put("NN", Nominal.concat(",00"));
        MPI.put("SKNP1", "1");
        MPI.put("NRF", Norek_Asal.trim());
        MPI.put("INP1", "");
        MPI.put("JNP2", "1");
        MPI.put("NNP1", nama_asal);
        MPI.put("INP2", "");
        MPI.put("NNP2", nama_tujuan);
        MPI.put("JNP1", "3");
        MPI.put("NNF", "0,00");
        MPI.put("KTR", desc);
        MPI.put("JT", "51");
        MPI.put("SKNP2", "1");
        MPI.put("ANP1", alamat_wajib_pajak);
        MPI.put("ANP2", "");
        MPI.put("NRNP1", Norek_Asal.trim());
        MPI.put("SKT", "");
        MPI.put("NRNP2", NorekTujuan.trim());
        MPI.put("IPPP", kbd);
        MPI.put("IPPA", kbd);
        MPI.put("NR", ("SPIDSK").concat(formatter2.format(date)));

        SKN.put("MPI", MPI);
        SKN.put("BRANCHCD", "0000");
        SKN.put("AUDITUID", "KAS1");
        SKN.put("CC", skconf.getChanelcode());
        SKN.put("MC", "320101");
        SKN.put("PC", "001002");
        SKNConection sknc = new SKNConection();
        SKNRsp = sknc.ConnectServerSocket("SKN", skconf.getIp(), skconf.getPort(), SKN, skconf.getTimeout(), skconf.getKeyEncrypt().trim());
        if (SKNRsp.getString("RC").equals("0000")) {
            response = SKNRsp.getString("RC").substring(2, 4).concat(SKNRsp.getJSONObject("MPO").getJSONObject("0").getString("NR").trim());
        } else {
            response = SKNRsp.getString("RC").substring(2, 4).trim();
        }
        return response;
    }
}
