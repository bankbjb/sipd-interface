package com.spring.security.dao;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

// AB Request
@JsonIgnoreProperties( { "valid" })
public class ReqAccBal {
	
	private String ST,NOREK;

    public ReqAccBal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getST() {
        return ST;
    }

    public void setST(String ST) {
        this.ST = ST;
    }

    public String getNOREK() {
        return NOREK;
    }

    public void setNOREK(String NOREK) {
        this.NOREK = NOREK;
    }
    
    public boolean isValid() {
    	return NOREK != null && ST != null;
    }

	@Override
	public String toString() {
		return "ReqAccBal [ST=" + ST + ", NOREK=" + NOREK + "]";
	}
    
}
