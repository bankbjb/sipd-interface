/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author DELL
 */
@Configuration
public class SknConfiguration {

    @Value("${skn.ip:10.6.226.253}")
    String ip;
    @Value("${skn.port:8080}")
    int port;
    @Value("${skn.key:5knS1pD01bjb@!%}")
    String keyEncrypt;
    @Value("${skn.cid:SIPD01}")
    String cid;
    @Value("${skn.timeout:30000}")
    int timeout;
    @Value("${skn.chanelcode:6032}")
    String chanelcode;

    public String getChanelcode() {
        return chanelcode;
    }

    public void setChanelcode(String chanelcode) {
        this.chanelcode = chanelcode;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getKeyEncrypt() {
        return keyEncrypt;
    }

    public void setKeyEncrypt(String keyEncrypt) {
        this.keyEncrypt = keyEncrypt;
    }

}
