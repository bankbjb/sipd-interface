/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.configuration.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;

/**
 *
 * @author L108
 */
public class CustomOauthExceptionSerializer extends StdSerializer<CustomOauthException>{

    public CustomOauthExceptionSerializer() {
        super(CustomOauthException.class);
    }
    
    @Override
    public void serialize(CustomOauthException value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        //jsonGenerator.writeBooleanField("error", true);
        jsonGenerator.writeObjectFieldStart("status");
        jsonGenerator.writeNumberField("code", 299);
        jsonGenerator.writeStringField("message", value.getMessage());
        jsonGenerator.writeEndObject();
        jsonGenerator.writeEndObject();
    }
    
}
