/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.configuration;

/**
 *
 * @author DELL
 */
public class utility {

    public String zeroLeftPadding(String pParam, int pLength) {
        String tParam = pParam;
        if (tParam.length() < pLength) {
            while (tParam.length() < pLength) {
                tParam = "0" + tParam;
            }
        } else {
            tParam = tParam.substring(0, pLength);
        }

        return tParam;
    }

    public String zeroRightPadding(String pParam, int pLength) {
        String tParam = pParam;

        if (tParam.length() < pLength) {
            while (tParam.length() < pLength) {
                tParam = tParam + "0";
            }
        } else {
            tParam = tParam.substring(0, pLength);
        }

        return tParam;
    }

    public String spaceLeftPadding(String pParam, int pLength) {
        String tParam = pParam;
        if (tParam.length() < pLength) {
            while (tParam.length() < pLength) {
                tParam = " " + tParam;
            }
        } else {
            tParam = tParam.substring(0, pLength);
        }

        return tParam;
    }

    public String spaceRightPadding(String pParam, int pLength) {
        String tParam = pParam;
        if (tParam.length() < pLength) {
            while (tParam.length() < pLength) {
                tParam = tParam + " ";
            }
        } else {
            tParam = tParam.substring(0, pLength);
        }
        return tParam;
    }
}
