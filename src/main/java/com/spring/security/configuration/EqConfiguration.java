/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author DELL
 */
@Configuration
public class EqConfiguration {

    @Value("${eq.ip:192.168.226.4}")
    String ip;
    @Value("${eq.UserName:IF400}")
    String UserName;
    @Value("${eq.Password:IF4001}")
    String Password;
    @Value("${eq.url:http://10.6.226.34:5100/api/dgb/}")
    String url;
    @Value("${eq.client_id:sipd@ace}")
    String client_id;
    @Value("${eq.client_secreet:p@ssw0rd}")
    String client_secreet;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_secreet() {
        return client_secreet;
    }

    public void setClient_secreet(String client_secreet) {
        this.client_secreet = client_secreet;
    }
    
    

}
