/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author DELL
 */
@Configuration
public class CallBackConf {

    @Value("${sipd.url:http://13.248.177.237/}")
    String URL;
    @Value("${sipd.client_id:e3374384-5db9-404b-b0cb-520092da6669}")
    String client_id;
    @Value("${sipd.secretId:ddcc4867-7037-44d4-b9c2-0d53f1840629}")
    String secretId;

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getSecretId() {
        return secretId;
    }

    public void setSecretId(String secretId) {
        this.secretId = secretId;
    }

}
