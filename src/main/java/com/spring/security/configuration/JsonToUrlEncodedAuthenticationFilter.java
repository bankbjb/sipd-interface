/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Objects;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.catalina.connector.RequestFacade;
import org.json.JSONObject;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 *
 * @author L108
 */
@Component
@Order(value = Integer.MIN_VALUE)
public class JsonToUrlEncodedAuthenticationFilter implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
        HttpServletResponse resp = (HttpServletResponse) response;
        
        if (Objects.equals(request.getContentType(), "application/json") 
                && Objects.equals(((RequestFacade) request).getServletPath(), "/api/oauth/token")) {
            
            InputStream is = request.getInputStream();
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            int nRead;
            byte[] data = new byte[16384];

            while ((nRead = is.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, nRead);
            }
            buffer.flush();
            
            JSONObject gt = new JSONObject(buffer.toString());
            gt.put("grant_type", "client_credentials");
            byte[] json = gt.toString().getBytes();
            
            //byte[] json = buffer.toByteArray();

            HashMap<String, String> result = new ObjectMapper().readValue(json, HashMap.class);
            HashMap<String, String[]> r = new HashMap<>();
            for (String key : result.keySet()) {
                String[] val = new String[1];
                val[0] = result.get(key);
                r.put(key, val);
            }

            String[] val = new String[1];
            val[0] = ((RequestFacade) request).getMethod();
            r.put("_method", val);
            System.out.println(Arrays.asList(result));

            HttpServletRequest s = new MyServletRequestWrapper(((HttpServletRequest) request), r);
            
//            if (!result.get("client_id").equals("xendit-client")) {
//                
//                System.out.println("NOT AUTHORIZED CLIENT_ID");
//                Map<String,Object> status = new HashMap<String,Object>();
//                status.put("code", 299);
//                status.put("message", "Invalid client_id or client_secret");
//                
//                JSONObject obj = new JSONObject();
//                obj.put("error", true);
//                obj.put("status", status);
//                
//                response.resetBuffer();
//                response.setContentType("application/json");
//                response.setCharacterEncoding("UTF-8");
//                
//                response.getOutputStream().write(obj.toString().getBytes());
//                resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//                
//            } else if(!result.get("client_secret").equals("xendit-secret")) {
//                System.out.println("NOT AUTHORIZED CLIENT_SECRET");
//                Map<String,Object> status = new HashMap<String,Object>();
//                status.put("code", 299);
//                status.put("message", "Invalid client_id or client_secret");
//                
//                JSONObject obj = new JSONObject();
//                obj.put("error", true);
//                obj.put("status", status);
//                
//                response.resetBuffer();
//                response.setContentType("application/json");
//                response.setCharacterEncoding("UTF-8");
//                
//                response.getOutputStream().write(obj.toString().getBytes());
//                resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
//                
//            }
            chain.doFilter(s, response);
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
    }
}
