package com.spring.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GapuraConfiguration {
	
	@Value("${gapura.ip:192.168.226.4}")
	private String ip;
	
	@Value("${gapura.port:10111}")
        private int port;
	
	@Value("${gapura.timeout:30000}")
        private int timeout;
	
	@Value("${gapura.sppw:BJB2013}")
        private String sppw;
	
	@Value("${gapura.sppu:UABS}")
        private String sppu;
	
	@Value("${gapura.cid:TG-CB-00-0000090}")
        private String cid;
    
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public String getSppw() {
		return sppw;
	}
	public void setSppw(String sppw) {
		this.sppw = sppw;
	}
	public String getSppu() {
		return sppu;
	}
	public void setSppu(String sppu) {
		this.sppu = sppu;
	}
	public String getCid() {
		return cid;
	}
	public void setCid(String cid) {
		this.cid = cid;
	}
	
}
