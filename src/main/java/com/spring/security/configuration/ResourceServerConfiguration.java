package com.spring.security.configuration;

import com.spring.security.configuration.util.AuthExceptionEntryPoint;
import com.spring.security.configuration.util.CustomAccessDeniedHandler;
import com.spring.security.configuration.util.CustomOauthException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.error.DefaultWebResponseExceptionTranslator;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
//import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    private static final String RESOURCE_ID = "resource-server-rest-api";
    //private static final String SECURED_READ_SCOPE = "#oauth2.hasScope('read')";
    //private static final String SECURED_WRITE_SCOPE = "#oauth2.hasScope('write')";
    //private static final String SECURED_PATTERN = "/secured/**";
    
    //Add for Request JSON
    @Autowired
    JsonToUrlEncodedAuthenticationFilter jsonFilter;
    
    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        resources
                .resourceId(RESOURCE_ID);
         OAuth2AuthenticationEntryPoint authenticationEntryPoint = new OAuth2AuthenticationEntryPoint();
        authenticationEntryPoint.setExceptionTranslator(new CustomWebResponseExceptionTranslator());
        resources.authenticationEntryPoint(authenticationEntryPoint);
    }
    @Override
    public void configure(HttpSecurity http) throws Exception {
        /*http.requestMatchers()
                .antMatchers(SECURED_PATTERN).and().authorizeRequests()
                .antMatchers(HttpMethod.POST, SECURED_PATTERN).access(SECURED_WRITE_SCOPE)
                .anyRequest().access(SECURED_READ_SCOPE);*/
    	http
//            .addFilterBefore(jsonFilter, ChannelProcessingFilter.class)
            .antMatcher("/api/**")
            .authorizeRequests()
            .anyRequest()
            .authenticated();
                        
    	/*http
    		.anonymous().disable()
    		.authorizeRequests()
    		.antMatchers("/**").access("hasRole('ADMIN')")
    		.and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());*/
    }
    
    
}

class CustomWebResponseExceptionTranslator extends DefaultWebResponseExceptionTranslator {

    @Override
    public ResponseEntity translate(Exception e) throws Exception {
        ResponseEntity responseEntity = super.translate(e);
        //OAuth2Exception tes = responseEntity.getBody();
        HttpHeaders headers = new HttpHeaders();
        headers.setAll(responseEntity.getHeaders().toSingleValueMap());

        //System.out.println(body.getAdditionalInformation().get());
        //body.addAdditionalInformation("uca", "haiiiiii");
        //body.addAdditionalInformation("error", "");
        // do something with header or response
        return new ResponseEntity<>(new CustomOauthException(e.getMessage()), headers, responseEntity.getStatusCode());
    }

}
