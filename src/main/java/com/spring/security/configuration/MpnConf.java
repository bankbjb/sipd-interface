/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author DELL
 */
@Configuration
public class MpnConf {
    @Value("${mpn.ip:10.6.226.9}")
    private String ip;
	
    @Value("${mpn.port:55013}")
    private int port;
	
    @Value("${mpn.timeout:30000}")
    private int timeout;
	
    @Value("${mpn.sppw:BJB2013}")
    private String sppw;
	
    @Value("${mpn.sppu:UABS}")
    private String sppu;
	
    @Value("${mpn.cid:TG-CB-00-0000090}")
    private String cid;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getSppw() {
        return sppw;
    }

    public void setSppw(String sppw) {
        this.sppw = sppw;
    }

    public String getSppu() {
        return sppu;
    }

    public void setSppu(String sppu) {
        this.sppu = sppu;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }
    
   
}
