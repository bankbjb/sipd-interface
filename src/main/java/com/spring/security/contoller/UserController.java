package com.spring.security.contoller;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.spring.security.configuration.CallBackConf;
import com.spring.security.configuration.EqConfiguration;
import com.spring.security.configuration.GapuraConfiguration;
import com.spring.security.configuration.MpnConf;
import com.spring.security.configuration.SknConfiguration;
import com.spring.security.dao.Calbback;
import com.spring.security.dao.Djp;
import com.spring.security.dao.InqTransfer;
import com.spring.security.dao.MPNPayment;
import com.spring.security.dao.MyPropertyNamingStrategy;
import com.spring.security.dao.ReqAccBal;
import com.spring.security.dao.ReqOvb;
import com.spring.security.dao.ReqRspAccBal;
import com.spring.security.dao.ReqRspOvb;
import com.spring.security.dao.RspAccBal;
import com.spring.security.dao.RspOvb;
import com.spring.security.dao.Transfer;
import com.spring.security.model.inq_log;
import com.spring.security.model.kode_bank;
import com.spring.security.model.payroll_pajak;
import com.spring.security.model.tarsansction;
import com.spring.security.repository.RepoViewGabungan;
import com.spring.security.repository.inq_logRepository;
import com.spring.security.repository.pajak_test;
import com.spring.security.repository.payroll_pajakRepository;
import com.spring.security.repository.transactionRepository;
import com.spring.security.service.KodeBankServiceImpl;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;

@RestController
@RequestMapping("/api")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    private static final String key = "bjb-sipd";
    private String msg = null;
    private static final String algorithm = "HmacSHA256";

    @Autowired
    private GapuraConfiguration gapuraConf;

    @Autowired
    private EqConfiguration eqConfiguration;

    @Autowired
    private MpnConf MpnConfiguration;

    @Autowired
    private KodeBankServiceImpl ki;

    @Autowired
    private transactionRepository tr;

    @Autowired
    private SknConfiguration skncf;

    @Autowired
    private inq_logRepository inqlr;

    @Autowired
    private CallBackConf cbc;

    @Autowired
    private payroll_pajakRepository Payroll_Pjk;

//    @Autowired
//    private view_transaksiRepo TGB;
    private final ObjectMapper mapper;

    public ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

    public UserController() {
        this.mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @PostMapping(value = "/accountBalance", consumes = "application/json")
    RspAccBal accBal(@RequestBody ReqAccBal request,
            @RequestHeader(name = "X-Signature", required = false) String xSignature,
            @RequestHeader(name = "X-Timestamp", required = false) String xTimestamp) throws IOException, JsonProcessingException, ParseException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        msg = request.toString().trim().toUpperCase().concat(":" + xTimestamp);

        // 1. Get an algorithm instance.
        Mac sha256_hmac = Mac.getInstance(algorithm);
        // 2. Create secret key.
        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);
        // 3. Assign secret key algorithm.
        sha256_hmac.init(secret_key);
        // 4. Generate Base64 encoded cipher string.
        String hash = javax.xml.bind.DatatypeConverter.printBase64Binary(sha256_hmac.doFinal(msg.getBytes("UTF-8")));
        // You can use any other encoding format to get hash text in that encoding.
        System.out.println(hash);

        if (!xSignature.equals(hash)) {
            System.out.println("Signature not match");
        } else {
            System.out.println("Signature match");
        }

        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setPropertyNamingStrategy(new MyPropertyNamingStrategy());
        ReqRspAccBal ab = new ReqRspAccBal();
        LOGGER.info("###REQ_AB : " + ow.writeValueAsString(request));
        RspAccBal response = ab.ABCreate_inq(request, gapuraConf);
        LOGGER.info("###RES_AB : " + ow.writeValueAsString(response));
        return response;
    }

    @PostMapping(value = "/ovb", consumes = "application/json")
    RspOvb overbook(@RequestBody ReqOvb request) throws IOException, JsonProcessingException, ParseException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        request.setCdDbt("019");
        request.setCdCrd("519");
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.setPropertyNamingStrategy(new MyPropertyNamingStrategy());
        LOGGER.info("###REQ_OVB: " + ow.writeValueAsString(request));
        ReqRspOvb ITA = new ReqRspOvb();
        RspOvb response = ITA.ItaRsp(request, gapuraConf);
        LOGGER.info("###RES_OVB: " + ow.writeValueAsString(response));
        return response;
    }

    @PostMapping(value = "/inqTrf", produces = MediaType.APPLICATION_JSON_VALUE)
    String ReqRspInqtrf(@RequestBody String request, HttpServletRequest requesthttp) throws JsonProcessingException {
        LOGGER.info("###REQ_InqTrf: " + "IP : " + requesthttp.getRemoteAddr() + " : " + request);
        UUID id = UUID.randomUUID();
        String rsp = null;
        JSONObject InqRequest = new JSONObject(request);
        InqTransfer it = new InqTransfer();
        JSONObject status = new JSONObject();

        if (inqlr.GetPatnerId(InqRequest.getString("inquiry_partner_id")) == null) {

            if (!InqRequest.getString("bank_code").trim().equals("110") && InqRequest.getString("bank_account").trim().length() <= 20) {
                kode_bank kb = ki.findByKodebankOnline(InqRequest.getString("bank_code").trim());
                if (kb != null) {
                    rsp = it.inqTrannsfer(eqConfiguration, InqRequest.getString("bank_code").trim(), InqRequest.getString("bank_account").trim(), InqRequest.getString("bank_account").trim());
                } else {
                    rsp = "99";
                }
            } else {
                if (InqRequest.getString("bank_account").trim().length() <= 20) {
                    rsp = it.inqTrannsfer(eqConfiguration, InqRequest.getString("bank_code").trim(), InqRequest.getString("bank_account").trim(), InqRequest.getString("bank_account").trim());
                } else {
                    status.put("code", "902");
                    InqRequest.put("bank_account_name", "");
                    status.put("message", "Invalid Account");
                    InqRequest.put("status", status);
                    InqRequest.remove("bank_code");
                    InqRequest.remove("bank_account");
                    InqRequest.put("inquiry_id", id);
                }

            }

            System.out.println(rsp);
            if (rsp != null && rsp.substring(0, 2).equals("00")) {
                status.put("code", "900");
                status.put("message", "Inquiry Success");
                InqRequest.put("status", status);
                InqRequest.put("bank_account_name", rsp.substring(2, 35).trim());
                InqRequest.remove("bank_code");
                InqRequest.remove("bank_account");
                InqRequest.put("inquiry_id", id);
            } else if (rsp != null && (rsp.substring(0, 2).equals("76") || rsp.substring(0, 2).equals("14"))) {
                status.put("code", "902");
                status.put("message", "Invalid Account");
                InqRequest.put("status", status);
                InqRequest.put("bank_account_name", "");
                InqRequest.remove("bank_code");
                InqRequest.remove("bank_account");
                InqRequest.put("inquiry_id", id);
            } else if (rsp != null && rsp.substring(0, 2).equals("99")) {
                status.put("code", "901");
                InqRequest.put("bank_account_name", "");
                status.put("message", "Kode bank tidak ditemukan");
                InqRequest.put("status", status);
                InqRequest.remove("bank_code");
                InqRequest.remove("bank_account");
                InqRequest.put("inquiry_id", id);
            } else {
                status.put("code", "904");
                status.put("message", "Hubungan Dengan Switcher Terputus");
                InqRequest.put("bank_account_name", "");
                InqRequest.put("status", status);
                InqRequest.remove("bank_code");
                InqRequest.remove("bank_account");
                InqRequest.put("inquiry_id", id);
            }
            inq_log iq = new inq_log();
            iq.setKode_inquery(id);
            iq.setInquiry_partner_id(InqRequest.getString("inquiry_partner_id"));
            iq.setRequest(request);
            iq.setResponse(InqRequest.toString());
            inqlr.save(iq);
        } else {
            status.put("code", "401");
            status.put("message", "Duplicate transaction");
            InqRequest.put("status", status);
            InqRequest.remove("bank_code");
            InqRequest.remove("bank_account");
            InqRequest.put("inquiry_id", id);
        }
        LOGGER.info("###RES_InqTrf: " + "IP : " + requesthttp.getRemoteAddr() + " : " + InqRequest.toString());
        return InqRequest.toString();
    }

    @PostMapping(value = "/inqAlmt", produces = MediaType.APPLICATION_JSON_VALUE)
    String GetAlamat(@RequestBody String request) {
        UUID id = UUID.randomUUID();
        String rsp = null;
        JSONObject InqRequest = new JSONObject(request);
        InqTransfer it = new InqTransfer();
        JSONObject status = new JSONObject();
        rsp = it.GetAlamat(eqConfiguration, InqRequest.getString("bank_code").trim(), InqRequest.getString("bank_account").trim(), InqRequest.getString("bank_account").trim());
        System.out.println(rsp);
        if (rsp.substring(0, 2).equals("00")) {
            status.put("code", "900");
            status.put("message", "Inquiry Success");
            InqRequest.put("status", status);
            InqRequest.put("bank_account_name", rsp.substring(2, 35).trim());
            InqRequest.remove("bank_code");
            InqRequest.remove("bank_account");
            InqRequest.put("inquiry_id", id);
        } else {
            status.put("code", "200");
            status.put("message", "Failed to create transaction");
            InqRequest.put("status", status);
            InqRequest.remove("bank_code");
            InqRequest.remove("bank_account");
            InqRequest.put("inquiry_id", id);
        }

        return InqRequest.toString();
    }

    @PostMapping(value = "/overbooking_req_tanpa_array", produces = MediaType.APPLICATION_JSON_VALUE)
    String Overbooking_tanpa_array_request(@RequestBody String request, HttpServletRequest requesthttp) throws JsonProcessingException {
        LOGGER.info("###REQ_OVB: " + "IP : " + requesthttp.getRemoteAddr() + " : " + ow.writeValueAsString(request));
        String rsp = null;
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        tarsansction trn = new tarsansction();
        JSONObject InqRequest = new JSONObject(request);
        String npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, nominalPajak, nop, idbilling;
        Djp in = new Djp();
        JSONObject status = new JSONObject();
        String amount = String.valueOf(InqRequest.getInt("amount"));
        String rek_asal = InqRequest.getJSONObject("sender_info").getString("account_number");
        String rek_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_number");
        String bank_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_bank");
        String note = InqRequest.getString("note");
        String desc = InqRequest.getJSONObject("tx_additional_data").getString("desc_sp2d");
        String nama_penerima = InqRequest.getJSONObject("recipient_info").getJSONObject("additional_data").getString("account_name");
        String alamat_penerima = InqRequest.getJSONObject("recipient_info").getJSONObject("additional_data").getString("alamat_wp");
        JSONObject response = new JSONObject();
        //JSONArray pajak2 = new JSONArray();
        JSONObject isipajak = new JSONObject();
        response.put("tx_partner_id", InqRequest.getString("tx_partner_id"));
        UUID id = UUID.randomUUID();
        trn.setTx_partner_id(InqRequest.getString("tx_partner_id"));
        trn.setTrx_id(id);
        response.put("tx_id", id);
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
        date = new Date(System.currentTimeMillis());
        response.put("created", formatter2.format(date));
        trn.setRequest(InqRequest.toString());
        Transfer trf = new Transfer();
        int flag = 0;
        int flagtrf = 0;
        rsp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tujuan, rek_asal, amount, note, nama_penerima, alamat_penerima, ki, skncf);
        trn.setCreated_date(ts);
        trn.setReal_time(true);
        if (rsp.substring(0, 2).equals("00")) {
            System.out.println(rsp.substring(2, 21));//Refnum
            trn.setRefnum(rsp.substring(2, 21).trim());
            if (InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").has("pajak")) {
                //int lt = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").length();
                JSONObject pajak;
                pajak = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONObject("pajak");
                if (pajak.getBoolean("include_pajak")) {
                    npwp = pajak.getString("npwp");
                    npwp = npwp.replaceAll("[^a-zA-Z0-9]", "");
                    mata_anggaran = pajak.getString("mata_anggaran").trim();
                    jenis_setoran = pajak.getString("jenis_setoran").trim();
                    String[] arrmasapajak = pajak.getString("masa_pajak").trim().split("-");
                    masapajak1 = arrmasapajak[0];
                    masapajak2 = arrmasapajak[1];
                    tahunpajak = pajak.getString("tahun_pajak").trim();
                    nomersk = pajak.getString("no_spm").trim();
                    nominalPajak = pajak.get("nominal_pajak").toString().trim();
                    nop = pajak.getString("nomor_object_pajak");
                    rsp = in.INpwp(eqConfiguration, npwp, mata_anggaran, jenis_setoran);
                    //System.out.println("InqDjp" + rsp);

                    //isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
                    isipajak.put("id_billing", "");
                    isipajak.put("ntpn", "");
                    isipajak.put("tgl_ntpn", "");
                    isipajak.put("ntb", "");
                    if (rsp.substring(0, 2).equals("00")) {
                        rsp = in.CreateIdbilling(eqConfiguration, nominalPajak, npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, desc, nop);
                        if (rsp.substring(0, 2).equals("00")) {
                            JSONObject Mpnrsp;
                            idbilling = rsp.substring(64, 79);
                            isipajak.put("id_billing", idbilling);
                            //System.out.println(idbilling);
                            MPNPayment mpn = new MPNPayment();
                            Mpnrsp = mpn.MpnAll(MpnConfiguration, idbilling, rek_asal);
                            if (Mpnrsp.getString("RESPONSECODE").equals("00") || Mpnrsp.getString("RESPONSECODE").equals("69")) {
                                isipajak.put("ntpn", Mpnrsp.getJSONObject("MPO").getString("NTPN"));
                                isipajak.put("tgl_ntpn", Mpnrsp.getJSONObject("MPO").getString("SETLEMENT_DATE"));
                                isipajak.put("ntb", Mpnrsp.getJSONObject("MPO").getString("NTB"));
                                //pajak2.put(isipajak);
                            } else {
                                flag = 1;
                                //pajak2.put(isipajak);
                            }
                        } else {
                            flag = 1;
                            //pajak2.put(isipajak);
                        }
                    } else {
                        flag = 1;
                        //pajak2.put(isipajak);
                    }
                }
            }
            if (flag == 1) {
                if (rsp.substring(0, 2).equals("  ")) {
                    trn.setRespone_code("68");
                } else {
                    trn.setRespone_code(rsp.substring(0, 2));
                }
                trn.setStatus("3");
                status.put("code", "300");
                status.put("message", "Pending Transaction");
            } else {
                trn.setStatus("1");
                trn.setRespone_code("00");
                status.put("code", "000");
                status.put("message", "Success");
            }
        } else if (rsp.substring(0, 2).equals("51")) {
            trn.setRespone_code("51");
            trn.setStatus("4");
            status.put("code", "203");
            status.put("message", "Balance sender is not enough");
        } else {
            trn.setStatus("4");
            trn.setRespone_code(rsp.substring(0, 2));
            status.put("code", "200");
            status.put("message", "Failed to create transaction");
        }

        response.put("status", status);
        JSONObject adtionaldata = new JSONObject();
        adtionaldata.put("pajak", isipajak);
        response.put("additional_data", adtionaldata);
        date = new Date();
        ts = new Timestamp(date.getTime());
        trn.setUpdate_date(ts);
        trn.setResponse(response.toString());
        tr.save(trn);
        LOGGER.info("###RES_OVB: " + "IP : " + requesthttp.getRemoteAddr() + " : " + ow.writeValueAsString(response.toString()));
        return response.toString();
    }

    @PostMapping(value = "/overbooking_pajak_array", produces = MediaType.APPLICATION_JSON_VALUE)
    String Overbooking_pajak_array(@RequestBody String request) throws JsonProcessingException {
        LOGGER.info("###REQ_OVB: " + ow.writeValueAsString(request));
        String rsp = null;
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        tarsansction trn = new tarsansction();
        JSONObject InqRequest = new JSONObject(request);
        String npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, nominalPajak, nop, idbilling;
        Djp in = new Djp();
        JSONObject status = new JSONObject();
        String amount = InqRequest.get("amount").toString();
        String rek_asal = InqRequest.getJSONObject("sender_info").getString("account_number");
        String rek_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_number");
        String bank_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_bank");
        String note = InqRequest.getString("note");
        String desc = InqRequest.getJSONObject("tx_additional_data").getString("desc_sp2d");
        String nama_penerima = InqRequest.getJSONObject("recipient_info").getJSONObject("additional_data").getString("account_name");
        String alamat_penerima = InqRequest.getJSONObject("recipient_info").getJSONObject("additional_data").getString("alamat_wp");
        JSONObject response = new JSONObject();
        JSONArray pajak2 = new JSONArray();
        response.put("tx_partner_id", InqRequest.getString("tx_partner_id"));
        UUID id = UUID.randomUUID();
        trn.setTx_partner_id(InqRequest.getString("tx_partner_id"));
        trn.setTrx_id(id);
        response.put("tx_id", id);
        SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
        date = new Date(System.currentTimeMillis());
        response.put("created", formatter2.format(date));
        trn.setRequest(InqRequest.toString());
        Transfer trf = new Transfer();
        int flag = 0;
        int flagtrf = 0;
        rsp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tujuan, rek_asal, amount, note, nama_penerima, alamat_penerima, ki, skncf);
        trn.setCreated_date(ts);
        trn.setReal_time(true);
        if (rsp.substring(0, 2).equals("00")) {
            System.out.println(rsp.substring(2, 14));//Refnum
            trn.setRefnum(rsp.substring(2, 14));
            if (InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").has("pajak")) {
                int lt = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").length();
                JSONObject pajak;
                for (int i = 0; i < lt; i++) {
                    pajak = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").getJSONObject(i);
                    if (pajak.getBoolean("include_pajak")) {
                        npwp = pajak.getString("npwp");
                        mata_anggaran = pajak.getString("mata_anggaran").trim();
                        jenis_setoran = pajak.getString("jenis_setoran").trim();
                        String[] arrmasapajak = pajak.getString("masa_pajak1").trim().split("-");
                        masapajak1 = arrmasapajak[0];
                        masapajak2 = arrmasapajak[1];
                        tahunpajak = pajak.getString("tahun_pajak").trim();
                        nomersk = pajak.getString("no_spm").trim();
                        nominalPajak = pajak.get("nominal_pajak").toString().trim();
                        nop = pajak.getString("nomor_object_pajak");
                        rsp = in.INpwp(eqConfiguration, npwp, mata_anggaran, jenis_setoran);
                        //System.out.println("InqDjp" + rsp);
                        JSONObject isipajak = new JSONObject();
                        isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
                        isipajak.put("id_billing", "");
                        isipajak.put("ntpn", "");
                        isipajak.put("tgl_ntpn", "");
                        isipajak.put("ntb", "");
                        if (rsp.substring(0, 2).equals("00")) {
                            rsp = in.CreateIdbilling(eqConfiguration, nominalPajak, npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, desc, nop);
                            if (rsp.substring(0, 2).equals("00")) {
                                JSONObject Mpnrsp;
                                idbilling = rsp.substring(64, 79);
                                isipajak.put("id_billing", idbilling);
                                //System.out.println(idbilling);
                                MPNPayment mpn = new MPNPayment();
                                Mpnrsp = mpn.MpnAll(MpnConfiguration, idbilling, rek_asal);
                                if (Mpnrsp.getString("RESPONSECODE").equals("00") || Mpnrsp.getString("RESPONSECODE").equals("69")) {
                                    isipajak.put("ntpn", Mpnrsp.getJSONObject("MPO").getString("NTPN"));
                                    isipajak.put("tgl_ntpn", Mpnrsp.getJSONObject("MPO").getString("SETLEMENT_DATE"));
                                    isipajak.put("ntb", Mpnrsp.getJSONObject("MPO").getString("NTB"));
                                    pajak2.put(isipajak);
                                } else {
                                    flag = 1;
                                    pajak2.put(isipajak);
                                }
                            } else {
                                flag = 1;
                                pajak2.put(isipajak);
                            }
                        } else {
                            flag = 1;
                            pajak2.put(isipajak);
                        }
                    }
                }
            }
            if (flag == 1) {
                trn.setRespone_code("68");
                trn.setStatus("3");
                status.put("code", "300");
                status.put("message", "Pending Transaction");
            } else {
                trn.setStatus("1");
                trn.setRespone_code("00");
                status.put("code", "000");
                status.put("message", "Success");
            }
        } else if (rsp.substring(0, 2).equals("51")) {
            trn.setRespone_code("51");
            trn.setStatus("4");
            status.put("code", "203");
            status.put("message", "Balance sender is not enough");
        } else {
            trn.setStatus("4");
            trn.setRespone_code(rsp.substring(0, 2));
            status.put("code", "200");
            status.put("message", "Failed to create transaction");
        }

        response.put("status", status);
        JSONObject adtionaldata = new JSONObject();
        adtionaldata.put("pajak", pajak2);
        response.put("additional_data", adtionaldata);
        date = new Date();
        ts = new Timestamp(date.getTime());
        trn.setUpdate_date(ts);
        trn.setResponse(response.toString());
        tr.save(trn);
        LOGGER.info("###RES_OVB: " + ow.writeValueAsString(response.toString()));
        return response.toString();
    }

    @PostMapping(value = "/overbooking", produces = MediaType.APPLICATION_JSON_VALUE)
    String Overbooking(@RequestBody String request, HttpServletRequest requesthttp) {

        LOGGER.info("###REQ_OVB: " + "IP : " + requesthttp.getRemoteAddr() + " : " + request);

        String rsp = null;
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        tarsansction trn = new tarsansction();
        JSONObject InqRequest = new JSONObject();
        String npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, nominalPajak, nop, idbilling;
        Djp in = new Djp();
        JSONObject status = new JSONObject();
        JSONObject response = new JSONObject();
        JSONArray pajak2 = new JSONArray();
        Transfer trf = new Transfer();
        UUID id = UUID.randomUUID();
        try {
            InqRequest = new JSONObject(request);
        } catch (JSONException ex1) {
            System.out.println(ex1);
            //response.put("tx_partner_id", InqRequest.getString("tx_partner_id"));
            //UUID id = UUID.randomUUID();
            //trn.setTx_partner_id(InqRequest.getString("tx_partner_id"));
            trn.setTrx_id(id);
            response.put("tx_id", id);
            SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
            date = new Date(System.currentTimeMillis());
            response.put("created", formatter2.format(date));
            trn.setCreated_date(ts);
            trn.setRequest(InqRequest.toString());
            trn.setStatus("5");
            trn.setRespone_code("05");
            status.put("code", "270");
            status.put("message", "Invalid Payload Request");
            response.put("status", status);
            JSONObject adtionaldata = new JSONObject();
            JSONObject isipajak = new JSONObject();
            isipajak.put("id_billing", "");
            isipajak.put("ntpn", "");
            isipajak.put("tgl_ntpn", "");
            isipajak.put("ntb", "");
            pajak2.put(isipajak);
            if (!pajak2.isEmpty()) {
                System.out.println(pajak2.length());
                adtionaldata.put("pajak", pajak2.getJSONObject(0));
            } else {
                adtionaldata.put("pajak", "");
            }
            response.put("additional_data", adtionaldata.get("pajak"));
            date = new Date();
            ts = new Timestamp(date.getTime());
            trn.setUpdate_date(ts);
            trn.setResponse(response.toString());
            tr.save(trn);
            System.out.println("haiiii");
            LOGGER.info("###RES_OVB: " + "IP : " + requesthttp.getRemoteAddr() + " : " + response.toString());
            return response.toString();
        }
        try {
            String amount = String.valueOf(InqRequest.getInt("amount"));
            String rek_asal = InqRequest.getJSONObject("sender_info").getString("account_number");
            String rek_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_number");
            String bank_tujuan = InqRequest.getJSONObject("recipient_info").getString("account_bank");
            String note = InqRequest.getString("note");
            String desc = InqRequest.getJSONObject("tx_additional_data").getString("desc_sp2d");
            String nama_penerima = InqRequest.getJSONObject("recipient_info").getString("account_bank_name");
            String alamat_penerima = InqRequest.getJSONObject("recipient_info").getJSONObject("additional_data").getString("alamat_wp");
            String nosp2d = InqRequest.getJSONObject("tx_additional_data").getString("no_sp2d");
            String tipe_trx = InqRequest.get("tx_type").toString();

            response.put("tx_partner_id", InqRequest.getString("tx_partner_id"));
            trn.setTx_partner_id(InqRequest.getString("tx_partner_id"));
            trn.setTrx_id(id);
            trn.setNo_sp2d(nosp2d);
            trn.setType_trx(tipe_trx);
            response.put("tx_id", id);
            SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
            date = new Date(System.currentTimeMillis());
            response.put("created", formatter2.format(date));
            trn.setRequest(InqRequest.toString());
            System.out.println("haiiii 2 kalo ini");
            int flag = 0;
            int flagtrf = 0;
            trn.setCreated_date(ts);
            if (tr.TransaksiCek(trn.getTx_partner_id()) == null) {
                if (InqRequest.getJSONObject("tx_additional_data").getBoolean("is_realtime")) {
                    rsp = trf.TrannsferAll(eqConfiguration, bank_tujuan, rek_tujuan, rek_asal, amount, note, nama_penerima, alamat_penerima, ki, skncf);
                    trn.setReal_time(true);
                    if (rsp.substring(0, 2).equals("00")) {
                        System.out.println(rsp.substring(2, 14));//Refnum
                        trn.setRefnum(rsp.substring(2, 14));
                        if (InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").has("pajak")) {
                            int lt = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").length();
                            JSONObject pajak;
                            for (int i = 0; i < lt; i++) {
                                pajak = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").getJSONObject(i);
                                if (pajak.getBoolean("include_pajak")) {
                                    npwp = pajak.getString("npwp");
                                    mata_anggaran = pajak.getString("mata_anggaran").trim();
                                    jenis_setoran = pajak.getString("jenis_setoran").trim();
                                    if (pajak.has("masa_pajak")) {
                                        String[] arrmasapajak = pajak.getString("masa_pajak").trim().split("-");
                                        masapajak1 = arrmasapajak[0];
                                        masapajak2 = arrmasapajak[1];
                                    } else {
                                        masapajak1 = pajak.getString("masa_pajak1").trim();
                                        masapajak2 = pajak.getString("masa_pajak2").trim();
                                    }
                                    tahunpajak = pajak.getString("tahun_pajak").trim();
                                    nomersk = pajak.getString("no_spm").trim();
                                    if (!UserController.isStringInteger(nomersk)) {
                                        nomersk = "000000000000000";
                                    }
                                    nominalPajak = pajak.get("nominal_pajak").toString().trim();
                                    nop = pajak.getString("nomor_object_pajak");
                                    if (!UserController.isStringInteger(nop)) {
                                        nop = "";
                                    }
                                    rsp = in.INpwp(eqConfiguration, npwp, mata_anggaran, jenis_setoran);
                                    //System.out.println("InqDjp" + rsp);
                                    JSONObject isipajak = new JSONObject();
                                    // isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
                                    isipajak.put("id_billing", "");
                                    isipajak.put("ntpn", "");
                                    isipajak.put("tgl_ntpn", "");
                                    isipajak.put("ntb", "");
                                    if (rsp.substring(0, 2).equals("00")) {
                                        rsp = in.CreateIdbilling(eqConfiguration, nominalPajak, npwp, mata_anggaran, jenis_setoran, masapajak1, masapajak2, tahunpajak, nomersk, desc, nop);
                                        if (rsp.substring(0, 2).equals("00")) {
                                            JSONObject Mpnrsp;
                                            idbilling = rsp.substring(64, 79);
                                            isipajak.put("id_billing", idbilling);
                                            //System.out.println(idbilling);
                                            MPNPayment mpn = new MPNPayment();
                                            Mpnrsp = mpn.MpnAll(MpnConfiguration, idbilling, rek_asal);
                                            if (Mpnrsp.getString("RESPONSECODE").equals("00") || Mpnrsp.getString("RESPONSECODE").equals("69")) {
                                                isipajak.put("ntpn", Mpnrsp.getJSONObject("MPO").getString("NTPN"));
                                                isipajak.put("tgl_ntpn", Mpnrsp.getJSONObject("MPO").getString("SETLEMENT_DATE"));
                                                isipajak.put("ntb", Mpnrsp.getJSONObject("MPO").getString("NTB"));
                                                pajak2.put(isipajak);
                                            } else {
                                                flag = 1;
                                                pajak2.put(isipajak);
                                            }
                                        } else {
                                            flag = 1;
                                            pajak2.put(isipajak);
                                        }
                                    } else {
                                        flag = 1;
                                        pajak2.put(isipajak);
                                    }
                                } else {
                                    JSONObject isipajak = new JSONObject();
                                    // isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
                                    isipajak.put("id_billing", "");
                                    isipajak.put("ntpn", "");
                                    isipajak.put("tgl_ntpn", "");
                                    isipajak.put("ntb", "");
                                    pajak2.put(isipajak);
                                }
                            }
                        }
                        if (flag == 1) {
                            trn.setRespone_code("68");
                            trn.setStatus("3");
                            status.put("code", "300");
                            status.put("message", "Pending Transaction");
                        } else {
                            trn.setStatus("1");
                            trn.setRespone_code("00");
                            status.put("code", "000");
                            status.put("message", "Success");
                        }
                    } else if (rsp.substring(0, 2).equals("51")) {
                        JSONObject isipajak = new JSONObject();
                        // isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
                        isipajak.put("id_billing", "");
                        isipajak.put("ntpn", "");
                        isipajak.put("tgl_ntpn", "");
                        isipajak.put("ntb", "");
                        pajak2.put(isipajak);
                        trn.setRespone_code("51");
                        trn.setStatus("4");
                        status.put("code", "203");
                        status.put("message", "Balance sender is not enough");
                    } else {
                        JSONObject isipajak = new JSONObject();
                        trn.setStatus("4");
                        trn.setRespone_code(rsp.substring(0, 2));
                        status.put("code", "200");
                        status.put("message", "Failed to create transaction");
                        isipajak.put("id_billing", "");
                        isipajak.put("ntpn", "");
                        isipajak.put("tgl_ntpn", "");
                        isipajak.put("ntb", "");
                        pajak2.put(isipajak);
                    }
                } else {
                    trn.setReal_time(false);
                    SimpleDateFormat formatter4 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    Date date5 = null;
                    try {
                        date5 = formatter4.parse(InqRequest.getJSONObject("tx_additional_data").getString("execution_time"));
                        System.out.println(date5.toString());
                    } catch (ParseException ex) {
                        java.util.logging.Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").has("pajak")) {
                        int lt = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").length();
                        JSONObject pajak;
                        if (!InqRequest.get("tx_type").equals("LS|NONGAJI")) {
                            for (int j = 0; j < lt; j++) {
                                pajak = InqRequest.getJSONObject("sender_info").getJSONObject("additional_data").getJSONArray("pajak").getJSONObject(j);
                                // if (pajak.getBoolean("include_pajak")) {
                                payroll_pajak pj = new payroll_pajak();
                                pj.setNo_sp2d(nosp2d);
                                npwp = pajak.getString("npwp");
                                npwp = npwp.replaceAll("[^a-zA-Z0-9]", "");
                                mata_anggaran = pajak.getString("mata_anggaran").trim();
                                jenis_setoran = pajak.getString("jenis_setoran").trim();
                                if (pajak.has("masa_pajak")) {
                                    String[] arrmasapajak = pajak.getString("masa_pajak").trim().split("-");
                                    masapajak1 = arrmasapajak[0];
                                    masapajak2 = arrmasapajak[1];
                                } else {
                                    masapajak1 = pajak.getString("masa_pajak1").trim();
                                    masapajak2 = pajak.getString("masa_pajak2").trim();
                                }
                                tahunpajak = pajak.getString("tahun_pajak").trim();
                                nomersk = pajak.getString("no_spm").trim();
                                if (!UserController.isStringInteger(nomersk)) {
                                    nomersk = "000000000000000";
                                }
                                nominalPajak = pajak.get("nominal_pajak").toString().trim();
                                nop = pajak.getString("nomor_object_pajak");
                                if (!UserController.isStringInteger(nop)) {
                                    nop = "";
                                }
                                pj.setRekening_asal(rek_asal);
                                pj.setMata_anggaran(mata_anggaran);
                                pj.setNpwp(npwp);
                                pj.setMata_anggaran(mata_anggaran);
                                pj.setJenis_setoran(jenis_setoran);
                                pj.setMasapajak1(masapajak1);
                                pj.setMasapajak2(masapajak2);
                                pj.setNomersk(nomersk);
                                pj.setTahunpajak(tahunpajak);
                                pj.setNominal(Integer.parseInt(nominalPajak));
                                pj.setNop(nop);
                                pj.setTrx_id(id);
                                pj.setNo_sp2d(nosp2d);
                                Payroll_Pjk.save(pj);
                                // }
                            }
                        }

                    }
                    JSONObject isipajak = new JSONObject();
                    // isipajak.put("trx_id_pajak", pajak.getString("trx_id_pajak").trim());
                    trn.setCreated_date(ts);
                    isipajak.put("id_billing", "");
                    isipajak.put("ntpn", "");
                    isipajak.put("tgl_ntpn", "");
                    isipajak.put("ntb", "");
                    pajak2.put(isipajak);
                    trn.setExe_time(new java.sql.Timestamp(date5.getTime()));
                    trn.setRespone_code("");
                    trn.setStatus("2");
                    status.put("code", "100");
                    status.put("message", "Processed Transaction");
                }
                response.put("status", status);
                response.put("bpd_code","110");
                response.put("tx_type",tipe_trx);
                JSONObject adtionaldata = new JSONObject();
                if (!pajak2.isEmpty()) {
                    System.out.println(pajak2.length());
                    if (status.getString("code").equals("100")) {
                        adtionaldata.put("pajak", pajak2);
                    } else {
                        adtionaldata.put("pajak", pajak2);
                    }
                } else {
                    adtionaldata.put("pajak", "");
                }
                response.put("additional_data", adtionaldata.get("pajak"));
                date = new Date();
                ts = new Timestamp(date.getTime());
                trn.setUpdate_date(ts);
                trn.setResponse(response.toString());
                tr.save(trn);
            } else {
                response.put("tx_partner_id", InqRequest.getString("tx_partner_id"));
                //UUID id = UUID.randomUUID();

                response.put("tx_id", id);
                SimpleDateFormat formatter5 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
                date = new Date(System.currentTimeMillis());
                response.put("created", formatter5.format(date));
                status.put("code", "401");
                status.put("message", "Duplicate Transaction");
                response.put("status", status);
                JSONObject adtionaldata = new JSONObject();
                JSONObject isipajak = new JSONObject();
                isipajak.put("id_billing", "");
                isipajak.put("ntpn", "");
                isipajak.put("tgl_ntpn", "");
                isipajak.put("ntb", "");
                pajak2.put(isipajak);
                if (!pajak2.isEmpty()) {
                    System.out.println(pajak2.length());
                    adtionaldata.put("pajak", pajak2);
                } else {
                    adtionaldata.put("pajak", "");
                }
                response.put("additional_data", adtionaldata.get("pajak"));

            }
            //LOGGER.info("###RES_OVB: " + "IP : " + requesthttp.getRemoteAddr() + " : " + response.toString());
        } catch (JSONException e) {
            System.out.println(e);
            response.put("tx_partner_id", InqRequest.getString("tx_partner_id"));
            //UUID id = UUID.randomUUID();
            trn.setTx_partner_id(InqRequest.getString("tx_partner_id"));
            trn.setTrx_id(id);
            response.put("tx_id", id);
            SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");
            date = new Date(System.currentTimeMillis());
            response.put("created", formatter2.format(date));
            trn.setCreated_date(ts);
            trn.setRequest(InqRequest.toString());
            trn.setStatus("5");
            trn.setRespone_code("05");
            status.put("code", "270");
            status.put("message", "Invalid Payload Request");
            response.put("status", status);
            JSONObject adtionaldata = new JSONObject();
            JSONObject isipajak = new JSONObject();
            isipajak.put("id_billing", "");
            isipajak.put("ntpn", "");
            isipajak.put("tgl_ntpn", "");
            isipajak.put("ntb", "");
            pajak2.put(isipajak);
            if (!pajak2.isEmpty()) {
                System.out.println(pajak2.length());
                adtionaldata.put("pajak", pajak2);
            } else {
                adtionaldata.put("pajak", "");
            }
            response.put("additional_data", adtionaldata.get("pajak"));
            date = new Date();
            ts = new Timestamp(date.getTime());
            trn.setUpdate_date(ts);
            trn.setResponse(response.toString());
            tr.save(trn);

        }
        LOGGER.info("###RES_OVB: " + "IP : " + requesthttp.getRemoteAddr() + " : " + response.toString());
        return response.toString();
    }

    @PostMapping(value = "/CekStatus", produces = MediaType.APPLICATION_JSON_VALUE)
    String CekStatus(@RequestBody String request, HttpServletRequest requesthttp) throws JsonProcessingException {
        //LOGGER.info("###REQ_Cek_Status: " + ow.writeValueAsString(request));
        LOGGER.info("###REQ_Cek_Status: " + "IP : " + requesthttp.getRemoteAddr() + " : " + request);
        JSONObject InqRequest = new JSONObject(request);
        tarsansction trn;
        trn = tr.TransaksiGet(InqRequest.getString("tx_partner_id"), UUID.fromString(InqRequest.getString("tx_id")));
        JSONObject response, status, additional_data;
        Date date;
        Timestamp ts;
        if (trn != null) {
            response = new JSONObject(trn.getResponse());
            if (response.getJSONObject("additional_data").has("pajak")) {
                response.put("additional_data", response.getJSONObject("additional_data").get("pajak"));
            }
        } else {
            response = InqRequest;
            status = new JSONObject();
            additional_data = new JSONObject();
            date = new Date();
            ts = new Timestamp(date.getTime());
            response.put("created", ts);
            status.put("code", "400");
            status.put("message", "Send invalid tx_id or tx_partner_id");
            response.put("status", status);
            additional_data.put("id_billing", "");
            additional_data.put("ntpn", "");
            additional_data.put("tgl_ntpn", "");
            additional_data.put("ntb", "");
            response.put("additional_data", additional_data);

        }
        LOGGER.info("###RES_Cek_Status: " + "IP : " + requesthttp.getRemoteAddr() + " : " + response.toString());
        //LOGGER.info("###RES_Cek_Status: " + ow.writeValueAsString(trn.toString()));
        return response.toString();
    }

    @PostMapping(value = "/TransactionHistory", produces = MediaType.APPLICATION_JSON_VALUE)
    String TransactionHistory(@RequestBody String request, HttpServletRequest requesthttp) {
        LOGGER.info("###REQ_Transaction_History: " + "IP : " + requesthttp.getRemoteAddr() + " : " + request);
        JSONObject InqRequest = new JSONObject(request);
        JSONObject response = new JSONObject();
        SimpleDateFormat fromUser = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date date1, date2;
        Timestamp timestamp = null, timestamp2 = null;
        JSONArray jr = new JSONArray();

        try {
            date1 = fromUser.parse(InqRequest.getString("created_tx_start"));
            date2 = fromUser.parse(InqRequest.getString("created_tx_end"));
            timestamp = new java.sql.Timestamp(date1.getTime());
            timestamp2 = new java.sql.Timestamp(date2.getTime());
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
        List<tarsansction> trn;
        Pageable pageable;
        if (InqRequest.getString("order").equals("ASC")) {
            pageable = new PageRequest(InqRequest.getInt("offset"), InqRequest.getInt("limit"), Sort.Direction.ASC, "created_date");
        } else {
            pageable = new PageRequest(InqRequest.getInt("offset"), InqRequest.getInt("limit"), Sort.Direction.DESC, "created_date");
        }
        trn = tr.listTransaksi(timestamp, timestamp2, pageable);
        response.put("total_count", trn.size());
        for (int i = 0; i < trn.size(); i++) {
            JSONObject t = new JSONObject(trn.get(i).getResponse().trim());
            if (t.getJSONObject("additional_data").has("pajak")) {
                t.put("additional_data", t.getJSONObject("additional_data").get("pajak"));
                //System.out.println(t.toString());
            }
            jr.put(t);
        }
        response.put("data", jr);
        LOGGER.info("###RSP_Transaction_History: " + "IP : " + requesthttp.getRemoteAddr() + " : " + response.toString());
        return response.toString();
    }

    public static boolean isStringInteger(String number) {
        try {
            Integer.parseInt(number);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    @PostMapping(value = "/CallBackAPI", produces = MediaType.APPLICATION_JSON_VALUE)
    String CallBackAPI(@RequestBody String request, HttpServletRequest requesthttp) {
        LOGGER.info("###REQ_Transaction_History: " + "IP : " + requesthttp.getRemoteAddr() + " : " + request);
        JSONObject InqRequest = new JSONObject(request);
        Calbback cb = new Calbback();
        cb.callback_api(InqRequest, cbc);
        return InqRequest.toString();
    }

    @PostMapping(value = "/test_pajak", produces = MediaType.APPLICATION_JSON_VALUE)
    String testpajak(@RequestBody String request, HttpServletRequest requesthttp) {
        LOGGER.info("###REQ_Transaction_History: " + "IP : " + requesthttp.getRemoteAddr() + " : " + request);
        JSONObject InqRequest = new JSONObject(request);
        List<pajak_test> pjt;
        if (Payroll_Pjk.FindPajak2("02.19/04.0/000082/LS/1.01.0.00.0.00.01.0000/P.02/5/2021").size() > 0) {
            System.out.println("ada");
        };
        return InqRequest.toString();
    }

    @PostMapping(value = "/test_gabungan", produces = MediaType.APPLICATION_JSON_VALUE)
    String testGabungan(@RequestBody String request, HttpServletRequest requesthttp) {
        LOGGER.info("###REQ_Transaction_History: " + "IP : " + requesthttp.getRemoteAddr() + " : " + request);
        JSONObject InqRequest = new JSONObject(request);
//        List<view_transaksiRepo> tes;
//        view_transaksi vw;
        if (tr.FindTransaksi().size() > 0) {
            System.out.println("ada");
            for (RepoViewGabungan FindTransaksi : tr.FindTransaksi()) {
                System.out.println("gajih: " + FindTransaksi.geta());
                System.out.println("iwp1: " + FindTransaksi.getb());
                System.out.println("iwp8: " + FindTransaksi.getc());
                System.out.println("jks: " + FindTransaksi.getd());
                System.out.println("jkk: " + FindTransaksi.gete());
                System.out.println("jmk: " + FindTransaksi.getf());
                System.out.println("tapera: " + FindTransaksi.getg());
                System.out.println("ppn: " + FindTransaksi.geth());
                System.out.println("zakat: " + FindTransaksi.geti());
                System.out.println("bulog: " + FindTransaksi.getj());
                System.out.println("no_sp2d: " + FindTransaksi.getk());
                System.out.println("nominal_sp2d: " + FindTransaksi.getl());
                System.out.println("total_data_msg: " + FindTransaksi.getm());
                System.out.println("total_data_messaging: " + FindTransaksi.getn());
                System.out.println("total_gaji: " + FindTransaksi.geto());
                System.out.println("exe_time: " + FindTransaksi.getp());
                System.out.println("status: " + FindTransaksi.getq());
                System.out.println("kode_wilayah: " + FindTransaksi.getr());
                System.out.println("type_trx: " + FindTransaksi.gets());
                System.out.println("norek: " + FindTransaksi.gett());
                System.out.println("npwp: " + FindTransaksi.getu());
                System.out.println("mata_anggaaran: " + FindTransaksi.getv());
                System.out.println("jenis_setoran: " + FindTransaksi.getw());
                System.out.println("masa_pajak dan tahun_pajak: " + FindTransaksi.getx());//mm-mm-yy
                System.out.println("nomer_sk: " + FindTransaksi.gety());
                System.out.println("nop: " + FindTransaksi.getz());
            }

//            System.out.println("getIWP1: "+tr.FindTransaksi().get(0).getIWP1());
//            System.out.println("NOSP2D: "+tr.FindTransaksi().get(0).getIWP8());
//            System.out.println("IWP8: "+tr.FindTransaksi().get(0).getJKS());
//            System.out.println("gajih: "+tr.FindTransaksi().get(0).getJKK());
//            System.out.println("IWP8: "+tr.FindTransaksi().get(0).getJMK());
//            System.out.println("NOSP2D: "+tr.FindTransaksi().get(0).getTAPERA());
//            System.out.println("IWP8: "+tr.FindTransaksi().get(0).getPPN());
//            System.out.println("gajih: "+tr.FindTransaksi().get(0).getZAKAT());
//            System.out.println("IWP8: "+tr.FindTransaksi().get(0).getBULOG());
//            System.out.println("NOSP2D: "+tr.FindTransaksi().get(0).getNO_SP2D());
//            System.out.println("IWP8: "+tr.FindTransaksi().get(0).getNOMINAL_SP2D());
//            System.out.println("gajih: "+tr.FindTransaksi().get(0).getTOTAL_DATA_MSG());
//            System.out.println("IWP8: "+tr.FindTransaksi().get(0).getTOTAL_DATA_MESSAGING());
//            System.out.println("NOSP2D: "+tr.FindTransaksi().get(0).getTOTAL_GAJI());
//            System.out.println("NOSP2D: "+tr.FindTransaksi().get(0).getEXE_TIME());
//            System.out.println("NOSP2D: "+tr.FindTransaksi().get(0).getSTATUS());
//            System.out.println("NOSP2D: "+tr.FindTransaksi().get(0).getKODE_WILAYAH());
//            System.out.println("NOSP2D: "+tr.FindTransaksi().get(0).getTYPE_TRX());
//            for (int a = 0; a < tes.size(); a++) {
//                Object[] row = (Object[]) tes.get(a);
//                System.out.println(Arrays.toString(row));
//                 vw = new view_transaksi(Long.parseLong(row[0].toString()),Long.parseLong(row[1].toString()),Long.parseLong(row[2].toString()),Long.parseLong(row[3].toString()),Long.parseLong(row[4].toString()),Long.parseLong(row[5].toString()),Long.parseLong(row[6].toString()),Long.parseLong(row[7].toString()),Long.parseLong(row[8].toString()),Long.parseLong(row[9].toString()),Long.parseLong(row[10].toString()),Long.parseLong(row[11].toString()),row[12].toString(),row[13].toString(),,Long.parseLong(row[0].toString()),Long.parseLong(row[0].toString()),Long.parseLong(row[0].toString()) {};
//
//            }
        }
        return InqRequest.toString();
    }
}
