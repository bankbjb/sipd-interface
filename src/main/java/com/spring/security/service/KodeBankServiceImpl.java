/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spring.security.service;

import com.spring.security.model.kode_bank;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

/**
 *
 * @author DELL
 */
@Repository
@Transactional
public class KodeBankServiceImpl {

    @PersistenceContext
    private EntityManager entityManager;

//    private static final EntityManagerFactory emf;
//    
//    static {
//        emf = Persistence.createEntityManagerFactory("com.spring.security.model.kode_bank");
//    }
//     public static EntityManager getEntityManager() {
//        return emf.createEntityManager();
//    }
    public kode_bank findByKodebankOnline(String kode_bank_online) {
//        entityManager = KodeBankServiceImpl.getEntityManager();
        Query qry = entityManager.createNativeQuery("Select k.* from kode_bank k where k.kode_bank_online = ?1", kode_bank.class).setParameter(1, kode_bank_online);
        kode_bank kb;
        try {
            Object result = qry.getSingleResult();
            if (result == null) {
                System.out.println("masuk sini");
                kb = null;
            } else {
                System.out.println("masuk ada isinya");
                kb = (kode_bank) result;
                System.out.println(kb.getKode_bank_rtgs());
            }
        } catch (Exception e) {
            kb = null;
        }

        return kb;

    }

}
